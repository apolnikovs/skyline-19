DROP PROCEDURE IF EXISTS `UpgradeSchemaVersion`;
DELIMITER ;;
CREATE PROCEDURE `UpgradeSchemaVersion`(p_version varchar(20))
BEGIN

  -- Verify database schema version for use in update scripts
  -- author     Brian Etherington <b.etherington@pccsuk.com>
  -- copyright  2012 PC Control Systems
  -- link       http://www.pccontrolsystems.com
  -- Version 1.0
  --
  -- Change History
  -- Date       Version   Author                  Reason
  -- ??/??/2012 1.0       Brian Etherington       Initial Version
  
    DECLARE bad_version CONDITION FOR SQLSTATE '99001';
    DECLARE current_version varchar(20);
    SELECT `VersionNo` from `version` order by `Updated` desc limit 0,1 into current_version;
    IF p_version <> current_version THEN
         SIGNAL bad_version
            SET MESSAGE_TEXT='Cannot Upgrade Schema.';
    END IF;
END
;;
DELIMITER ;