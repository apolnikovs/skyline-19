# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-09-13 16:11                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.78');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` DROP FOREIGN KEY `country_TO_network`;

ALTER TABLE `network` DROP FOREIGN KEY `county_TO_network`;

ALTER TABLE `network` DROP FOREIGN KEY `user_TO_network`;

ALTER TABLE `network` DROP FOREIGN KEY `client_TO_network`;

ALTER TABLE `client` DROP FOREIGN KEY `user_TO_client`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `client` DROP FOREIGN KEY `branch_TO_client`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `county_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `country_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `user_TO_service_provider`;

ALTER TABLE `brand` DROP FOREIGN KEY `client_TO_brand`;

ALTER TABLE `brand` DROP FOREIGN KEY `network_TO_brand`;

ALTER TABLE `brand` DROP FOREIGN KEY `user_TO_brand`;

ALTER TABLE `network_client` DROP FOREIGN KEY `network_TO_network_client`;

ALTER TABLE `network_client` DROP FOREIGN KEY `client_TO_network_client`;

ALTER TABLE `network_manufacturer` DROP FOREIGN KEY `network_TO_network_manufacturer`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `client_TO_client_branch`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `network_TO_network_service_provider`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `service_provider_TO_network_service_provider`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `client_TO_service_type_alias`;

ALTER TABLE `unit_client_type` DROP FOREIGN KEY `client_TO_unit_client_type`;

ALTER TABLE `user` DROP FOREIGN KEY `network_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `service_provider_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `client_TO_user`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `brand_branch` DROP FOREIGN KEY `brand_TO_brand_branch`;

ALTER TABLE `job_type` DROP FOREIGN KEY `brand_TO_job_type`;

ALTER TABLE `county` DROP FOREIGN KEY `brand_TO_county`;

ALTER TABLE `country` DROP FOREIGN KEY `brand_TO_country`;

ALTER TABLE `payment_type` DROP FOREIGN KEY `brand_TO_payment_type`;

ALTER TABLE `product` DROP FOREIGN KEY `client_TO_product`;

ALTER TABLE `customer_title` DROP FOREIGN KEY `brand_TO_customer_title`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `network_TO_client_branch`;

ALTER TABLE `client_purchase_order` DROP FOREIGN KEY `brand_TO_client_purchase_order`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `network_TO_unit_pricing_structure`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `client_TO_unit_pricing_structure`;

ALTER TABLE `service_type` DROP FOREIGN KEY `brand_TO_service_type`;

ALTER TABLE `user` DROP FOREIGN KEY `brand_TO_user`;

ALTER TABLE `product` DROP FOREIGN KEY `network_TO_product`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `network_TO_service_type_alias`;

ALTER TABLE `audit_trail_action` DROP FOREIGN KEY `brand_TO_audit_trail_action`;

ALTER TABLE `contact_history_action` DROP FOREIGN KEY `brand_TO_contact_history_action`;

ALTER TABLE `security_question` DROP FOREIGN KEY `brand_TO_security_question`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `network_TO_town_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `network_TO_central_service_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `network_TO_postcode_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_provider_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_provider_TO_town_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `client_TO_town_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `client_TO_postcode_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `client_TO_central_service_allocation`;

ALTER TABLE `general_default` DROP FOREIGN KEY `brand_TO_general_default`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_provider_TO_postcode_allocation`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `network_TO_bought_out_guarantee`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `client_TO_bought_out_guarantee`;

ALTER TABLE `completion_status` DROP FOREIGN KEY `brand_TO_completion_status`;

# ---------------------------------------------------------------------- #
# Modify table "network"                                                 #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` ADD COLUMN `Skin` VARCHAR(20);

ALTER TABLE `network` MODIFY `Skin` VARCHAR(20) AFTER `TermsConditions`;

# ---------------------------------------------------------------------- #
# Modify table "client"                                                  #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` ADD COLUMN `Skin` VARCHAR(20);

ALTER TABLE `client` MODIFY `Skin` VARCHAR(20) AFTER `InvoicedPostalCode`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD COLUMN `Skin` VARCHAR(20);

ALTER TABLE `service_provider` MODIFY `Skin` VARCHAR(20) AFTER `Platform`;

# ---------------------------------------------------------------------- #
# Modify table "brand"                                                   #
# ---------------------------------------------------------------------- #

ALTER TABLE `brand` ADD COLUMN `Skin` VARCHAR(20);

ALTER TABLE `brand` MODIFY `Skin` VARCHAR(20) AFTER `BrandLogo`;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` ADD CONSTRAINT `country_TO_network` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `network` ADD CONSTRAINT `county_TO_network` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `network` ADD CONSTRAINT `user_TO_network` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network` ADD CONSTRAINT `client_TO_network` 
    FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `client` ADD CONSTRAINT `user_TO_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `client` ADD CONSTRAINT `branch_TO_client` 
    FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `brand` ADD CONSTRAINT `client_TO_brand` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `brand` ADD CONSTRAINT `network_TO_brand` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `brand` ADD CONSTRAINT `user_TO_brand` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_client` ADD CONSTRAINT `network_TO_network_client` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_client` ADD CONSTRAINT `client_TO_network_client` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `network_TO_network_manufacturer` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `client_TO_client_branch` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `network_TO_network_service_provider` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `client_TO_service_type_alias` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `client_TO_unit_client_type` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `user` ADD CONSTRAINT `network_TO_user` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `client_TO_user` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `brand_branch` ADD CONSTRAINT `brand_TO_brand_branch` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`) ON UPDATE CASCADE;

ALTER TABLE `job_type` ADD CONSTRAINT `brand_TO_job_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `county` ADD CONSTRAINT `brand_TO_county` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `country` ADD CONSTRAINT `brand_TO_country` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `payment_type` ADD CONSTRAINT `brand_TO_payment_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `product` ADD CONSTRAINT `client_TO_product` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `customer_title` ADD CONSTRAINT `brand_TO_customer_title` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `network_TO_client_branch` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `client_purchase_order` ADD CONSTRAINT `brand_TO_client_purchase_order` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `network_TO_unit_pricing_structure` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `client_TO_unit_pricing_structure` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `service_type` ADD CONSTRAINT `brand_TO_service_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `user` ADD CONSTRAINT `brand_TO_user` 
    FOREIGN KEY (`DefaultBrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `product` ADD CONSTRAINT `network_TO_product` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `network_TO_service_type_alias` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `audit_trail_action` ADD CONSTRAINT `brand_TO_audit_trail_action` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `contact_history_action` ADD CONSTRAINT `brand_TO_contact_history_action` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `security_question` ADD CONSTRAINT `brand_TO_security_question` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `network_TO_town_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `network_TO_central_service_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `network_TO_postcode_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `client_TO_town_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `client_TO_postcode_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `client_TO_central_service_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `general_default` ADD CONSTRAINT `brand_TO_general_default` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `network_TO_bought_out_guarantee` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `client_TO_bought_out_guarantee` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `brand_TO_completion_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.79');
