
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.243');

# ---------------------------------------------------------------------- #
# Update diary_matrix                                                    #
# ---------------------------------------------------------------------- # 
ALTER TABLE `diary_matrix`
	ADD COLUMN `ViamenteMatrixID` INT(10) NULL DEFAULT NULL AFTER `DiaryMatrixID`,
	ADD INDEX `ViamenteMatrixID` (`ViamenteMatrixID`);

	
# ---------------------------------------------------------------------- #
# New Permission                                                         #
# ---------------------------------------------------------------------- # 
INSERT INTO permission (PermissionID, Name, Description, ModifiedUserID) VALUES (12005, 'Service Action Tracking', 'Service Action Tracking', 1);



# ---------------------------------------------------------------------- #
# Andris Part System Changes                                             #
# ---------------------------------------------------------------------- # 
CREATE TABLE `service_provider_model` (
    `ServiceProviderModelID` INT(11) NOT NULL AUTO_INCREMENT,
    `ModelID` INT(11) NOT NULL,
    `ServiceProviderManufacturerID` INT(11) NULL DEFAULT NULL,
    `UnitTypeID` INT(11) NULL DEFAULT NULL,
    `ModelNumber` VARCHAR(30) NULL DEFAULT NULL,
    `ModelName` VARCHAR(24) NULL DEFAULT NULL,
    `ModelDescription` TEXT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `Status` ENUM('Active','In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INT(11) NULL DEFAULT NULL,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `Features` VARCHAR(50) NULL DEFAULT NULL,
    `IMEILengthFrom` INT(11) NULL DEFAULT NULL,
    `IMEILengthTo` INT(11) NULL DEFAULT NULL,
    `MSNLengthFrom` INT(11) NULL DEFAULT NULL,
    `MSNLengthTo` INT(11) NULL DEFAULT NULL,
    `WarrantyRepairLimit` DECIMAL(10,4) NULL DEFAULT NULL,
    `ExcludeFromRRCRepair` ENUM('Yes','No') NULL DEFAULT 'No',
    `AllowIMEIAlphaChar` ENUM('Yes','No') NULL DEFAULT 'No',
    `UseReplenishmentProcess` ENUM('Yes','No') NULL DEFAULT 'No',
    `HandsetWarranty1Year` ENUM('Yes','No') NULL DEFAULT 'No',
    `ReplacmentValue` DECIMAL(10,4) NULL DEFAULT NULL,
    `ExchangeSellingPrice` DECIMAL(10,4) NULL DEFAULT NULL,
    `LoanSellingPrice` DECIMAL(10,4) NULL DEFAULT NULL,
    `RRCOrderCap` INT(11) NULL DEFAULT NULL,
    PRIMARY KEY (`ServiceProviderModelID`),
    INDEX `IDX_model_ManufacturerID_FK` (`ServiceProviderManufacturerID`),
    INDEX `IDX_model_UnitTypeID_FK` (`UnitTypeID`),
    INDEX `IDX_model_ModifiedUserID_FK` (`ModifiedUserID`),
    CONSTRAINT `service_provider_manufacturer_TO_service_provider_model` FOREIGN KEY (`ServiceProviderManufacturerID`) REFERENCES `service_provider_manufacturer` (`ServiceProviderManufacturerID`),
    CONSTRAINT `unit_type_TO_service_provider_model` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
    CONSTRAINT `user_TO_service_provider_model` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

ALTER TABLE service_provider_model ADD COLUMN ServiceProviderID INT(11) NULL DEFAULT NULL AFTER RRCOrderCap;

ALTER TABLE accessory_model CHANGE COLUMN ModelID ServiceProviderModelID INT(10) NOT NULL DEFAULT '0' AFTER AccessoryModelID;

ALTER TABLE colour_model CHANGE COLUMN ModelID ServiceProviderModelID INT(10) NOT NULL DEFAULT '0' AFTER ColourID;

ALTER TABLE product_code_model CHANGE COLUMN ModelID ServiceProviderModelID INT(10) NULL DEFAULT NULL AFTER ProductCodeModelID;

ALTER TABLE colour ADD COLUMN ApproveStatus ENUM('Approved','Pending','NotApproved') NOT NULL DEFAULT 'Approved' AFTER Status;

ALTER TABLE accessory ADD COLUMN ApproveStatus ENUM('Approved','Pending','NotApproved') NOT NULL DEFAULT 'Approved' AFTER Status;

ALTER TABLE product_code ADD COLUMN ApproveStatus ENUM('Approved','Pending','NotApproved') NOT NULL DEFAULT 'Approved' AFTER Status;

CREATE TABLE service_provider_accessory ( ServiceProviderAccessoryID INT(11) NOT NULL AUTO_INCREMENT, AccessoryID INT(11) NOT NULL DEFAULT '0', AccessoryName VARCHAR(60) NULL DEFAULT NULL, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', EndDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, ServiceProviderID INT NOT NULL, PRIMARY KEY (ServiceProviderAccessoryID), INDEX IDX_unit_type_ModifiedUserID_FK (ModifiedUserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE service_provider_colour ( ServiceProviderColourID INT(10) NOT NULL AUTO_INCREMENT, ColourID INT(10) NOT NULL DEFAULT '0', ColourName VARCHAR(50) NULL DEFAULT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', ServiceProviderID INT NOT NULL, PRIMARY KEY (ServiceProviderColourID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=5;

CREATE TABLE service_provider_product_code ( ServiceProviderProductCodeID INT(10) NOT NULL AUTO_INCREMENT, ProductCodeID INT(10) NOT NULL DEFAULT '0', ProductCodeName VARCHAR(50) NOT NULL DEFAULT '0', Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', ServiceProviderID INT(10) NULL, PRIMARY KEY (ServiceProviderProductCodeID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;




# ---------------------------------------------------------------------- #
# Update table email                                                     #
# ---------------------------------------------------------------------- #
SET foreign_key_checks = 0;
REPLACE INTO `email` (`EmailID`, `EmailCode`, `Title`, `Message`, `ModifiedDate`, `ModifiedUserID`) VALUES
(16, 'job_complete', 'Job Complete - SL JOB No: <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html xmlns="http://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n<title>[PAGE_TITLE]</title>\r\n</head>\r\n\r\n<body>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:''Calibri'';">\r\n	<tbody><tr>\r\n		<td bgcolor="#ffffff" align="center">\r\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:''Calibri''">\r\n            	<tbody>\r\n                \r\n                <!-- #HEADER\r\n        		================================================== -->\r\n            	<tr>\r\n                	<td width="640"><!-- End top bar table -->\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                <td width="640" bgcolor="#52aae0" align="center">\r\n    \r\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\r\n        <tbody><tr>\r\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\r\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:''Calibri''" height="60px" colspan="2" valign="bottom">CUSTOMER RECEIPT</td>\r\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:''Calibri''" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:''Calibri''">Email us: <a style="color:#ffffff;font-family:''Calibri''" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a></p></td>\r\n        </tr>\r\n        \r\n    </tbody></table>\r\n    \r\n    <!-- End header -->\r\n</td>\r\n                </tr>\r\n                <!-- #CONTENT\r\n        		================================================== -->\r\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\r\n                <tr>\r\n                <td width="640" bgcolor="#ffffff">\r\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\r\n        <tbody><tr>\r\n            <td width="30"></td>\r\n            <td width="580">\r\n                <!-- #TEXT ONLY\r\n                ================================================== -->\r\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\r\n                    <tbody><tr>\r\n                        <td width="405">\r\n                            <p style="font-size:13px;font-family:''Calibri'';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\r\n                            \r\n							\r\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:''Calibri'';">Following the recent service we supplied we sincerely hope that we satisfied your requirement and that the standard of service exceeded your expectations.  Should you experience any issues with this product or any other similar products, please feel free to contact us to arrange a new service request.</p>\r\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:''Calibri''"></p>\r\n                                \r\n\r\n                        </td>\r\n                        <td width="175">\r\n						                        \r\n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\r\n                       \r\n					    </td>\r\n                    </tr>\r\n                   \r\n                </tbody></table>\r\n                <table>\r\n                <tbody>\r\n                \r\n                    <tr><td width="580" height="2" style="background:#3576bc;font-family:''Calibri''"></td></tr>\r\n                   \r\n                    </tbody>\r\n                    </table>\r\n                                        \r\n                <!-- #TEXT WITH ADDRESS\r\n                ================================================== -->\r\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\r\n                    <tbody>\r\n					\r\n                    <tr>\r\n                        <td colspan="2" style="padding-bottom:25px;text-align:center;"  ><br>\r\n                          <a href="[SERVICE_QUESTIONNAIRE]" style="color:#3576bc;font-size:16px;font-weight:bold;" >Complete our Service Questionnaire</a>\r\n                       </td>\r\n                        \r\n                    </tr>\r\n					\r\n					\r\n			    </tbody></table>\r\n                                        \r\n           \r\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody>\r\n                    <tr><td width="580" height="3" style="background:#3576bc;font-family:''Calibri''"></td></tr>\r\n                </tbody></table>\r\n                                        \r\n                <!-- #TEXT FOOTER\r\n                ================================================== -->\r\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody><tr>\r\n                        \r\n                    </tr>\r\n                    <tr><td width="580" height="100">\r\n                    \r\n\r\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:''Calibri''"></p>\r\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:''Calibri''">Assuring you of our best intention at all times.</p>\r\n\r\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:''Calibri''">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\r\n                    </td>\r\n                    </tr>\r\n                </tbody></table>\r\n                                                                                \r\n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\r\n                ================================================== -->\r\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody><tr>\r\n                        <td valign="top">\r\n                          \r\n                        </td>\r\n                       \r\n                    </tr>\r\n                </tbody></table>-->\r\n                                        \r\n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\r\n                ================================================== -->\r\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody><tr>\r\n                        <td width="180" valign="top">\r\n                          \r\n                        </td>\r\n                        <td width="20"></td>\r\n                        <td width="180" valign="top">\r\n                       \r\n                        </td>\r\n                        <td width="20"></td>\r\n                        <td width="180" valign="top">\r\n                            \r\n                        </td>\r\n                    </tr>\r\n                </tbody></table>-->\r\n            </td>\r\n            <td width="30"></td>\r\n        </tr>\r\n    </tbody></table>\r\n</td>\r\n                </tr>\r\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\r\n                <!-- #FOOTER\r\n   				 ================================================== -->\r\n                <tr>\r\n                <td width="640">\r\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\r\n        <tbody><tr>\r\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:''Calibri''">\r\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:''Calibri''">If you have any queries contact us on <a style="color:#ffffff;font-family:''Calibri''" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\r\n            </td>\r\n            </tr>\r\n\r\n    </tbody></table>\r\n</td>\r\n                </tr>\r\n               \r\n            </tbody></table>\r\n        </td>\r\n	</tr>\r\n</tbody></table>\r\n</body>\r\n</html>\r\n', '2013-04-29 08:50:26', 1);
SET foreign_key_checks = 1;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.244');
