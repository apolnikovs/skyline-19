# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.276');

# ---------------------------------------------------------------------- #
# Andris Stock Changes                                                   #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider_part_category ADD COLUMN ServiceProviderID INT NULL DEFAULT NULL AFTER CategoryName;

ALTER TABLE service_provider_part_sub_category ADD COLUMN ServiceProviderID INT(11) NULL DEFAULT NULL AFTER ModifiedUserID;

ALTER TABLE service_provider_part_sub_category CHANGE COLUMN ServiceProviderCategoryID ServiceProviderPartCategoryID INT(11) NULL DEFAULT NULL AFTER SubCategoryName;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.277');
