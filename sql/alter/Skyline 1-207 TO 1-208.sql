# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.207');

# ---------------------------------------------------------------------- #
# Modify table "unit_type"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE unit_type ADD COLUMN ImeiRequired ENUM('Yes','No') NULL DEFAULT NULL AFTER Status;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.208');
