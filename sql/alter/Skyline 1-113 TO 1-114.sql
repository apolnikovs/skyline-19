# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-06 10:50                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.113');

# ---------------------------------------------------------------------- #
# Add table "service_provider_contacts"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_contacts` (
    `ServiceProviderID` INTEGER NOT NULL,
    `ClientID` INTEGER NOT NULL,
    `ContactPhone` VARCHAR(40),
    `ContactEmail` VARCHAR(40),
    CONSTRAINT `service_provider_contactsID` PRIMARY KEY (`ServiceProviderID`, `ClientID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.114');
