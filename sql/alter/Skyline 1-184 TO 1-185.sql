# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.184');

# ---------------------------------------------------------------------- #
# Add table "appointment"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment`
ADD COLUMN `LockPartsEngineer` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `ViamenteExclude`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.185');
