# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.178');

# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `branch` ADD CONSTRAINT `branch_TO_service_provider1` FOREIGN KEY (`ThirdPartyServiceProvider`) REFERENCES `service_provider` (`ServiceProviderID`);

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.179');
