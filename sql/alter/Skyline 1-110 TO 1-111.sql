# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-01 12:11                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.110');

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_workload"                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer_workload` (
    `ServiceProviderEngineerWorkloadID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER NOT NULL,
    `ServiceProviderEngineerID` INTEGER NOT NULL,
    `WorkingDay` DATE NOT NULL,
    `TotalWorkTimeSec` INTEGER NOT NULL,
    `TotalIdleTimeSec` INTEGER NOT NULL,
    `TotalServiceTimeSec` INTEGER NOT NULL,
    `TotalDriveTimeSec` INTEGER NOT NULL,
    `TotalSteps` INTEGER NOT NULL,
    CONSTRAINT `service_provider_engineer_workloadID` PRIMARY KEY (`ServiceProviderEngineerWorkloadID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.111');
