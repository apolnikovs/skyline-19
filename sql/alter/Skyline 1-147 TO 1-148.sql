# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.147');


# ---------------------------------------------------------------------- #
# Modify table "repair_skill"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `repair_skill` ADD `RepairSkillCode` ENUM('ConsumerElectronics','DomesticAppliance') AFTER `RepairSkillName`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.148');