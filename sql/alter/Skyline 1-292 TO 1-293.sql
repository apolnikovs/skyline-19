# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.292');

# ---------------------------------------------------------------------- #
# Modify service_provider_supplier table								 #
# ---------------------------------------------------------------------- # 
ALTER TABLE `service_provider_supplier`
	ADD COLUMN `ShipToAccountNo` VARCHAR(20) NULL DEFAULT NULL AFTER `ModifiedDate`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.293');
