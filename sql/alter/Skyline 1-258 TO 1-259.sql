# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.258');

# ---------------------------------------------------------------------- #
# Modify Table job                                                       #
# ---------------------------------------------------------------------- # 
ALTER TABLE `job`
	CHANGE COLUMN `EngineerAssignedTime` `EngineerAssignedTime` TIME NULL DEFAULT NULL AFTER `EngineerAssignedDate`;


# ---------------------------------------------------------------------- #
# New Permissions                                                        #
# ---------------------------------------------------------------------- # 
INSERT INTO permission (PermissionID, Name, Description, ModifiedUserID) VALUES (12007, 'Job update page - remote engineer history table', 'Job update page - remote engineer history table', 1);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.259');
