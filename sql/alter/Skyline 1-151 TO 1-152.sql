# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.151');


# ---------------------------------------------------------------------- #
# Modify table "manufacturer"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE manufacturer ADD COLUMN AuthorisationManager VARCHAR(100) NULL DEFAULT NULL AFTER AuthReqChangedDate;
ALTER TABLE manufacturer ADD COLUMN AuthorisationManagerEmail VARCHAR(100) NULL DEFAULT NULL AFTER AuthorisationManager;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.152');


