<?php

/**
 * DiarySpMap
 * 
 * This model handles the diary_sp_map table. 
 * 
 * The primary purpose of this model is to allow a Viamente optimisation map code
 * to be stored against an date & service provider
 *
 * @author      Andrew Williams <a.williams@pccsuk.com>
 * 
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 16/10/2012  1.00    Andrew J. Williams   Initial Version             
 ******************************************************************************/

class DiarySpMap extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    private $conn;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

        $this->table = TableFactory::DiarySPMap();
    }
    
    /**
     * create
     *  
     * Create an entry for a service provider and date
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the entry
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        $this->Execute($this->conn, $cmd, $args);
        
        $id = $this->conn->lastInsertId();
        
        if ( $id == 0 ) {                                                       /* No id of new insert so error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Created';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'id' => $id
                     )
               );
    }
    
    /**
     * update
     *  
     * Update an ent5ry for service provider date and map 
     * 
     * @param array $args   Associative array of field values for to update the
     *                      part. Not this must include the primary key (diarySPMapID)
     *                      for the record to be updated.
     * 
     * @return array    (status - Status Code, message - Status message, rows_affected number of rows updated)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * delete
     *  
     * Delete a map refernce record. 
     * 
     * @param array $args (Field diarySPMapID => Value )
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($args) {
        $index  = array_keys($args); 
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$args[$index[0]] );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Deleted';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * getIdBySpAndDate
     *  
     * Get's a map id by ServiceProvider and Date 
     * 
     * @param a
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getIdBySpAndDate($spId, $d) {
        $sql = "
                SELECT
			`diarySPMapID`
		FROM
			`part`
		WHERE
			`ServiceProviderID` = $spId
			`MapDate` = '$d'
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['diarySPMapID']);                                                 /* Entry for date and service provider so return ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
}

?>
