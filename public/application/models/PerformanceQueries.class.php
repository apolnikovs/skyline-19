<?php

require_once('CustomModel.class.php');

/**
 * Short Description of Performance Queries Model.
 * 
 * Long description of Performance Queries Model.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 11/12/2012  1.0     Brian Etherington     Initial Version
 ******************************************************************************/

class PerformanceQueries extends CustomModel {
    
    public function __construct($controller) {  
        
        parent::__construct($controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
    }
    
    public function listServiceCentres() {
        
        $sql = 'select CompanyName as ServiceCentre from service_provider';
        return $this->Query($this->conn, $sql);
    }
    
    public function getSegSummary( $EndDate, $StartDate ) {        
        
        $sql = "select sp.CompanyName as ServiceCentre,
AVG(NETWORKDAYS(status03.`Date`, 
                status06or34.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as ResponseTime,
AVG(NETWORKDAYS(status06.`Date`, 
                appt.AppointmentDate, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as CallOutTime,
AVG(NETWORKDAYS(IF(appt.AppointmentDate < status07.`Date`,appt.AppointmentDate,status07.`Date`), 
                status12.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as InspectionTime,
AVG(NETWORKDAYS(status12.`Date`, 
                status13or14.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as WaitingForRepair,
AVG(NETWORKDAYS(status34.`Date`, 
                status07.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime) +
    NETWORKDAYS(status31.`Date`, 
                status32.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime) +
    NETWORKDAYS(status08.`Date`, 
                status09or10.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as CustomerFeedback,
AVG(NETWORKDAYS(IF(appt.AppointmentDate > completionStart.`Date`,appt.AppointmentDate,completionStart.`Date`), 
                completionEnd.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as CompletionTime,
AVG(NETWORKDAYS(status18.`Date`, 
                DATE_ADD(job.RepairCompleteDate, INTERVAL '12:00:00'  HOUR_SECOND), 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as FinalDelivery,
AVG(NETWORKDAYS(DATE_ADD(job.RepairCompleteDate, INTERVAL '12:00:00'  HOUR_SECOND), 
                status21W.`Date`, 
                sp.WeekdaysOpenTime, sp.WeekdaysCloseTime)
  ) as TimeToClaim
from job 
join service_provider sp on job.ServiceProviderID=sp.ServiceProviderID
join status s on job.statusID=s.StatusID

left join (select a.JobID, min(a.AppointmentDate) as AppointmentDate from appointment a
           join job on a.JobID=job.JobId and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           group by a.JobID) appt on job.JobId=appt.JobId

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='03 TRANSMITTED TO AGENT' 
           group by sh.JobID) status03 on job.JobID=status03.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='06 FIELD CALL ARRANGED' 
           group by sh.JobID) status06 on job.JobID=status06.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='07 UNIT IN REPAIR' 
           group by sh.JobID) status07 on job.JobID=status07.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='08 ESTIMATE SENT' 
           group by sh.JobID) status08 on job.JobID=status08.JobID

left join (select sh.JobID, max(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName in ('09 ESTIMATE ACCEPTED','10 ESTIMATE REFUSED')       
           group by sh.JobID) status09or10 on job.JobID=status09or10.JobID

left join (select sh.JobID, max(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName in ('06 FIELD CALL ARRANGED','34 PACKAGING SENT')       
           group by sh.JobID) status06or34 on job.JobID=status06or34.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='12 PARTS ORDERED'        
           group by JobID) status12 on job.JobID=status12.JobID

left join (select sh.JobID, max(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName in ('13 PARTS RECEIVED','14 2ND FIELD CALL REQ.')       
           group by sh.JobID) status13or14 on job.JobID=status13or14.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName in ('18 REPAIR COMPLETED','18 FIELD CALL COMPLETE')        
           group by sh.JobID) status18 on job.JobID=status18.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='21W CLAIMED'        
           group by sh.JobID) status21W on job.JobID=status21W.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='31 WAITING FOR INFO'        
           group by sh.JobID) status31 on job.JobID=status31.JobID

left join (select sh.JobID, max(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='32 INFO RECEIVED'        
           group by sh.JobID) status32 on job.JobID=status32.JobID

left join (select sh.JobID, min(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName='34 PACKAGING SENT'        
           group by sh.JobID) status34 on job.JobID=status34.JobID

left join (select sh.JobID, max(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName in ('13 PARTS RECEIVED',
                                  '09 ESTIMATE ACCEPTED',
                                  '10 ESTIMATE REFUSED',
                                  '32 INFO RECEIVED',
                                  '17 AUTHORITY ISSUED',
                                  '03 TRANSMITTED TO AGENT',
                                  '07 UNIT IN REPAIR')        
           group by sh.JobID) completionStart on job.JobID=completionStart.JobID

left join (select sh.JobID, max(sh.`Date`) as `Date` 
           from status_history sh
           join `status` s on sh.StatusID=s.StatusID 
           join job on sh.JobID=job.JobID and job.ClosedDate is not null
                       and job.ClosedDate between :StartDate and :EndDate
           where s.StatusName in ('18 REPAIR COMPLETED','18 FIELD CALL COMPLETE')        
           group by sh.JobID) completionEnd on job.JobID=completionEnd.JobID

where job.ClosedDate is not null and s.StatusName<>'29 JOB CANCELLED'
and job.ClosedDate between :StartDate and :EndDate " . $this->UserJobFilter() . " group by sp.CompanyName";
        
        $params = array( 'StartDate' => $StartDate,
                         'EndDate' => $EndDate );
        
        return $this->Query($this->conn, $sql, $params);
    }
    
    private function UserJobFilter( $jobalias='job', $branchbrandalias='branch_brand') {
             
        switch ($this->controller->user->UserType) {  /* Set job filter based on user type */
            case 'Admin':
                return '' ;
                break;
            
            case 'ServiceProvider':
                return ' AND '.$jobalias.'.ServiceProviderID='.$this->controller->user->ServiceProviderID;
                break;
            
            case 'Branch':
                switch ($this->controller->user->BranchJobSearchScope) {  /* User is branch so filter is based on Brancj Job Scope Serach */
                    case 'Brand':
                        return (" AND $branchbrandalias.`BrandID` = {$this->controller->user->DefaultBrandID}");
                        break;
                    
                    case 'Client':
                        return ' AND '.$jobalias.'.ClientID='.$this->controller->user->ClientID;
                        break;
                    
                    default:                                                    /* Branch Only */
                        return ' AND '.$jobalias.'.BranchID='.$this->controller->user->BranchID;
                }                
                break;
            
            case 'Client':
                return ' AND '.$jobalias.'.ClientID='.$this->controller->user->ClientID;
                break;
            
            case 'Network':
                return ' AND '.$jobalias.'.NetworkID='.$this->controller->user->NetworkID;
                break;
            
            case 'Manufacturer':
                return ' AND '.$jobalias.'.ManufacturerID='.$this->controller->user->ManufacturerID;
                break;
            
             case 'ExtendedWarrantor':
                return '';
                break;
            
            default:
                //$this->controller->log(var_export($this->controller->user,true));
                throw new exception('Unrecognised User Type');
        }   
    }

}

?>
