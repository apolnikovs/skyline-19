<?php

require_once('CustomModel.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

/**
 * This model is for general Skyline Database Queries only.
 * 
 * There should be NO database updates in this model (those should be 
 * marshalled by a separate Business model and rely upon individual table models
 * for create/update/delete methods for specific tables).  Neither should there 
 * be any data manipulation - that should be performed in the higher level
 * Business model. Just plain vanilla database queries here please.
 * 
 * In this way, these same database queries can be used in different
 * Business models with different business logic without duplication of 
 * query code.
 * 
 * 
 * Methods in this model should be of the form:
 * 
 *      public function getXXXXXX( $args,...... ) {
 *          $sql = "select .......";
 *          $params = array( 'name' => value );
 *          return $this->Query($this->conn, $sql, $params);
 *      }
 * 
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.1
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 30/01/2013  1.0     Brian Etherington     Initial Version
 * 12/03/2013  1.1     Brian Etherington     Fix problem with customer table name not lower case
 ******************************************************************************/

class SkylineQueriesModel extends CustomModel {

    public function __construct($controller) {  
        
        parent::__construct($controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        $this->debug = false;

    }
    
    public function findJobs( array $args, stdClass $User=null, array $fields=null ) {
                    
        $select = 'select JobID from job';
        $left_cust = '';       
        $where = ' where ('; 
        $params = array();
        
        if ($fields !== null) {
            $select = 'select';
            $first = true;
            foreach($fields as $key => $value) {
                if (is_array($value)) {
                    switch($key) {
                        case  'Customer':
                            $left_cust = ' left join customer cust on cust.CustomerID=job.CustomerID';
                            $alias = 'cust';
                            break;
                        case 'Job':
                            $alias = 'job';
                            break;
                        default:
                            $alias = 'job';
                            break;                       
                    }
                    foreach($value as $field) {
                        if (!$first) $select .= ',';
                        $first = false;
                        $select .= " `$alias`.`$field`";
                    }
                }
            }
            $select .= ' from job';
        }
        
        $first = true;
        foreach($args as $key => $value) {
            // ignore empty search terms
            if (!empty($value)) {
                if (is_array($value)) {
                    // extend this for any child table as necessary (eg Service Provider etc....)
                    switch($key){
                        case 'Customer':
                            $left_cust = ' left join customer cust on cust.CustomerID=job.CustomerID';
                            $alias = 'cust';
                            break;
                        case 'Job':
                            $alias = 'job';
                            break;  
                        default:
                            $alias = 'job';
                            break; 
                    }
                    foreach($value as $key2 => $value2) {
                        if (!$first) $where .= ' AND';
                        $first = false;
                        // make sure customer parameter names are unique by prefixing with cust_
                        if (strpos($value2, '%') === FALSE)
                            $where .= " $alias.`$key2` = :{$alias}_$key2";
                        else
                            $where .= " $alias.`$key2` LIKE :{$alias}_$key2";
                        $params["{$alias}_$key2"]=$value2;
                    }
                }
            }
        }
        
        // return array most recent job first
        $sql = 'select JobID from job'.$left_cust.$where.')'.Functions::UserJobFilter( $User ).' order by JobID desc';
               
        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
    }
    
    public function getJobDetails( $JobID, stdClass $User=null ) {
                
        $sql = "select 
            
                     job.DateBooked,
                     st.StatusName as Status,
                     ifnull(mfg.ManufacturerName, job.ServicebaseManufacturer) as Manufacturer,
                     ifnull(mdl.ModelName, job.ServiceBaseModel) as Model,
                     stype.ServiceTypeName as serviceType,
                     ifnull(utype.UnitTypeName, job.ServiceBaseUnitType) as UnitType,
                     job.ServiceCentreJobNo,
                     job.ServiceProviderID,
                     
                     job.ColAddCompanyName,
                     job.ColAddBuildingNameNumber, 
                     job.ColAddStreet, 
                     job.ColAddLocalArea, 
                     job.ColAddTownCity, 
                     jc.Name as CollectionCounty, 
                     job.ColAddPostcode, 
                     jcy.name as CollectionCountry,
                     job.ColAddPhone,
                     job.ColAddPhoneExt,
                     job.ColAddEmail,
                     job.CollectionDate,
                     job.CourierID,
                     job.ColAddCountyID,
                     job.ColAddCountryID,
                     ship.ConsignmentNo as ConsignmentNo,
                     ship.PackagingType as PackagingType,

                     n.CompanyName as NetworkName, 
                     n.BuildingNameNumber as NetworkBuildingNameNumber, 
                     n.Street as NetworkStreet, 
                     n.LocalArea as NetworkLocalArea, 
                     n.TownCity as NetworkTownCity, 
                     n.PostalCode as NetworkPostalCode, 
                     n.ContactPhone as NetworkContactPhone, 
                     n.ContactPhoneExt as NetworkContactPhoneExt,
                     n.ContactEmail as NetworkContactEmail,
                     nc.Name as NetworkCounty,
                     ncy.Name as NetworkCountry,
                     
                     c.ClientName as ClientName, 
                     c.ClientShortName as ClientShortName,
                     c.BuildingNameNumber as ClientBuildingNameNumber, 
                     c.Street as ClientStreet, 
                     c.LocalArea as ClientLocalArea, 
                     c.TownCity as ClientTownCity, 
                     c.PostalCode as ClientPostalCode, 
                     c.ContactPhone as ClientContactPhone, 
                     c.ContactPhoneExt as ClientContactPhoneExt,
                     c.ContactEmail as ClientContactEmail,
                     cc.Name as ClientCounty,
                     ccy.Name as ClientCountry,
                     
                     cust.CustomerID as CustomerID,
                     custt.Title as CustomerTitle,
                     cust.ContactFirstName as CustomerFirstName, 
                     cust.ContactLastName as CustomerLastName,
                     cust.BuildingNameNumber as CustomerBuildingNameNumber, 
                     cust.Street as CustomerStreet, 
                     cust.LocalArea as CustomerLocalArea, 
                     cust.TownCity as CustomerTownCity, 
                     cust.PostalCode as CustomerPostalCode, 
                     cust.ContactHomePhone as CustomerHomePhone,
                     cust.ContactMobile as CustomerMobile,
                     cust.ContactWorkPhone as CustomerWorkPhone, 
                     cust.ContactWorkPhoneExt as CustomerWorkPhoneExt,
                     cust.ContactEmail as CustomerEmail,
                     custc.Name as CustomerCounty,
                     custcy.Name as CustomerCountry,
                     
                     b.BranchName as BranchName, 
                     b.BuildingNameNumber as BranchBuildingNameNumber, 
                     b.Street as BranchStreet, 
                     b.LocalArea as BranchLocalArea, 
                     b.TownCity as BranchTownCity, 
                     b.PostalCode as BranchPostalCode, 
                     b.ContactPhone as BranchContactPhone, 
                     b.ContactPhoneExt as BranchContactPhoneExt,
                     b.ContactEmail as BranchContactEmail,
                     bc.Name as BranchCounty,
                     bcy.Name as BranchCountry,
                     bd.BrandName as BrandName,
                     
                     sp.CompanyName as ServiceProviderName,
                     sp.BuildingNameNumber as ServiceProviderBuildingNameNumber, 
                     sp.Street as ServiceProviderStreet, 
                     sp.LocalArea as ServiceProviderLocalArea, 
                     sp.TownCity as ServiceProviderTownCity, 
                     sp.PostalCode as ServiceProviderPostalCode, 
                     sp.ContactPhone as ServiceProviderContactPhone, 
                     sp.ContactPhoneExt as ServiceProviderContactPhoneExt,
                     sp.ContactEmail as ServiceProviderContactEmail,
                     spc.Name as ServiceProviderCounty,
                     spcy.Name as ServiceProviderCountry, 
                     sp.WeekdaysOpenTime as ServiceProviderWeekdaysOpenTime,
                     sp.WeekdaysCloseTime as ServiceProviderWeekdaysCloseTime,
                     sp.SaturdayOpenTime as ServiceProviderSaturdayOpenTime,
                     sp.SaturdayCloseTime as ServiceProviderSaturdayCloseTime,
                     
                     u.UserName as UserName, 
                     u.ContactFirstName as UserContactFirstName, 
                     u.ContactLastName as UserContactLastName, 
                     u.ContactEmail as UserContactEmail, 
                     u.NetworkID as UserNetworkID, 
                     u.ServiceProviderID as UserServiceProviderID, 
                     u.ClientID as UserClientID, 
                     u.BranchID as UserBranchID,
                     u.ManufacturerID as UserManufacturerID,
                     u.ExtendedWarrantorID as UserExtendedWarrantorID,
                     u.SuperAdmin as UserSuperAdmin,
                     ub.BrandName as UserBrandName
                                         
                from job
                left join county jc on jc.CountyId=job.ColAddCountyID
                left join country jcy on jcy.CountryId=job.ColAddCountryID
                left join shipping ship on ship.JobID=job.JobID
                
                left join network n on n.NetworkID=job.NetworkID
                    left join county nc on nc.CountyId=n.CountyID
                    left join country ncy on ncy.CountryId=n.CountryID
                    
                left join client c on c.ClientID=job.ClientID
                    left join county cc on cc.CountyId=c.CountyID
                    left join country ccy on ccy.CountryId=c.CountryID
                    
                left join branch b on b.BranchID=job.BranchID
                    left join county bc on bc.CountyId=b.CountyID
                    left join country bcy on bcy.CountryId=b.CountryID
                    
                left join service_provider sp on sp.ServiceProviderID=job.ServiceProviderID
                    left join county spc on spc.CountyId=sp.CountyID
                    left join country spcy on spcy.CountryId=sp.CountryID
                    
                left join customer cust on cust.CustomerID=job.CustomerID
                    left join customer_title custt on custt.CustomerTitleID=cust.CustomerTitleID
                    left join county custc on custc.CountyId=cust.CountyID
                    left join country custcy on custcy.CountryId=cust.CountryID

                left join user u on u.userId=job.BookedBy
                    left join brand ub on ub.BrandID=u.DefaultBrandID
                    
                left join brand bd on bd.BrandId=job.BrandID
                left join status st on st.StatusID=job.StatusID
                left join manufacturer mfg on mfg.ManufacturerID=job.ManufacturerID
                left join model mdl on mdl.ModelID=job.ModelID
                left join unit_type utype on utype.UnitTypeID=mdl.UnitTypeID
                left join service_type stype on stype.ServiceTypeID=job.ServiceTypeID

                where job.JobID=:JobID" . Functions::UserJobFilter( $User ) .
                " order by ship.ShippingID desc limit 1";
        
        $params = array( 'JobID' => (int) $JobID );
               
        $result = $this->Query($this->conn, $sql, $params);
               
        if (count($result) > 0) 
            return $result[0];
                
        return null;
        
    }
    
    public function getAppointments( $JobID ) {
        
        $sql = "select AppointmentDate, AppointmentTime, 
                       AppointmentType, EngineerCode, OutCardleft
                from appointment
                where JobID=:JobID
                order by AppointmentDate, AppointmentTime";
        
        $params = array( 'JobID' => (int) $JobID );
        
        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
    
    }
    
    public function getContactHistory( $JobID ) {
 
        $sql = "select ContactDate, ContactTime, UserCode, Subject, Note
                from contact_hist 
                where JobID=:JobID
                order by ContactDate, ContactTime";
        
        $params = array( 'JobID' => (int) $JobID );
        
        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
    }
    
    public function getStatusHistory( $JobID ) {
     
        $sql = "select sh.Date as StatusDate, st.Status as Status, sh.UserCode as UserCode
                from status_history sh
                left join status st on st.StatusId=sh.StatusID
                where sh.JobID=:JobID
                order by sh.Date";
        
        $params = array( 'JobID' => (int) $JobID );
        
        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
    }
    
    public function getTitles( $BrandID=Constants::SKYLINE_BRAND_ID ) {
               
        $sql = "select CustomerTitleID, TitleCode, Title from customer_title where BrandID=:BrandID and status='Active' order by TitleCode";
        $params = array( 'BrandID' => (int) $BrandID);

        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
        
    }
    
    public function getServiceTypes( $JobTypeID, $BrandID=Constants::SKYLINE_BRAND_ID ) {
               
        $sql = "select ServiceTypeID, ServiceTypeCode, ServiceTypeName from service_type where BrandID=:BrandID and JobTypeID=:JobTypeID and status='Active' order by ServiceTypeCode";
        $params = array( 'BrandID' => (int) $BrandID,
                         'JobTypeID' => (int) $JobTypeID);

        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
        
    }
    
    public function getClientServiceTypes($ClientID, $JobTypeID = null) {
              
        $params = array( 'ClientID' => (int) $ClientID );
               
        $sql = "select st.ServiceTypeID, st.ServiceTypeCode, st.ServiceTypeName 
                from service_type st
                left join service_type_alias sta on sta.ServiceTypeID=st.ServiceTypeID
                where sta.ClientId=:ClientID and sta.Status='Active' and st.status='Active' 
	       ";
        
        if (!empty($JobTypeID)) {
            $sql .= "and st.JobTypeId=:JobTypeID ";
            $params['JobTypeID'] = $JobTypeID;
        }
        
        $sql .= "order by st.Priority";

        $result = $this->Query($this->conn, $sql, $params);
        
        return $result;
        
    }
    
    public function getJobTypeFromServiceType( $ServiceTypeID ) {
        
        $sql = "select JobTypeID from service_type where ServiceTypeID=:ServiceTypeID";
        $params = array( 'ServiceTypeID' => $ServiceTypeID );
        $result = $this->Query($this->conn, $sql, $params);
        
        if(count($result) > 0)
            return $result[0]['JobTypeID'];
        
        return false;
    }
}

?>
