<?php

/**
 * Email.class.php
 * 
 * This model handles interaction with the email table.
 * 
 * @author      Andrew J. Williams <a.williams@pccsuk.com>
 * @version     1.07
 * @copyright   2012 - 2013 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author                 Reason
 * 06/09/2012  1.00    Andrew J. Williams     Initial Version
 * 23/09/2012  1.01    Nageswara Rao Kanteti  added EmailCode for selection of row. 
 * 23/09/2012  1.02    Nageswara Rao Kanteti  Added mail body parameter to mark email sent
 * 22/09/2012  1.03    Nageswara Rao Kanteti  Fixed html body message bug and added from_name to the SetFrom method.
 * 10/01/2013  1.04    Vykintas Rutkunas      RA Changes
 * 17/01/2013  1.05    Vykintas Rutkunas      E-Mail Sending Changes - development.ini
 * 29/01/2013  1.06    Brian Etherington      Implemented cahnges to spec for autorised Manufacturers / Service Providers
 * 20/02/2013  1.07    Nageswara Rao Kanteti  Completed Email service intrsuction
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
include_once(APPLICATION_PATH . '/include/phpmailer.class.php');

class Email extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    private $table_email_job;                                                   /* Table factory class for table email_job */
    private $blank_record = array(
                                  'EmailID' => '',
                                  'Title' => '',
                                  'Message' => '',
                                  'ModifiedUserID' => '',
                                  'ModifiedDate' => ''
                                 );
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

        $this->table = TableFactory::Email();
        $this->table_email_job = TableFactory::EmailJob();
        
        $this->email = new PHPMailer();
        
        $this->email->IsSMTP();          // telling the class to use SMTP
        $this->email->SMTPAuth   = true; // enable SMTP authentication
        $this->email->Host       = $this->controller->config['SMTP']['Host']; 
        $this->email->Port       = $this->controller->config['SMTP']['Port']; 
        $this->email->Username   = $this->controller->config['SMTP']['Username'];
        $this->email->Password   = $this->controller->config['SMTP']['Password'];
        $this->email->Timeout = 30;
        
        $this->host_server = $_SERVER['SERVER_NAME'];
        
        if ($_SERVER['SERVER_PORT'] != '80' && $_SERVER['SERVER_PORT'] != '443') {
            $this->host_server .= ':' . $_SERVER['SERVER_PORT'];
        }
        
        if (defined('SUB_DOMAIN')) {
            $this->host_server .= SUB_DOMAIN;
        }
    }
    
    /**
     * fetchRow
     * 
     * Get a row from the email table based upon arguments passed
     * 
     * @param $args     Criteria for search (EmailID or EmailCode)
     * 
     * @return array    Associative array of matching row
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper.com>  
     **************************************************************************/
    
     public function fetchRow($args) {
         
        if(isset($args['EmailCode'])) 
        {
            $sql = "
                    SELECT 
                            *
                    FROM
                            `email`
                    WHERE
                            EmailCode=:EmailCode
                   ";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));     

            $fetchQuery->execute(array(':EmailCode' => $args['EmailCode']));
        }
        else
        {
            $sql = "
                    SELECT 
                            *
                    FROM
                            `email`
                    WHERE
                            EmailID=:EmailID
                   ";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));     

            $fetchQuery->execute(array(':EmailID' => $args['EmailID']));
        }
        $result = $fetchQuery->fetch();
        
        
        return $result;
     }
     
     
     
     
     /**
     * fetchUserEmail
     * 
     * Get a row from the email_job table based upon arguments passed
     * 
     * @param  string    $MailAccessCode  Criteria for search 
     * 
     * @return array    Associative array of matching row
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper.com>  
     * 
     **************************************************************************/
    
     public function fetchUserEmail($MailAccessCode) {
         
        
        $sql = "
                SELECT 
                        *
                FROM
                        `email_job`
                WHERE
                        MailAccessCode=:MailAccessCode
               ";

        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));     

        $fetchQuery->execute(array(':MailAccessCode' => $MailAccessCode));
      
       
        $result = $fetchQuery->fetch();
        
        
        return $result;
     }
     
     
     
     
     /**
     * fetchLastSentEmail
     * 
     * Get a row from the email_job table based upon arguments passed
     * 
     * @param  string    $EmailID  Criteria for search 
     * 
     * @return array    Associative array of matching row
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper.com>  
     * 
     **************************************************************************/
    
     public function fetchLastSentEmail($EmailID, $JobID) {
         
        
        $sql = "
                SELECT 
                        *
                FROM
                        `email_job`
                WHERE
                        EmailID=:EmailID AND JobID=:JobID
                        
                ORDER BY EmailJobID DESC LIMIT 0,1
               ";

        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));     

        $fetchQuery->execute(array(':EmailID' => $EmailID, ':JobID' => $JobID));
      
       
        $result = $fetchQuery->fetch();
        
        
        return $result;
     }
     
     
     
     
    
    /**
     * getPredefinedEmail
     * 
     * Get the list of predefined e-mails for the job which have not been sent
     * 
     * @param integer $jId  Job ID to get the e-mail list for
     * @param integer $BrandID of Job  - default false
     * 
     * @return array        Associative array of matching records
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getPredefinedEmail($jId, $BrandID) {
        
        
        $email_type_condition = " EmailCode NOT LIKE '%_crm' AND EmailCode NOT LIKE '%_service_tracker' AND ";
        
        //if($BrandID)
        //{
//            $BrandsModel  =   $this->controller->loadModel('Brands');
//            $brand_result = $BrandsModel->fetchRow(array('BrandID'=>$BrandID));
//            
            $jobsModel  =   $this->controller->loadModel('Job');
            $jobResult =  $jobsModel->fetch($jId);
        
            if(isset($jobResult['EmailType']))
            {
                if($jobResult['EmailType']=='CRM')
                {
                    
                    $email_type_condition = " EmailCode LIKE '%_crm' AND ";
                }    
                else if($jobResult['EmailType']=='Tracker')
                {
                    
                    $email_type_condition = " EmailCode LIKE '%_service_tracker' AND ";
                }
                
            }    
        //}     
        
        $sql = "
                SELECT 
			*
		FROM
			`email`
		WHERE
		  EmailCode != 'ra_rejected_branch' AND ".$email_type_condition."	`EMailID` NOT IN (
					  SELECT
						  DISTINCT `EmailID`
				          FROM
					          `email_job`
					  WHERE
						  `JobID` = $jId
					 )
               ";
        
        //$this->log($sql); 
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Emails exist - return all records */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * getSentPredefinedEmail
     * 
     * Get the list of predefined e-mails for the job which have been sent
     * 
     * @param $jId      Job ID to get the e-mail list for
     * @param integer $BrandID of Job  - default false 
     * 
     * @return array    Associative array of matching records
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getSentPredefinedEmail($jId, $BrandID) {
        
        
        $email_type_condition = " EmailCode NOT LIKE '%_crm' AND EmailCode NOT LIKE '%_service_tracker' AND ";
        
      //  if($BrandID)
       // {
          //  $BrandsModel  =   $this->controller->loadModel('Brands');
          //  $brand_result = $BrandsModel->fetchRow(array('BrandID'=>$BrandID));
            
            $jobsModel  =   $this->controller->loadModel('Job');
            $jobResult =  $jobsModel->fetch($jId);
        
            if(isset($jobResult['EmailType']))
            {
                if($jobResult['EmailType']=='CRM')
                {
                    
                    $email_type_condition = " EmailCode LIKE '%_crm' AND ";
                }    
                else if($jobResult['EmailType']=='Tracker')
                {
                    
                    $email_type_condition = " EmailCode LIKE '%_service_tracker' AND ";
                }
                
            }  
            
        //}
        
        
        $sql = "
                SELECT 
			*
		FROM
			`email`
		WHERE
		    ".$email_type_condition."	`EMailID` IN (
					  SELECT
						  `EmailID`
				          FROM
					          `email_job`
					  WHERE
						  `JobID` = $jId
					 )
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Emails exist - return all records */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * hasEmailBeenSent
     * 
     * Return whether a specific e-mail has been sent for a specific job
     * 
     * @param $jId      Job ID to be checked
     *        $eId      Email ID to be checked
     * 
     * @return boolean  Whether the e-mail has been sent
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function hasEmailBeenSent($jId, $eId) {
        $sql = "
                SELECT 
			*
		FROM
			`email_job`
		WHERE
			`EMailID` = $eId
			AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Emails exist - return true */
        } else {
            return(null);                                                       /* Not found - return false */
        }
    }
    
    /**
     * markEmailSent
     * 
     * Set the fact a specific e-mail has been sent for a specific job
     * 
     * @param $jId      Job ID to be checked
     *        $eId      Email ID to be checked
     *        $MailBody final copy of mail body. 
     *        $MailAccessCode this code is used for accesss mail directly from browser.
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function markEmailSent($jId, $emailDetails, $MailBody, $MailAccessCode) {
        $params = array (
            
                         'EmailID' => isset($emailDetails['EmailID'])?$emailDetails['EmailID']:NULL,
                         'JobID' => $jId,
                         'MailBody'=>$MailBody,
                         'MailAccessCode'=>$MailAccessCode,            
                         'ModifiedUserID' => (isset($this->controller->user) && isset($this->controller->user->UserID))?$this->controller->user->UserID:NULL,
                         'ToEmail' => (isset($emailDetails['toEmail']) && $emailDetails['toEmail'])?$emailDetails['toEmail']:NULL,
                         'CCEmails' => (isset($emailDetails['ccEmails']) && $emailDetails['ccEmails'])?$emailDetails['ccEmails']:NULL,
                         'BCCEmails' => (isset($emailDetails['bccEmails']) && $emailDetails['bccEmails'])?$emailDetails['bccEmails']:NULL,
                         'MailSubject' => (isset($emailDetails['mailSubject']) && $emailDetails['mailSubject'])?$emailDetails['mailSubject']:NULL,
                         'FromName' => (isset($emailDetails['fromName']) && $emailDetails['fromName'])?$emailDetails['fromName']:NULL,
                         'FromEmail' => (isset($emailDetails['fromEmail']) && $emailDetails['fromEmail'])?$emailDetails['fromEmail']:NULL,
                         'ReplyEmail' => (isset($emailDetails['replyEmail']) && $emailDetails['replyEmail'])?$emailDetails['replyEmail']:NULL,
                         'ReplyName' => (isset($emailDetails['replyName']) && $emailDetails['replyName'])?$emailDetails['replyName']:NULL
                            
            
                       );

        $cmd = $this->table_email_job->insertCommand( $params );
        $this->Execute($this->conn, $cmd, $params);
    }
    
    /**
    * Send
    * 
    * Send an email
    * (NB: Copied from Noise Website and minor changes made)
    * 
    * @param $name 
    *        $from
    *        $to
    *        $subject
    *        $body
    * 
    * @return 
    * 
    * @author  Brian Etherington <b.etherington@pccsuk.com>  
    **************************************************************************/
    
    public function Send($from_name, $from_email, &$to, $subject, $body, &$cc_email = false, $reply_email = false, $reply_name = false, $bccFlag = false, &$bcc = false) {   
	
        if(isset($this->controller->config['SuperAdmin']['bcc_email']) && $this->controller->config['SuperAdmin']['bcc_email'])
        {
            $bccFlag = true;
            if($bcc)
            {    
                $bcc = $bcc.';'.$this->controller->config['SuperAdmin']['bcc_email'];
            }
            else
            {
                $bcc = $this->controller->config['SuperAdmin']['bcc_email'];
            }
        }    
        
        
        
	if(isset($this->controller->config['dev']['email'])) {
	    $to = $this->controller->config['dev']['email'];
	    $cc_email = false;
	    $bcc = false;
	}	
	
        if(!$reply_email) {
            $reply_email = $from_email;
        }
        if(!$reply_name) {
            $reply_name = $from_name;
        }
        
        $this->email->ClearAllRecipients();
        $this->email->ClearReplyTos();
        $this->email->ClearBCCs();
        
        $this->email->AddReplyTo($reply_email, $reply_name);
        $this->email->SetFrom($from_email, $from_name);
        
        if($cc_email) {
	    $ccArray = explode(";", $cc_email);
	    foreach($ccArray as $cc) {
		$this->email->AddCC($cc);
	    }
	}
        
        $this->email->Subject = $subject;
        $this->email->AltBody = $body;
        $this->email->MsgHTML($body);
	
	//Commented out in order to add multiple emails separated by ;
	//Single email will work just fine
        //$this->email->AddAddress($to);
	$toArray = explode(";", $to);
	foreach($toArray as $email) {
	    $this->email->AddAddress(trim($email));
	}
	
        if($bccFlag) {
	    //We should commment out following line when we finalized bcc mail should be send to sender.
	    //$this->email->AddBCC($from_email);
	    if($bcc) {
		$bccArray = explode(";", $bcc);
		foreach($bccArray as $email) {
		    $this->email->AddBCC(trim($email));
		}
	    }
        }   
        
        $emailResult = $this->email->Send();
        
	//$emailResult = false;
	//$this->controller->log('We should take out comment on send mail functionality when we are ready to send emails.');
        
        
        if (!$emailResult) {
            
            $this->controller->log( 'Mailer Error: ' . $this->email->ErrorInfo
                                   . "\n\nEmail: " . $to . "\n\n" . $body );
        } else {
            if ($this->debug) $this->controller->log('To: ' . $to . "\n\n" 
                                                     .'From: ' . $from_email . "\n\n" 
                                                     .'Subject: ' . $subject . "\n\n" 
                                                     . $body);
        }
        
        return($emailResult);
    }
    
    
    
    
    
     /**
    * prepareCancelledJobEmailBody
    * 
    * Prepares the Cancelled Job email body.
    * 
    * @param string $mailBody
    * @param string $MailSubject 
    * @param array  $Job It contains job details.
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    
    public function prepareCancelledJobEmailBody($mailBody, $MailSubject, $Job) {
        
        if(defined('SUB_DOMAIN')) {
            $sub_domain = SUB_DOMAIN;
        } else {
	    $sub_domain = '';
        }
        
        if(preg_match('/www/', $_SERVER['HTTP_HOST']))
        {
            $wwwString = '';
        }
        else
        {
            $wwwString = 'www.';
        }
        
        if(isset($_SERVER['HTTPS'])) { 
	    //$domain_name = 'https://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
             $domain_name = 'https://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
             
        } else { 
            //$domain_name = 'http://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
            $domain_name = 'http://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } 
        
        $brandLogo = $domain_name . $this->controller->config['Path']['brandLogosPath'];
        
        if($Job['BrandLogo']) {    
            if(!file_exists(APPLICATION_PATH . "/../" . $this->controller->config['Path']['brandLogosPath'] . $Job['BrandLogo'])) {
                $Job['BrandLogo'] = 'default_logo.png';
            }        
            $brandLogo = $brandLogo . $Job['BrandLogo'];
        } else {
            $brandLogo = $brandLogo . 'default_logo.png';
        }
        
        $brandLogoSize = getimagesize($brandLogo);
        
        
        $JobModel = $this->controller->loadModel('Job');
	
        $openJobsStats = $JobModel->fetchServiceProviderOpenStats($Job['ServiceProviderID']);
        
        $SkylineModel = $this->controller->loadModel('Skyline');
        $product_location = $SkylineModel->getProductLocationName($Job['ProductLocation']);
        
        
        
        $customer_address1 = ($Job['CompanyName']) ? $Job['CompanyName'] : '';
        
        if($customer_address1 != '' && $Job['BuildingNameNumber'] != '') {
            $customer_address1 = $customer_address1 . ", " . $Job['BuildingNameNumber'];
        }
        
        if($customer_address1 == '') {
            $customer_address1 = trim($Job['Street']);
            $customer_address2 = trim($Job['LocalArea']);
	    } else {
           $customer_address2 = trim($Job['Street']);
           if($customer_address2 != '' && $Job['LocalArea'] != '') {
               $customer_address2 = $customer_address2 . ", " . trim($Job['LocalArea']);
           }
           else
           {
               $customer_address2 = trim($Job['LocalArea']);
           }
        }
        
        //Customer address array.
        $customerAddressArray = [];
        
        if($customer_address1) {
            $customerAddressArray[] = $customer_address1;
        }
        if($customer_address2) {
            $customerAddressArray[] = $customer_address2;
        }
        if($Job['TownCity']) {
            $customerAddressArray[] = $Job['TownCity'];
        }
        if($Job['CountryName']) {
            $customerAddressArray[] = $Job['CountryName'];
        }
        
        
        $customer_address = implode(', ', $customerAddressArray);
        
        if($Job['PostalCode']) {
            $customer_address = $customer_address.'. '.$Job['PostalCode'];
        }
        $customer_address = str_replace(", ,", ", ", $customer_address);
        
        $PhoneNumber = $Job['ContactHomePhone'];
        
        if($PhoneNumber && $Job['ContactWorkPhoneExt']) {
            $PhoneNumber = $PhoneNumber . "&nbsp;&nbsp;&nbsp;Ex:" . $Job['ContactWorkPhoneExt'];
        }
        
        $Job['ServiceCentreAddress'] = str_replace(', ,', ',', $Job['ServiceCentreAddress']);
        
        $scAddressArray =  explode(",", $Job['ServiceCentreAddress']);
        $scAddCnt = count($scAddressArray);
        $ServiceCentreAddress = '';
        for($s=0;$s<$scAddCnt;$s++)
        {
            $ServiceCentreAddress .= $scAddressArray[$s];
            
            if( ($s==0  || $s==($scAddCnt-3)) && $scAddCnt>1)
            {
                $ServiceCentreAddress .= ', <br>';
            }
            else if($s!=($scAddCnt-1))
            {
                $ServiceCentreAddress .= ',';
            }
            
        }
        
        //$this->log("service prorororor");
        //$this->log($Job['ServiceCentreAddress']);
        
        if(trim($Job['SPAdminSupervisorForename'])=='' || !trim($Job['SPAdminSupervisorForename']))
        {
            $Job['SPAdminSupervisorForename'] = 'Supervisor';
        }    
        
        $placeHolders = [
            
            "[PAGE_TITLE]" => $MailSubject,
            "[CUSTOMER_TITLE_FIRST_LAST_NAME]" => trim($Job['ContactTitle'] . " " . $Job['ContactFirstName'] . " " . $Job['ContactLastName']),
            "[IMAGES_LINK]" => $domain_name . "css/Skins/" . $this->controller->user->Skin . "/images/email/",
            "[BRAND_NAME]" => $Job['Brand'],
            "[BRAND_LOGO]" => $brandLogo,
            "[LINK_VIEW_MAIL_IN_BROWSER]" => $domain_name . "index/viewEmail/" . $Job['MailAccessCode'],
            "[SC_JOB_NUMBER]" => ($Job['ServiceCentreJobNo']) ? "SC" . $Job['ServiceCentreJobNo'] : 'SL' . $Job['JobID'],
            "[BRAND_LOGO_WIDTH]" => $brandLogoSize[0],
            "[BRAND_LOGO_HEIGHT]" => $brandLogoSize[1],
            "[SERVICE_MANAGER]" => trim($Job['SPAdminSupervisorForename']),
            "[SERVICE_CENTRE_ADDRESS]" => $ServiceCentreAddress,
            "[SERVICE_CENTRE_PHONE_NUMBER]" => ($Job['ServiceCentreTelephone']) ? '(T) '.$Job['ServiceCentreTelephone'].'&nbsp;&nbsp;&nbsp;&nbsp;' : '',
            "[SERVICE_CENTRE_EMAIL]" => ($Job['ServiceCentreEmail']) ? $Job['ServiceCentreEmail'] : '',
            "[SERVICE_CENTRE_EMAIL_PREFIX]" => ($Job['ServiceCentreEmail']) ? '(E) ' : '',
            
            "[BOOKED_DATE]" => date("d/m/Y", strtotime($Job['DateBooked'])),
            "[MANUFACTURER_NAME]" => strtoupper($Job['ManufacturerName']),
           // "[PRODUCT_NAME]" => $Job['ProductNo'],
            "[MODEL_NO]" => strtoupper(($Job['ModelNumber']) ? $Job['ModelNumber'] : $Job['ServiceBaseModel']),
            "[SERVICE_TYPE]" => strtoupper($Job['ServiceTypeName']),
            "[PRODUCT_LOCATION]" => strtoupper($product_location),
            "[UNIT_TYPE_NAME]" => strtoupper(($Job['UnitTypeName']) ? $Job['UnitTypeName'] : $Job['ServiceBaseUnitType']),
            "[CUSTOMER_ADDRESS]" => $customer_address,
            "[CUSTOMER_PHONE_NUMBER]" => ($PhoneNumber) ? '(T) '.$PhoneNumber.'&nbsp;&nbsp;&nbsp;&nbsp;' : '',
            "[CUSTOMER_MOBILE_NUMBER]" => ($Job['ContactMobile']) ? '(M) '.$Job['ContactMobile'].'&nbsp;&nbsp;&nbsp;&nbsp;' : '',
            "[CUSTOMER_EMAIL]" => ($Job['ContactEmail']) ? $Job['ContactEmail'] : '',
            "[CUSTOMER_EMAIL_PREFIX]" => ($Job['ContactEmail']) ? '(E) ': ''
            
            
        ];
        
        foreach($placeHolders as $phKey => $phVal) {
            $mailBody = str_replace($phKey, $phVal, $mailBody);
        }
        
        return $mailBody;
    }
    
    
    
    
    /**
    * prepareServiceInstructionEmailBody
    * 
    * Prepares Service Instruction email body.
    * 
    * @param string $mailBody
    * @param string $MailSubject 
    * @param array  $Job It contains job details.
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    
    public function prepareServiceInstructionEmailBody($mailBody, $MailSubject, $Job) {
        
        if(defined('SUB_DOMAIN')) {
            $sub_domain = SUB_DOMAIN;
        } else {
	    $sub_domain = '';
        }
        
        if(preg_match('/www/', $_SERVER['HTTP_HOST']))
        {
            $wwwString = '';
        }
        else
        {
            $wwwString = 'www.';
        }
        
        if(isset($_SERVER['HTTPS'])) { 
	   // $domain_name = 'https://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
           $domain_name = 'https://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } else { 
           // $domain_name = 'http://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
            $domain_name = 'http://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } 
        
        $brandLogo = $domain_name . $this->controller->config['Path']['brandLogosPath'];
        
        if($Job['BrandLogo']) {    
            if(!file_exists(APPLICATION_PATH . "/../" . $this->controller->config['Path']['brandLogosPath'] . $Job['BrandLogo'])) {
                $Job['BrandLogo'] = 'default_logo.png';
            }        
            $brandLogo = $brandLogo . $Job['BrandLogo'];
        } else {
            $brandLogo = $brandLogo . 'default_logo.png';
        }
        
        $brandLogoSize = getimagesize($brandLogo);
        
        
        $JobModel = $this->controller->loadModel('Job');
	
        $openJobsStats = $JobModel->fetchServiceProviderOpenStats($Job['ServiceProviderID']);
        	
        $placeHolders = [
            
            "[PAGE_TITLE]" => $MailSubject,
            "[CUSTOMER_TITLE_FIRST_LAST_NAME]" => trim($Job['ContactTitle'] . " " . $Job['ContactFirstName'] . " " . $Job['ContactLastName']),
            "[IMAGES_LINK]" => $domain_name . "css/Skins/" . $this->controller->user->Skin . "/images/email/",
            "[BRAND_NAME]" => $Job['Brand'],
            "[BRAND_LOGO]" => $brandLogo,
            "[LINK_VIEW_MAIL_IN_BROWSER]" => $domain_name . "index/viewEmail/" . $Job['MailAccessCode'],
            "[SC_JOB_NUMBER]" => ($Job['ServiceCentreJobNo']) ? "SC" . $Job['ServiceCentreJobNo'] : 'SL' . $Job['JobID'],
            "[BRAND_LOGO_WIDTH]" => $brandLogoSize[0],
            "[BRAND_LOGO_HEIGHT]" => $brandLogoSize[1],
            "[SERVICE_MANAGER]" => trim($Job['SPAdminSupervisorForename'].' '.$Job['SPAdminSupervisorSurname']),
            "[7_DAYS_COUNT]" => $openJobsStats['7days'],
            "[7_TO_14_DAYS_COUNT]" => $openJobsStats['7_14days'],
            "[MORETHAN_14_DAYS_COUNT]" => $openJobsStats['14days']
            
        ];
        
        foreach($placeHolders as $phKey => $phVal) {
            $mailBody = str_replace($phKey, $phVal, $mailBody);
        }
        
        return $mailBody;
    }
    
    
     /**
    * sendAppointmentEmail
    * 
    * It selects the Appointment (Booked or Cancelled) email template and data.  And it send it to given email addresses.
    * 
    * @param int $AppointmentID
    * @param string $Type 
    * @param string  $ToEmail
    * @param string  $CCEmails  Emails address should be separated with colons (;). 
    * @param string  $BCCEmails  Emails address should be separated with colons (;).  
    * @param boolean  $checkBrandFlag  If it is true then it sends email based on brand default. 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    public function sendAppointmentEmail($AppointmentID, $Type, $ToEmail, $CCEmails=false, $BCCEmails=false, $checkBrandFlag=true)
    {
            if(($Type=='Booked' || $Type=='Cancelled') && $AppointmentID && $ToEmail)
            {
                // $JobID = 12;
                // $AppointmentID = 1;
                
                
                $Appointment_slot = '';
                $Appointment_date = '';
                
                $AppointmentModel =  $this->controller->loadModel('Appointment');
                $AppointmentDetails = $AppointmentModel->fetchRow(array('AppointmentID'=>$AppointmentID));

                if(isset($AppointmentDetails['AppointmentStartTime']) && isset($AppointmentDetails['AppointmentEndTime']))
                {
                    $Appointment_slot = substr($AppointmentDetails['AppointmentStartTime'],0,strlen($AppointmentDetails['AppointmentStartTime'])-3)." - ".substr($AppointmentDetails['AppointmentEndTime'],0,strlen($AppointmentDetails['AppointmentEndTime'])-3);
                }

                if(isset($AppointmentDetails['AppointmentDate']) && $AppointmentDetails['AppointmentDate'])
                {
                    $Appointment_date = date("d/m/Y", strtotime($AppointmentDetails['AppointmentDate']));
                }

                $ServiceProviderSkillsSetModel = $this->controller->loadModel('ServiceProviderSkillsSet');
                $SkillType = $ServiceProviderSkillsSetModel->getSkillSetName($AppointmentID);

                
                $EmailCode = "appointment_booked";
                if($Type=='Cancelled')
                {
                    $EmailCode = "appointment_cancelled";
                }
                
               
                 if($AppointmentDetails['NonSkylineJobID'])
                 {
                    
                        $NonSkylineJobModel    = $this->controller->loadModel('NonSkylineJob');
                        $NonSkylineJobDetails  = $NonSkylineJobModel->fetchRow(array("NonSkylineJobID"=>$AppointmentDetails['NonSkylineJobID']));
                     
                        if(isset($NonSkylineJobDetails['DateCreated']) && $NonSkylineJobDetails['DateCreated'])
                        {
                            $booking_date = date("Y-m-d", strtotime($NonSkylineJobDetails['DateCreated']));
                        }
                        else
                        {
                            $booking_date = '';
                        }
                        
                        $JobDetails['BrandLogo'] = '2030_logo.png';//Samsung experience document.
                        $JobDetails['CompanyName'] = '';
                        $JobDetails['BuildingNameNumber'] = (isset($NonSkylineJobDetails['CustomerAddress1']))?$NonSkylineJobDetails['CustomerAddress1']:'';
                        $JobDetails['Street'] = (isset($NonSkylineJobDetails['CustomerAddress2']))?$NonSkylineJobDetails['CustomerAddress2']:'';
                        $JobDetails['LocalArea'] = (isset($NonSkylineJobDetails['CustomerAddress3']))?$NonSkylineJobDetails['CustomerAddress3']:'';
                        $JobDetails['TownCity'] = false;
                        $JobDetails['CountryName'] = '';
                        $JobDetails['PostalCode'] = (isset($NonSkylineJobDetails['Postcode']))?$NonSkylineJobDetails['Postcode']:'';
                        
                        $JobDetails['ContactTitle'] = (isset($NonSkylineJobDetails['CustomerTitle']))?$NonSkylineJobDetails['CustomerTitle']:'';
                        $JobDetails['ContactFirstName'] = (isset($NonSkylineJobDetails['CustomerSurname']))?$NonSkylineJobDetails['CustomerSurname']:'';
                        $JobDetails['ContactLastName'] = '';
                        $JobDetails['Brand'] = 'SAMSUNG';
                        $JobDetails['DateBooked'] = $booking_date;
                        $JobDetails['ManufacturerName'] = (isset($NonSkylineJobDetails['Manufacturer']))?$NonSkylineJobDetails['Manufacturer']:'';
                        $JobDetails['ModelNumber'] = (isset($NonSkylineJobDetails['ModelNumber']))?$NonSkylineJobDetails['ModelNumber']:'';
                        $JobDetails['ServiceBaseModel']  = '';
                        $JobDetails['UnitTypeName'] = (isset($NonSkylineJobDetails['ProductType']))?$NonSkylineJobDetails['ProductType']:'';
                        $JobDetails['ServiceBaseUnitType'] = '';
                        $JobDetails['NetworkRefNo'] = (isset($NonSkylineJobDetails['NetworkRefNo']))?$NonSkylineJobDetails['NetworkRefNo']:'';
                        $JobDetails['NonSkylineJobID'] = $AppointmentDetails['NonSkylineJobID'];
                        
                        $BrandsModel    = $this->controller->loadModel('Brands');
                        $BrandDetails   = $BrandsModel->fetchRow(array('BrandID'=>2000));
                        $JobDetails['AutoSendEmails'] = (isset($BrandDetails['AutoSendEmails']))?$BrandDetails['AutoSendEmails']:0;
                        
                 }
                 else
                 {
                      $jobModel = $this->controller->loadModel('Job'); 


                      $jobModel->fetch($AppointmentDetails['JobID']);
                      $JobDetails = $jobModel->current_record; 
                 }
                 
                 
                 
                 
                 $JobDetails['AppointmentID']      = $AppointmentID;
                 $JobDetails['Appointment_slot']   = $Appointment_slot;
                 $JobDetails['Appointment_date']   = $Appointment_date;
                 $JobDetails['SkillType']          = $SkillType;
                 $JobDetails['ServiceProviderID']  = $AppointmentDetails['ServiceProviderID'];
                 
                 

                 

                 $appointment_email  =  $this->fetchRow(['EmailCode' => $EmailCode]);


                 if(!$checkBrandFlag)
                 {
                     $JobDetails['AutoSendEmails'] = 1;
                 }    
                 
                 if(is_array($appointment_email) && count($appointment_email)>0 && $JobDetails['AutoSendEmails']==1)
                 {    
                  //   $this->log("appointment_booked:");
                  //   $this->log($JobDetails);

                     $appointment_email["toEmail"] = $ToEmail;
                     $appointment_email["fromName"] = "Skyline";
                     $appointment_email["fromEmail"] = "no-reply@skylinecms.co.uk";
                     $appointment_email["replyEmail"] = false;
                     $appointment_email["replyName"] = false;
                     $appointment_email["ccEmails"] = $CCEmails;
                     $appointment_email["bccEmails"] = $BCCEmails;

                     $this->processEmail($appointment_email, $JobDetails);

                }

            }
    }
    
    
    
    
    
     /**
    * sendAppointmentDetailsEmail
    * 
    * It selects the Appointment Reminder/Date confirmed mail template and data.  And it sends to given email addresses.
    * 
    * @param int $AppointmentID
    * @param string  $CCEmails  Emails address should be separated with colons (;). 
    * @param string  $BCCEmails  Emails address should be separated with colons (;).  
    * @param boolean  $checkBrandFlag  If it is true then it sends email based on brand default. 
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    public function sendAppointmentDetailsEmail($AppointmentID, $Type, $CCEmails=false, $BCCEmails=false, $checkBrandFlag=true)
    {
            if($AppointmentID && ($Type=="reminder" || $Type=="date_confirmed"))
            {
                
                $Appointment_slot = '';
                $Appointment_date = '';
                
                $AppointmentModel =  $this->controller->loadModel('Appointment');
                $AppointmentDetails = $AppointmentModel->fetchRow(array('AppointmentID'=>$AppointmentID));

                if(isset($AppointmentDetails['AppointmentStartTime']) && isset($AppointmentDetails['AppointmentEndTime']))
                {
                    $Appointment_slot = substr($AppointmentDetails['AppointmentStartTime'],0,strlen($AppointmentDetails['AppointmentStartTime'])-3)." - ".substr($AppointmentDetails['AppointmentEndTime'],0,strlen($AppointmentDetails['AppointmentEndTime'])-3);
                }

                if(isset($AppointmentDetails['AppointmentDate']) && $AppointmentDetails['AppointmentDate'])
                {
                    $Appointment_date = date("d/m/Y", strtotime($AppointmentDetails['AppointmentDate']));
                }

                
                 if($Type=='reminder')
                 {
                     $EmailCode = "appointment_reminder";
                 } 
                 else
                 {
                      $EmailCode = "appointment_date_confirmed";
                 }
                 
               
                 if($AppointmentDetails['NonSkylineJobID'])
                 {
                        
                        if($Type=='reminder')
                        {
                             $EmailCode = "appointment_reminder_smart_service_tracker";
                        } 
                        else
                        {
                             $EmailCode = "appointment_date_confirmed_smart_service_tracker";
                        }
                     
                        
                        
                        $NonSkylineJobModel    = $this->controller->loadModel('NonSkylineJob');
                        $NonSkylineJobDetails  = $NonSkylineJobModel->fetchRow(array("NonSkylineJobID"=>$AppointmentDetails['NonSkylineJobID']));
                     
                        if(isset($NonSkylineJobDetails['DateCreated']) && $NonSkylineJobDetails['DateCreated'])
                        {
                            $booking_date = date("Y-m-d", strtotime($NonSkylineJobDetails['DateCreated']));
                        }
                        else
                        {
                            $booking_date = '';
                        }
                        
                        $JobDetails['BrandLogo'] = '2030_logo.png';//Samsung experience document.
                        $JobDetails['CompanyName'] = '';
                        $JobDetails['BuildingNameNumber'] = (isset($NonSkylineJobDetails['CustomerAddress1']))?$NonSkylineJobDetails['CustomerAddress1']:'';
                        $JobDetails['Street'] = (isset($NonSkylineJobDetails['CustomerAddress2']))?$NonSkylineJobDetails['CustomerAddress2']:'';
                        $JobDetails['LocalArea'] = (isset($NonSkylineJobDetails['CustomerAddress3']))?$NonSkylineJobDetails['CustomerAddress3']:'';
                        $JobDetails['TownCity'] = (isset($NonSkylineJobDetails['CustomerAddress4']))?$NonSkylineJobDetails['CustomerAddress4']:'';
                        $JobDetails['CountryName'] = '';
                        $JobDetails['PostalCode'] = (isset($NonSkylineJobDetails['Postcode']))?$NonSkylineJobDetails['Postcode']:'';
                        
                        $JobDetails['ContactTitle'] = (isset($NonSkylineJobDetails['CustomerTitle']))?$NonSkylineJobDetails['CustomerTitle']:'';
                        $JobDetails['ContactFirstName'] = (isset($NonSkylineJobDetails['CustomerSurname']))?$NonSkylineJobDetails['CustomerSurname']:'';
                        $JobDetails['ContactLastName'] = '';
                        $JobDetails['Brand'] = 'SAMSUNG';
                        $JobDetails['DateBooked'] = $booking_date;
                        $JobDetails['ManufacturerName'] = (isset($NonSkylineJobDetails['Manufacturer']))?$NonSkylineJobDetails['Manufacturer']:'';
                        $JobDetails['ModelNumber'] = (isset($NonSkylineJobDetails['ModelNumber']))?$NonSkylineJobDetails['ModelNumber']:'';
                        $JobDetails['ServiceBaseModel']  = '';
                        $JobDetails['UnitTypeName'] = (isset($NonSkylineJobDetails['ProductType']))?$NonSkylineJobDetails['ProductType']:'';
                        $JobDetails['ServiceBaseUnitType'] = '';
                        $JobDetails['NetworkRefNo'] = (isset($NonSkylineJobDetails['NetworkRefNo']))?$NonSkylineJobDetails['NetworkRefNo']:'';
                        $JobDetails['NonSkylineJobID'] = $AppointmentDetails['NonSkylineJobID'];
                        $JobDetails['ContactEmail'] = $NonSkylineJobDetails['CustomerEmail'];
                        $serviceProviders = $this->controller->loadModel('ServiceProviders');
                        $spDetails = $serviceProviders->fetchRow(array('ServiceProviderID'=>$NonSkylineJobDetails['ServiceProviderID']));
                        $servAddress = $spDetails['CompanyName'].", ";
                        $servAddress.=(isset($spDetails['BuildingNameNumber']) && !empty($spDetails['BuildingNameNumber']))?$spDetails['BuildingNameNumber'].", ":'';
                        $servAddress.=(isset($spDetails['Street']) && !empty($spDetails['Street']))?$spDetails['Street'].", ":'';
                        $servAddress.=(isset($spDetails['LocalArea']) && !empty($spDetails['LocalArea']))?$spDetails['LocalArea'].", ":'';
                        $servAddress.=(isset($spDetails['TownCity']) && !empty($spDetails['TownCity']))?$spDetails['TownCity'].", ":'';
                        $servAddress = substr($servAddress,0,strlen($servAddress)-2);
                        //$servAddress.=".".$spDetails['PostalCode'];
                        $spAddr = $servAddress.". ".$spDetails['PostalCode'];
                        $JobDetails['ServiceCentreAddress'] = $spAddr;
                        $JobDetails['ServiceCentreName'] = $spDetails['CompanyName'];
                        $JobDetails['ServiceCentreTelephone'] = $spDetails['ContactPhone'];
                        $JobDetails['ServiceCentreEmail'] = $spDetails['ContactEmail'];
                        $JobDetails['ServiceTypeName'] = $NonSkylineJobDetails['ServiceType'];
                        $JobDetails['ServiceCentreJobNo'] = $NonSkylineJobDetails['ServiceProviderJobNo'];
                        $JobDetails['ContactHomePhone'] = $NonSkylineJobDetails['CustHomeTelNo'];
                        $JobDetails['ClientID'] = $NonSkylineJobDetails['Client'];
                        $JobDetails['ProductLocation'] = "";
                        $JobDetails['BranchID'] = 2000;
                        $JobDetails['ContactMobile'] = $NonSkylineJobDetails['CustMobileNo'];
                        $JobDetails['ProductNo'] = $NonSkylineJobDetails['ModelNumber'];
                        $BranchesModel    = $this->controller->loadModel('Branches');
                        $BranchDetails   = $BranchesModel->fetchRow(array('BranchID'=>2000));
                        $JobDetails['BranchContactEmail'] = $BranchDetails['ContactEmail'];
                        $JobDetails['ContactWorkPhoneExt'] = "";
                        
                        $BrandsModel    = $this->controller->loadModel('Brands');
                        $BrandDetails   = $BrandsModel->fetchRow(array('BrandID'=>2000));
                        $JobDetails['AutoSendEmails'] = (isset($BrandDetails['AutoSendEmails']))?$BrandDetails['AutoSendEmails']:0;

                 }
                 else
                 {
                      $jobModel = $this->controller->loadModel('Job'); 


                      $jobModel->fetch($AppointmentDetails['JobID']);
                      $JobDetails = $jobModel->current_record; 
                      
                      
                    if($Type=='reminder')
                    {
                        if($JobDetails['EmailType']=="CRM")
                        {
                            $EmailCode = "appointment_reminder_crm";
                        }
                        else if($JobDetails['EmailType']=="Tracker")
                        {
                            $EmailCode = "appointment_reminder_smart_service_tracker";
                        }
                    } 
                    else
                    {
                        if($JobDetails['EmailType']=="CRM")
                        {
                            $EmailCode = "appointment_date_confirmed_crm";
                        }
                        else if($JobDetails['EmailType']=="Tracker")
                        {
                            $EmailCode = "appointment_date_confirmed_smart_service_tracker";
                        }
                    }
                  
                 }
                 
                 
                 
                 
                 $JobDetails['AppointmentID']      = $AppointmentID;
                 $JobDetails['Appointment_slot']   = $Appointment_slot;
                 $JobDetails['Appointment_date']   = $Appointment_date;
                 $JobDetails['SkillType']          = '';
                 $JobDetails['ServiceProviderID']  = $AppointmentDetails['ServiceProviderID'];
                 $JobDetails['CustContactType']    = $AppointmentDetails['CustContactType'];
                 $JobDetails['CustContactTime']  = $AppointmentDetails['CustContactTime'];
                 
                 
                 if(!$checkBrandFlag)
                 {
                     $JobDetails['AutoSendEmails'] = 1;
                 }    
                 
                 if(isset($JobDetails['ContactEmail']) && $JobDetails['ContactEmail'] && $JobDetails['AutoSendEmails']==1)
                 {    
                    $appointment_email  =  $this->fetchRow(['EmailCode' => $EmailCode]);


                    if(is_array($appointment_email) && count($appointment_email)>0)
                    {    
                        $appointment_email["toEmail"] = $JobDetails['ContactEmail'];
                        $appointment_email["fromName"] = "Skyline";
                        $appointment_email["fromEmail"] = "no-reply@skylinecms.co.uk";
                        $appointment_email["replyEmail"] = false;
                        $appointment_email["replyName"] = false;
                       // $appointment_email["ccEmails"] = $CCEmails;
                       // $appointment_email["bccEmails"] = $BCCEmails;
                        $appointment_email["ccEmails"]  = false;
                        $appointment_email["bccEmails"] = false;

                        $this->processEmail($appointment_email, $JobDetails);

                   }
                }

            }
    }
    
    
    
    
    
    
    /**
    * sendAuthorisationEmail
    * 
    * 
    * @param int $JobID
    * @param string $Type 
    * @param string  $CCEmails  Emails address should be separated with colons (;). 
    * @param string  $BCCEmails  Emails address should be separated with colons (;).  
    * @param boolean  $checkBrandFlag  If it is true then it sends email based on brand default.  
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    public function sendAuthorisationEmail($JobID, $Type, $CCEmails=false, $BCCEmails=false, $checkBrandFlag=true)
    {
            if($JobID && ($Type=='unable_to_authorise_repair' || $Type=='query_raised_against_authorisation'))
            {
                
                    $jobModel = $this->controller->loadModel('Job'); 


                    $jobModel->fetch($JobID);
                    $JobDetails = $jobModel->current_record; 
                    
                    
                    $ToEmail = false;
                    
                    if($JobDetails['BranchID'])
                    {
                        $BranchesModel = $this->controller->loadModel('Branches'); 
                        $bdResult = $BranchesModel->fetchRow(array('BranchID'=>$JobDetails['BranchID']));
                        
                        $ToEmail = $bdResult['ContactEmail'];
                    }
                    
                    $JobDetails['AuthEmailFlag'] = true;
                   
                    $authorisation_email  =  $this->fetchRow(['EmailCode' => $Type]);
                    
                    if(!$checkBrandFlag)
                    {
                        $JobDetails['AutoSendEmails'] = 1;
                    }    


                    if(is_array($authorisation_email) && count($authorisation_email)>0 && $ToEmail && $JobDetails['AutoSendEmails']==1)
                    {    
                        $authorisation_email["toEmail"] = $ToEmail;
                        $authorisation_email["fromName"] = "Skyline";
                        $authorisation_email["fromEmail"] = "no-reply@skylinecms.co.uk";
                        $authorisation_email["replyEmail"] = false;
                        $authorisation_email["replyName"] = false;
                        $authorisation_email["ccEmails"] = $CCEmails;
                        $authorisation_email["bccEmails"] = $BCCEmails;

                        $this->processEmail($authorisation_email, $JobDetails);

                   }
                

            }
    }
    
    
    
    
    
    
    /**
    * sendJobCompleteEmail
    * 
    * 
    * @param int $JobID
    * @param string  $CCEmails  Emails address should be separated with colons (;). 
    * @param string  $BCCEmails  Emails address should be separated with colons (;).  
    * @param boolean  $checkBrandFlag  If it is true then it sends email based on brand default.
    * 
    * @return void
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    public function sendJobCompleteEmail($JobID, $CCEmails=false, $BCCEmails=false, $checkBrandFlag=true)
    {
            if($JobID)
            {
                
                 $jobModel = $this->controller->loadModel('Job'); 


                 $jobModel->fetch($JobID);
                 $JobDetails = $jobModel->current_record; 
                 
                 if(isset($JobDetails['ContactEmail']) && $JobDetails['ContactEmail'])
                 {    
                    
                    
                    $JobDetails['JobCompleteEmailFlag'] = true;
                   
                    $job_complete_email  =  $this->fetchRow(['EmailCode' => 'job_complete']);
                    
                    if(!$checkBrandFlag)
                    {
                        $JobDetails['AutoSendEmails'] = 1;
                    }    

                    if(is_array($job_complete_email) && count($job_complete_email)>0 && $JobDetails['AutoSendEmails']==1)
                    {    
                        $job_complete_email["toEmail"] = $JobDetails['ContactEmail'];
                        $job_complete_email["fromName"] = "Skyline";
                        $job_complete_email["fromEmail"] = "no-reply@skylinecms.co.uk";
                        $job_complete_email["replyEmail"] = false;
                        $job_complete_email["replyName"] = false;
                        $job_complete_email["ccEmails"] = $CCEmails;
                        $job_complete_email["bccEmails"] = $BCCEmails;

                        
                        
                        $this->processEmail($job_complete_email, $JobDetails);
                        
                        

                   }
                }

            }
    }
    
    
    
    
    
    /**
    * sendAppraisalRequestEmail
    * 
    * 
    * @param int      $JobID
    * @param string   $CCEmails  Emails address should be separated with colons (;). 
    * @param string   $BCCEmails  Emails address should be separated with colons (;).  
    * @param boolean  $checkBrandFlag  If it is true then it sends email based on brand default. 
    * 
    * @return void
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    public function sendAppraisalRequestEmail($JobID, $CCEmails=false, $BCCEmails=false, $checkBrandFlag=true)
    {
            if($JobID)
            {
                
                 $jobModel = $this->controller->loadModel('Job'); 


                 $jobModel->fetch($JobID);
                 $JobDetails = $jobModel->current_record; 
                 
                 if(isset($JobDetails['ContactEmail']) && $JobDetails['ContactEmail'])
                 {  
                     
                    if(isset($JobDetails['ServiceProviderID']) && $JobDetails['ServiceProviderID'])
                    {
                        $ServiceProvidersModel = $this->controller->loadModel('ServiceProviders'); 
                        $spResult =  $ServiceProvidersModel->fetchRow(array('ServiceProviderID'=>$JobDetails['ServiceProviderID']));
                        
                        if(isset($spResult['AdminSupervisorEmail']) && $spResult['AdminSupervisorEmail'])
                        {
                            if($CCEmails)
                            {    
                                $CCEmails = $spResult['AdminSupervisorEmail'];
                            }
                            else
                            {
                                $CCEmails = $CCEmails.";".$spResult['AdminSupervisorEmail'];
                            }
                        }    
                    }
                     
                    
                    
                    $JobDetails['AppraisalRequestEmailFlag'] = true;
                    
                    if($JobDetails['EmailType']=="CRM")
                    {
                        $EmailCode = "appraisal_request_crm";
                    }
                    else if($JobDetails['EmailType']=="Tracker")
                    {
                        $EmailCode = "appraisal_request_smart_service_tracker";
                    }
                    else
                    {
                        $EmailCode = "appraisal_request";
                    }
                   
                    $appraisal_request_email  =  $this->fetchRow(['EmailCode' => $EmailCode]);
                    
                    if(!$checkBrandFlag)
                    {
                        $JobDetails['AutoSendEmails']  = 1;
                    }    

                    if(is_array($appraisal_request_email) && count($appraisal_request_email)>0 && $JobDetails['AutoSendEmails']==1)
                    {    
                        $appraisal_request_email["toEmail"] = $JobDetails['ContactEmail'];
                        $appraisal_request_email["fromName"] = "Skyline";
                        $appraisal_request_email["fromEmail"] = "no-reply@skylinecms.co.uk";
                        $appraisal_request_email["replyEmail"] = false;
                        $appraisal_request_email["replyName"] = false;
                        $appraisal_request_email["ccEmails"] = $CCEmails;
                        $appraisal_request_email["bccEmails"] = $BCCEmails;

                        
                        
                        $this->processEmail($appraisal_request_email, $JobDetails);
                        
                        

                   }
                }

            }
    }
    
    
    
    
    
    
     /**
    * sendOneTouchJobCancellationEmail
    * 
   * 
    * @param int $JobID
    * @param string $Type (Skyline or NonSkyline)
    * @param string  $ToEmail
    * @param string  $CCEmails  Emails address should be separated with colons (;). 
    * @param string  $BCCEmails  Emails address should be separated with colons (;).  
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    public function sendOneTouchJobCancellationEmail($JobID, $Type, $ToEmail, $CCEmails=false, $BCCEmails=false)
    {
            if($JobID && $ToEmail)
            {
              
                
                $EmailCode = "one_touch_job_cancellation";
                
                 if($Type=="NonSkyline")
                 {
                    
                        $NonSkylineJobModel    = $this->controller->loadModel('NonSkylineJob');
                        $NonSkylineJobDetails  = $NonSkylineJobModel->fetchRow(array("NonSkylineJobID"=>$JobID));
                     
                        if(isset($NonSkylineJobDetails['DateCreated']) && $NonSkylineJobDetails['DateCreated'])
                        {
                            $booking_date = date("Y-m-d", strtotime($NonSkylineJobDetails['DateCreated']));
                        }
                        else
                        {
                            $booking_date = '';
                        }
                        
                        $JobDetails['BrandLogo'] = '2030_logo.png';//Samsung experience document.
                        $JobDetails['CompanyName'] = '';
                        $JobDetails['BuildingNameNumber'] = (isset($NonSkylineJobDetails['CustomerAddress1']))?$NonSkylineJobDetails['CustomerAddress1']:'';
                        $JobDetails['Street'] = (isset($NonSkylineJobDetails['CustomerAddress2']))?$NonSkylineJobDetails['CustomerAddress2']:'';
                        $JobDetails['LocalArea'] = (isset($NonSkylineJobDetails['CustomerAddress3']))?$NonSkylineJobDetails['CustomerAddress3']:'';
                        $JobDetails['TownCity'] = false;
                        $JobDetails['CountryName'] = '';
                        $JobDetails['PostalCode'] = (isset($NonSkylineJobDetails['Postcode']))?$NonSkylineJobDetails['Postcode']:'';
                        
                        $JobDetails['ContactTitle'] = (isset($NonSkylineJobDetails['CustomerTitle']))?$NonSkylineJobDetails['CustomerTitle']:'';
                        $JobDetails['ContactFirstName'] = (isset($NonSkylineJobDetails['CustomerSurname']))?$NonSkylineJobDetails['CustomerSurname']:'';
                        $JobDetails['ContactLastName'] = '';
                        $JobDetails['Brand'] = 'SAMSUNG';
                        $JobDetails['DateBooked'] = $booking_date;
                        $JobDetails['ManufacturerName'] = (isset($NonSkylineJobDetails['Manufacturer']))?$NonSkylineJobDetails['Manufacturer']:'';
                        $JobDetails['ModelNumber'] = (isset($NonSkylineJobDetails['ModelNumber']))?$NonSkylineJobDetails['ModelNumber']:'';
                        $JobDetails['ServiceBaseModel']  = '';
                        $JobDetails['UnitTypeName'] = (isset($NonSkylineJobDetails['ProductType']))?$NonSkylineJobDetails['ProductType']:'';
                        $JobDetails['ServiceBaseUnitType'] = '';
                        $JobDetails['NetworkRefNo'] = (isset($NonSkylineJobDetails['NetworkRefNo']))?$NonSkylineJobDetails['NetworkRefNo']:'';
                        $JobDetails['NonSkylineJobID'] = $JobID;
                        $JobDetails['ServiceProviderID'] = (isset($NonSkylineJobDetails['ServiceProviderID']))?$NonSkylineJobDetails['ServiceProviderID']:'';
                 }
                 else
                 {
                      $jobModel = $this->controller->loadModel('Job'); 


                      $jobModel->fetch($JobID);
                      $JobDetails = $jobModel->current_record; 
                 }
                 
                 
                 
                 
                 $JobDetails['AppointmentID']      = '';
                 $JobDetails['Appointment_slot']   = '';
                 $JobDetails['Appointment_date']   = '';
                 $JobDetails['SkillType']          = '';
                 $JobDetails['ServiceProviderID']  = $JobDetails['ServiceProviderID'];
                 
                 

                 

                 $ot_jobcancellation_email  =  $this->fetchRow(['EmailCode' => $EmailCode]);


                 if(is_array($ot_jobcancellation_email) && count($ot_jobcancellation_email)>0)
                 {    
                 

                     $ot_jobcancellation_email["toEmail"] = $ToEmail;
                     $ot_jobcancellation_email["fromName"] = "Skyline";
                     $ot_jobcancellation_email["fromEmail"] = "no-reply@skylinecms.co.uk";
                     $ot_jobcancellation_email["replyEmail"] = false;
                     $ot_jobcancellation_email["replyName"] = false;
                     $ot_jobcancellation_email["ccEmails"] = $CCEmails;
                     $ot_jobcancellation_email["bccEmails"] = $BCCEmails;

                     $this->processEmail($ot_jobcancellation_email, $JobDetails);

                }

            }
    }
    
    
    
    
    
    /**
    * prepareAppointmentEmailBody
    * 
    * Prepares Appointment (Booked or Cancelled) email body.
    * 
    * @param string $mailBody
    * @param string $MailSubject 
    * @param array  $Job It contains job details.
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    
    public function prepareAppointmentEmailBody($mailBody, $MailSubject, $Job) {
        
        if(defined('SUB_DOMAIN')) {
            $sub_domain = SUB_DOMAIN;
        } else {
	    $sub_domain = '';
        }
        
        if(preg_match('/www/', $_SERVER['HTTP_HOST']))
        {
            $wwwString = '';
        }
        else
        {
            $wwwString = 'www.';
        }
        
        if(isset($_SERVER['HTTPS'])) { 
	    $domain_name = 'https://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } else { 
            $domain_name = 'http://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } 
        
        $brandLogo = $domain_name . $this->controller->config['Path']['brandLogosPath'];
        
//        if($Job['BrandLogo']) {    
//            if(!file_exists(APPLICATION_PATH . "/../" . $this->controller->config['Path']['brandLogosPath'] . $Job['BrandLogo'])) {
//                //$Job['BrandLogo'] = 'default_logo.png';
//                
//                $Job['BrandLogo'] = '2030_logo.png';//Samsung experience logo..Chris asked me to put this on 26/03/2013
//            }        
//            $brandLogo = $brandLogo . $Job['BrandLogo'];
//        } else {
                
            //$brandLogo = $brandLogo . 'default_logo.png';
            $brandLogo = $brandLogo . '2030_logo.png';//Samsung experience logo..Chris asked me to put this on 26/03/2013
        //}
        
        $brandLogoSize = getimagesize($brandLogo);
        
        $app_confirm_receipt = '';
        
        
        if(!strpos(" ", $Job['PostalCode']) && strlen($Job['PostalCode'])>3)
        {
            
            
            $Job['PostalCode'] = wordwrap($Job['PostalCode'], strlen($Job['PostalCode'])-3,' ', true);
  
            
        }        
        
        if(isset($Job['ServiceProviderID']) && $Job['ServiceProviderID']!='')
        {    
            $ServiceProvidersModel = $this->controller->loadModel('ServiceProviders');
            $DiaryType = $ServiceProvidersModel->getServiceCentreDiaryType($Job['ServiceProviderID']);
            
            if($DiaryType=='AllocationOnly')
            {
                $app_confirm_receipt = 'Please click <a href="'.$domain_name .'Diary/confirmAppointmentEmail/?confirmApp='.urlencode(base64_encode($Job['AppointmentID'])).'" target="_blank" >here</a> to confirm receipt of this appointment and acknowledge you will action this appointment.';
            }
            else
            {
               $app_confirm_receipt = 'Please be aware that this job may take up to 15 minutes to appear in ServiceBase.';
            }
        }    
      
        
        $customer_address1 = ($Job['CompanyName']) ? $Job['CompanyName'] : '';
        
        if($customer_address1 != '' && $Job['BuildingNameNumber'] != '') {
            $customer_address1 = $customer_address1 . ", " . $Job['BuildingNameNumber'];
        }
        else
        {
            $customer_address1 = $Job['BuildingNameNumber'];
        }
        
        if($customer_address1 == '') {
            $customer_address1 = trim($Job['Street']);
            $customer_address2 = trim($Job['LocalArea']);
	    } else {
           $customer_address2 = trim($Job['Street']);
           if($customer_address2 != '' && $Job['LocalArea'] != '') {
               $customer_address2 = $customer_address2 . ", " . trim($Job['LocalArea']);
           }
           else 
           {
               $customer_address2 =  trim($Job['LocalArea']);
           }
        }
        
        //Customer address array.
        $customerAddressArray = [];
        
        if($customer_address1) {
            $customerAddressArray[] = $customer_address1;
        }
        if($customer_address2) {
            $customerAddressArray[] = $customer_address2;
        }
        if($Job['TownCity']) {
            $customerAddressArray[] = $Job['TownCity'];
        }
        if($Job['CountryName']) {
            $customerAddressArray[] = $Job['CountryName'];
        }
        
        
        $customer_address = implode(', ', $customerAddressArray);
        
        if($Job['PostalCode']) {
            $customer_address = $customer_address.'. '.$Job['PostalCode'];
        }
        $customer_address = str_replace(", ,", ", ", $customer_address);
        $SkinName = 'Skyline';
        if(isset($this->controller->user->Skin))
        {
            $SkinName = $this->controller->user->Skin;
        }    
        
        
        $unitTypeHtml = strtoupper(($Job['UnitTypeName']) ? $Job['UnitTypeName'] : $Job['ServiceBaseUnitType']);
        
        if($unitTypeHtml && $unitTypeHtml!='UNKNOWN')
        {
            $unitTypeHtml = "Product: ".$unitTypeHtml."<BR>";
        }   
        
        $ManufacturerNameHtml =  strtoupper($Job['ManufacturerName']);
       
        if($ManufacturerNameHtml)
        {
           $ManufacturerNameHtml = "Manufacturer: ".$ManufacturerNameHtml."<br>";
        }
        
        $Job['Brand'] = 'SAMSUNG';//Chris asked me to put this on 26/03/2013
        
        $placeHolders = [
            
            "[PAGE_TITLE]" => $MailSubject,
            "[CUSTOMER_TITLE_FIRST_LAST_NAME]" => trim($Job['ContactTitle'] . " " . $Job['ContactFirstName'] . " " . $Job['ContactLastName']),
            "[IMAGES_LINK]" => $domain_name . "css/Skins/" . $SkinName . "/images/email/",
            "[BRAND_NAME]" => $Job['Brand'],
            "[BRAND_LOGO]" => $brandLogo,
            "[LINK_VIEW_MAIL_IN_BROWSER]" => $domain_name . "index/viewEmail/" . $Job['MailAccessCode'],
            "[BRAND_LOGO_WIDTH]" => $brandLogoSize[0],
            "[BRAND_LOGO_HEIGHT]" => $brandLogoSize[1],
            "[BOOKED_DATE]" => ($Job['DateBooked'])?date("d/m/Y", strtotime($Job['DateBooked'])):' -- ',
            "[MANUFACTURER_NAME]" => $ManufacturerNameHtml,
            "[MODEL_NO]" => strtoupper(($Job['ModelNumber']) ? $Job['ModelNumber'] : $Job['ServiceBaseModel']),
            "[UNIT_TYPE_NAME]" => $unitTypeHtml,
            "[CUSTOMER_ADDRESS]" => $customer_address,
            "[ORDER_REFERENCE_NO]" => $Job['NetworkRefNo'], 
            "[SKILL_TYPE]" => $Job['SkillType'], 
            "[APPOINTMENT_SLOT]" => $Job['Appointment_slot'],
            "[APPOINTMENT_DATE]" => $Job['Appointment_date'],
            "[POSTCODE]" => $Job['PostalCode'],
            "[SKYLINE_LOGIN_LINK]" => $domain_name.'Login',
            "[APP_CONFIRM_RECEIPT]" => $app_confirm_receipt
            
            
        ];
        
        foreach($placeHolders as $phKey => $phVal) {
            $mailBody = str_replace($phKey, $phVal, $mailBody);
        }
        
        return $mailBody;
    }
    
    
    
    /**
    * prepareJobBookingEmailBody
    * 
    * Prepares the job booking email body.
    * 
    * @param string $mailBody
    * @param string $MailSubject 
    * @param array  $Job It contains job details.
    * 
    * @return string $mailBody
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    **************************************************************************/
    
    public function prepareJobBookingEmailBody($mailBody, $MailSubject, $Job) {
        
	if(defined('SUB_DOMAIN')) {
            $sub_domain = SUB_DOMAIN;
        } else {
	    $sub_domain = '';
        }
        
        
        if(preg_match('/www/', $_SERVER['HTTP_HOST']))
        {
            $wwwString = '';
        }
        else
        {
            $wwwString = 'www.';
        }
        
        
        if(isset($_SERVER['HTTPS'])) { 
	    //$domain_name = 'https://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
            $domain_name = 'https://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } else { 
          //  $domain_name = 'http://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
            $domain_name = 'http://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
        } 
               
        $PhoneNumber = $Job['ContactHomePhone'];
        
        if($PhoneNumber && $Job['ContactWorkPhoneExt']) {
            $PhoneNumber = $PhoneNumber . "&nbsp;&nbsp;&nbsp;Ex:" . $Job['ContactWorkPhoneExt'];
        }
        
        $Job['ServiceCentreAddress'] = str_replace(', ,', ',', $Job['ServiceCentreAddress']);
        
//        $SCAddress = explode(",", $Job['ServiceCentreAddress']);
//        $SCAddressArray = [];
//        
//        $sp_address1 = isset($SCAddress[1]) ? $SCAddress[1] : '';
//        
//        $sp_address1 = $sp_address1 . " " . (isset($SCAddress[2]) ? $SCAddress[2] : '');
//        $sp_address1 = trim($sp_address1);
//        
//        if($sp_address1) {
//            $SCAddressArray[] = $sp_address1;
//        }
//        if(isset($SCAddress[3]) && $SCAddress[3] != '') {
//            $SCAddressArray[] = $SCAddress[3];
//        }     
//	if(isset($SCAddress[4]) && $SCAddress[4] != '') {
//            $SCAddressArray[] = $SCAddress[4];
//        }
//        if(isset($SCAddress[5]) && $SCAddress[5] != '') {
//            $SCAddressArray[] = $SCAddress[5];
//        }
        
	$ServiceProvidersModel = $this->controller->loadModel('ServiceProviders');
	$spClientContacts = $ServiceProvidersModel->getServiceProviderClientContacts($Job['ServiceProviderID'], $Job['ClientID']);
               
	if(is_array($spClientContacts)) {

	   if(isset($spClientContacts[0]['ContactPhone']) && $spClientContacts[0]['ContactPhone']) {
	      $Job['ServiceCentreTelephone']  = $spClientContacts[0]['ContactPhone'];
	   }

	   if(isset($spClientContacts[0]['ContactEmail']) && $spClientContacts[0]['ContactEmail']) {
	      $Job['ServiceCentreEmail']  = $spClientContacts[0]['ContactEmail'];
	   }
	   
	}  
       
//        $SCAddressArray[] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(T)</span>' . $Job['ServiceCentreTelephone'];
//        $SCAddressArray[] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(E)</span><a href="mailto:' . 
//			    $Job['ServiceCentreEmail'] . '" target="_blank" >'.$Job['ServiceCentreEmail'] . '</a>';
//        
        $customer_address1 = ($Job['CompanyName']) ? $Job['CompanyName'] : '';
        
        if($customer_address1 != '' && $Job['BuildingNameNumber'] != '') {
            $customer_address1 = $customer_address1 . ", " . $Job['BuildingNameNumber'];
        }
        
        if($customer_address1 == '') {
            $customer_address1 = trim($Job['Street']);
            $customer_address2 = trim($Job['LocalArea']);
	} else {
           $customer_address2 = trim($Job['Street']);
           if($customer_address2 != '' && $Job['LocalArea'] != '') {
               $customer_address2 = $customer_address2 . ", " . trim($Job['LocalArea']);
           }
           else
           {
               $customer_address2 = trim($Job['LocalArea']);
           }
        }
        
        $brandLogo = $domain_name . $this->controller->config['Path']['brandLogosPath'];
        
        if($Job['BrandLogo']) {    
            if(!file_exists(APPLICATION_PATH . "/../" . $this->controller->config['Path']['brandLogosPath'] . $Job['BrandLogo'])) {
                $Job['BrandLogo'] = 'default_logo.png';
            }        
            $brandLogo = $brandLogo . $Job['BrandLogo'];
        } else {
            $brandLogo = $brandLogo . 'default_logo.png';
        }
          
          $brandLogoSize = getimagesize($brandLogo);
        
      //  $product_location = '';
//        if($Job['ProductLocation']=="Branch")
//        {
//            $product_location = $Job['Branch'];
//        }   
//        else if($Job['ProductLocation']=="Customer")
//        {
//            $product_location = trim($Job['ContactTitle']." ".$Job['ContactFirstName']." ".$Job['ContactLastName']);
//        } 
//        else if($Job['ProductLocation']=="Service Provider")
//        {
//            $product_location = $Job['ServiceCentreName'];
//        }
//        else if($Job['ProductLocation']=="Other")
//        {
//            
//            $product_location  = $Job['ColAddCompanyName'];
//            $collectionAddress = array("ColAddBuildingNameNumber", "ColAddStreet", "ColAddLocalArea", "ColAddTownCity", "ColAddPostcode");
//            
//            foreach($collectionAddress as $ca)
//            {
//                if($Job[$ca])
//                {
//                    if($product_location)
//                    {
//                        $product_location .= ", ".$Job[$ca];
//                    }
//                    else
//                    {
//                        $product_location .= $Job[$ca];
//                    }    
//                }
//            }
//                
//        }
        
        
        $SkylineModel = $this->controller->loadModel('Skyline');
        $product_location = $SkylineModel->getProductLocationName($Job['ProductLocation']);
        
	//Customer address array.
        $customerAddressArray = [];
        
        if($customer_address1) {
            $customerAddressArray[] = $customer_address1;
        }
        if($customer_address2) {
            $customerAddressArray[] = $customer_address2;
        }
        if($Job['TownCity']) {
            $customerAddressArray[] = $Job['TownCity'];
        }
        if($Job['CountryName']) {
            $customerAddressArray[] = $Job['CountryName'];
        }
//        if($Job['PostalCode']) {
//            $customerAddressArray[] = $Job['PostalCode'];
//        }
        
        
         $customer_address = implode(', ', $customerAddressArray);
        
        if($Job['PostalCode']) {
            $customer_address = $customer_address.'. '.$Job['PostalCode'];
        }
        $customer_address = str_replace(", ,", ", ", $customer_address);
	$branchModel = $this->controller->loadModel("Branches");
	$branch = $branchModel->getBranchByID($Job["BranchID"]);
        
        
        $scAddressArray =  explode(",", $Job['ServiceCentreAddress']);
        $scAddCnt = count($scAddressArray);
        $ServiceCentreAddress = '';
        for($s=0;$s<$scAddCnt;$s++)
        {
            $ServiceCentreAddress .= $scAddressArray[$s];
            
            if( ($s==0  || $s==($scAddCnt-3)) && $scAddCnt>1)
            {
                $ServiceCentreAddress .= ', <br>';
            }
            else if($s!=($scAddCnt-1))
            {
                $ServiceCentreAddress .= ',';
            }
            
        }
        
        $Appointment_slot = '';
        /*if(isset($Job['Appointment_slot']))
        {    
           if(strtoupper($Job['Appointment_slot'])=='AM')
           {
               $Appointment_slot = '09:00-12:00';
           }
           else if(strtoupper($Job['Appointment_slot'])=='PM')
           {
               $Appointment_slot = '13:00-17:00';
           }
           else if(strtoupper($Job['Appointment_slot'])=='AM/PM')
           {
               $Appointment_slot = '09:00-12:00 or 13:00-17:00';
           }
           else
           {
               $Appointment_slot = $Job['Appointment_slot'];
           }
        }*/
        if(isset($Job['Appointment_slot']))
            $Appointment_slot = $Job['Appointment_slot'];
        if(isset($Job['CustContactType']) && $Job['CustContactType'] != "None")
        {
            if($Job['CustContactType'] == "SMS")
                $type = "TXT";
            else
                $type = $Job['CustContactType'];
            $preVisitTxt = '<p style="font-size:13px; color:#444444;line-height:115%;font-family: Calibri;">We will confirm the actual appointment time by '.$type.' '.$Job['CustContactTime'].' minutes before we are due to arrive.';
        }
        else
            $preVisitTxt = '';
        $placeHolders = [
            "[PAGE_TITLE]" => $MailSubject,
            "[CUSTOMER_TITLE_SURNAME]" => trim($Job['ContactTitle'] . " " . $Job['ContactLastName']),
            "[CUSTOMER_WEBSITE_NAME]" => (isset($Job['CUSTOMER_WEBSITE_NAME']))?$Job['CUSTOMER_WEBSITE_NAME']:'',
            "[CUSTOMER_WEBSITE_URL]" => (isset($Job['CUSTOMER_WEBSITE_URL']))?$Job['CUSTOMER_WEBSITE_URL']:'',
            "[CUSTOMER_TITLE_FIRST_LAST_NAME]" => trim($Job['ContactTitle'] . " " . $Job['ContactFirstName'] . " " . $Job['ContactLastName']),
            
          //  "[CUSTOMER_ADDRESS1]" => isset($customerAddressArray[0]) ? $customerAddressArray[0] : '&nbsp;',
          //  "[CUSTOMER_ADDRESS2]" => isset($customerAddressArray[1]) ? $customerAddressArray[1] : '&nbsp;',
          //  "[CUSTOMER_ADDRESS3]" => isset($customerAddressArray[2]) ? $customerAddressArray[2] : '&nbsp;',
          //  "[CUSTOMER_ADDRESS4]" => isset($customerAddressArray[3]) ? $customerAddressArray[3] : '&nbsp;',
          //  "[CUSTOMER_ADDRESS5]" => isset($customerAddressArray[4]) ? $customerAddressArray[4] : '&nbsp;',
            "[CUSTOMER_ADDRESS]" => $customer_address,
            "[CUSTOMER_PHONE_NO]" => ($PhoneNumber) ? '(T) '.$PhoneNumber.'&nbsp;&nbsp;&nbsp;&nbsp;' : '',
            "[CUSTOMER_MOBILE_NO]" => ($Job['ContactMobile']) ? '(M) '.$Job['ContactMobile'].'&nbsp;&nbsp;&nbsp;&nbsp;' : '',
            "[CUSTOMER_EMAIL]" => ($Job['ContactEmail'])?$Job['ContactEmail']:'',
            "[CUSTOMER_EMAIL_PREFIX]" => ($Job['ContactEmail']) ? '(E) ': '',
            "[SERVICE_PROVIDER_NAME]" => $Job['ServiceCentreName'],
            "[SERVICE_CENTRE_ADDRESS]" => $ServiceCentreAddress,
            
          //  "[SERVICE_PROVIDER_ADDRESS1]" => isset($SCAddressArray[0]) ? strtoupper($SCAddressArray[0]) : '&nbsp;',
          //  "[SERVICE_PROVIDER_ADDRESS2]" => isset($SCAddressArray[1]) ? strtoupper($SCAddressArray[1]) : '&nbsp;',
          //  "[SERVICE_PROVIDER_ADDRESS3]" => isset($SCAddressArray[2]) ? strtoupper($SCAddressArray[2]) : '&nbsp;',
          //  "[SERVICE_PROVIDER_POSTCODE]" => isset($SCAddressArray[3]) ? strtoupper($SCAddressArray[3]) : '&nbsp;',
         //   "[SERVICE_PROVIDER_TELNO_HTML]" => isset($SCAddressArray[4]) ? strtoupper($SCAddressArray[4]) : '&nbsp;',
         //   "[SERVICE_PROVIDER_EMAIL_HTML]" => isset($SCAddressArray[5]) ? strtoupper($SCAddressArray[5]) : '&nbsp;',
              "[SERVICE_PROVIDER_TELNO_HTML]" => ($Job['ServiceCentreTelephone']) ? '(T) '.$Job['ServiceCentreTelephone'].'&nbsp;&nbsp;&nbsp;&nbsp;' : '',
            
              "[SERVICE_CENTRE_EMAIL_PREFIX]" => ($Job['ServiceCentreEmail']) ? '(E) ' : '',
          
            
            "[SERVICE_PROVIDER_TELNO]" => $Job['ServiceCentreTelephone'],
            "[SERVICE_PROVIDER_EMAIL]" => ($Job['ServiceCentreEmail']) ? $Job['ServiceCentreEmail'] : '',
            
            "[JOB_NUMBER]" => "SL" . $Job['JobID'],
            "[BOOKED_DATE]" => date("d/m/Y", strtotime($Job['DateBooked'])),
            "[MANUFACTURER_NAME]" => strtoupper($Job['ManufacturerName']),
            "[PRODUCT_NAME]" => $Job['ProductNo'],
            "[MODEL_NO]" => strtoupper(($Job['ModelNumber']) ? $Job['ModelNumber'] : $Job['ServiceBaseModel']),
            "[SERVICE_TYPE]" => strtoupper($Job['ServiceTypeName']),
            "[PRODUCT_LOCATION]" => strtoupper($product_location),
            "[IMAGES_LINK]" => $domain_name . "css/Skins/" . $this->controller->user->Skin . "/images/email/",
            "[BRAND_NAME]" => $Job['Brand'],
            "[BRAND_LOGO]" => $brandLogo,
            "[LINK_VIEW_MAIL_IN_BROWSER]" => $domain_name . "index/viewEmail/" . $Job['MailAccessCode'],
            "[SC_JOB_NUMBER]" => ($Job['ServiceCentreJobNo']) ? "SC" . $Job['ServiceCentreJobNo'] : 'SL' . $Job['JobID'],
            "[BRAND_LOGO_WIDTH]" => $brandLogoSize[0],
            "[BRAND_LOGO_HEIGHT]" => $brandLogoSize[1],
            "[UNIT_TYPE_NAME]" => strtoupper(($Job['UnitTypeName']) ? $Job['UnitTypeName'] : $Job['ServiceBaseUnitType']),
	    "[SERVICE_MANAGER]" => (isset($branch[0]["ServiceManager"]))?$branch[0]["ServiceManager"]: '',
            "[BRANCH_EMAIL]" => ($Job['BranchContactEmail']) ? $Job['BranchContactEmail'] : '',
            "[APPOINTMENT_DATE]" => (isset($Job['Appointment_date'])) ? $Job['Appointment_date'] : '',
            "[TIME_SLOT]" => $Appointment_slot,
            "[CUST_CONTACT_TYPE]" => (isset($Job['CustContactType'])) ? $Job['CustContactType'] : '',
            "[CUST_CONTACT_TIME]" => (isset($Job['CustContactTime'])) ? $Job['CustContactTime'] : '',
            "[PRE_VISIT_INFO]" => $preVisitTxt
        ];
        
        
        
        
        if(isset($Job['AppraisalRequestEmailFlag']))
        {
            
            $placeHolders['[BRANCH_NAME]'] = $Job['Branch'];
        }
        
        if(isset($Job['JobCompleteEmailFlag']))
        {
            $placeHolders['[NETWORK_EMAIL]'] = $Job['NetworkContactEmail'];
            $QuestionnaireModel = $this->controller->loadModel('Questionnaire');            
            //change brand name to brandid questionnaire log insertion...
            $guid = $QuestionnaireModel->AddLog( $Job['JobID'], $Job['JobBrandID'], 'A0001' );                             
            $placeHolders['[SERVICE_QUESTIONNAIRE]'] = $domain_name.'questionnaire/'.$guid;
                       
        }
        
        
        if(isset($Job['AuthEmailFlag']))
        {
            $manufacturerLogo = $domain_name . $this->controller->config['Path']['manufacturerLogosPath'];
            
            
            if($Job['ManufacturerLogo']) 
            {    
                if(!file_exists(APPLICATION_PATH . "/../" . $this->controller->config['Path']['manufacturerLogosPath'] . $Job['ManufacturerLogo'])) 
                {
                    $Job['ManufacturerLogo'] = 'no_logo.png';
                }        
                
                $manufacturerLogo = $manufacturerLogo . $Job['ManufacturerLogo'];
                
            } 
            else 
            {
                $manufacturerLogo = $manufacturerLogo . 'no_logo.png';
            }
        
            $manufacturerLogoSize = getimagesize($manufacturerLogo);
            
            
            $placeHolders['[MANUFACTURER_LOGO]'] = $manufacturerLogo;
            
            $placeHolders['[MANUFACTURER_LOGO_WIDTH]']  = $manufacturerLogoSize[0];
            $placeHolders['[MANUFACTURER_LOGO_HEIGHT]'] = $manufacturerLogoSize[1];
            $placeHolders['[WA_MANAGER_NAME]'] = 'Test Name';
            $placeHolders['[WA_MANAGER_EMAIL]'] = 'test@test.com';
            
        }
        
        foreach($placeHolders as $phKey => $phVal) {
            $mailBody = str_replace($phKey, $phVal, $mailBody);
        }
        
        return $mailBody;
	
    }
    

    
    public function processEmail($email, $job) {
	
        if(isset($email['notCheckBrandFlag']) && $email['notCheckBrandFlag'])
        {
           $job['AutoSendEmails'] = 1; 
        }
        else if (!isset($job['AutoSendEmails']))
        {
            $job['AutoSendEmails'] = 1; 
        }
        
        if($job['AutoSendEmails']==1)
        {

            $mailSubject = trim($job['Brand'] . " " . $email['Title']);

            if(isset($job['NonSkylineJobID']))
            {
                $mailAccessCode = md5($job["NonSkylineJobID"] . $email['EmailID']);
            }    
            else
            {
                $mailAccessCode = md5($job["JobID"] . $email['EmailID']);
            }


            $job['MailAccessCode'] = $mailAccessCode;

            $mailSubject = str_replace('<customer>', trim($job['ContactTitle'] . " " . $job['ContactLastName']), $mailSubject);
            
            if(!isset($job['JobID']))
            {
                $job['JobID'] = '';
            }
            
            $mailSubject = str_replace('<Job No>', (isset($job['ServiceCentreJobNo']) && $job['ServiceCentreJobNo']) ? "SC" . $job['ServiceCentreJobNo'] : 'SL' . $job['JobID'], $mailSubject);


            if($email['EmailCode']=='appointment_booked' || $email['EmailCode']=='appointment_cancelled' || $email['EmailCode']=='one_touch_job_cancellation')
            {
                if($email['EmailCode']=='appointment_booked' || $email['EmailCode']=='appointment_cancelled')
                {
                    if($job['ServiceProviderID'])
                    {
                        $ServiceProvidersModel   =  $this->controller->loadModel('ServiceProviders');
                        $scDetails               =  $ServiceProvidersModel->fetchRow(array('ServiceProviderID'=>$job['ServiceProviderID']));

                        if(isset($scDetails['CompanyName']) && $scDetails['CompanyName'])
                        {
                            $mailSubject = $mailSubject." - ".$scDetails['CompanyName'];
                        }    
                    }    
                }

                $mailBody    = self::prepareAppointmentEmailBody($email['Message'], $mailSubject, $job);

            }
            else if($email['EmailCode']=='email_service_instruction')//Email service instruction email
            {
                $mailBody = self::prepareServiceInstructionEmailBody($email['Message'], $mailSubject, $job);

            }
            else if($email['EmailCode']=='cancelled_job_notification')//Cancelled Job Notification
            {
                $mailBody = self::prepareCancelledJobEmailBody($email['Message'], $mailSubject, $job);

            }
            else
            {


                if($email['EmailCode']=='job_booking' || $email['EmailCode']=='job_booking_crm' || $email['EmailCode']=='job_booking_smart_service_tracker')
                {    
                    if(isset($job['ServiceProviderID']) && $job['ServiceProviderID'])
                    {
                        $ServiceProvidersModel = $this->controller->loadModel('ServiceProviders'); 
                        $spResult =  $ServiceProvidersModel->fetchRow(array('ServiceProviderID'=>$job['ServiceProviderID']));

                        if(isset($spResult['AdminSupervisorEmail']) && $spResult['AdminSupervisorEmail'])
                        {
                            if($email["ccEmails"])
                            {    
                                $email["ccEmails"] = $spResult['AdminSupervisorEmail'];
                            }
                            else
                            {
                                $email["ccEmails"] = $email["ccEmails"].";".$spResult['AdminSupervisorEmail'];
                            }
                        }    
                    }
                }




                if($email['EmailCode']=='job_booking_crm' || $email['EmailCode']=='appointment_reminder_crm'  || $email['EmailCode']=='appointment_date_confirmed_crm' || $email['EmailCode']=='appraisal_request_crm')
                {
                    $job['CUSTOMER_WEBSITE_URL']  = 'http://www.allyours.co';
                    $job['CUSTOMER_WEBSITE_NAME'] = 'all yours';
                }
                else if($email['EmailCode']=='job_booking_smart_service_tracker' 
                        || $email['EmailCode']=='appointment_reminder_smart_service_tracker' 
                        || $email['EmailCode']=='appointment_date_confirmed_smart_service_tracker'
                        || $email['EmailCode']=='appraisal_request_smart_service_tracker'
                        )
                {    
                    $job['CUSTOMER_WEBSITE_URL']  = (isset($job['TrackerURL']))?$job['TrackerURL']:'';
                    $job['CUSTOMER_WEBSITE_NAME'] = (isset($job['TrackerURL']))?$job['TrackerURL']:'';

                }

                $mailBody = self::prepareJobBookingEmailBody($email['Message'], $mailSubject, $job);
            }

            $ccEmail = false;
            $bccEmails = false;

            if(isset($email["bccEmails"]) && $email["bccEmails"])
            {
                $bccEmails = $email["bccEmails"];
            }    

            $emailResult = self::Send(
                $email["fromName"],	    //from name
                $email["fromEmail"],    //from email
                $email["toEmail"],	    //to email
                $mailSubject,	    //subject
                $mailBody,		    //body
                $email["ccEmails"],	    //cc emails
                $email["replyEmail"],   //reply email
                $email["replyName"],    //reply name
                true,		    //bcc flag,
                $bccEmails 	    //bcc emails    
            );

            if($emailResult) {    
               //Mark the e-mail as sent
                if(isset($job["JobID"]) && isset($this->controller->user))
                {    
                    $email['mailSubject'] = $mailSubject;
                    self::markEmailSent($job["JobID"], $email, $mailBody, $mailAccessCode);
                }
            } 
        
        
        }
    }
    
    
    
}

?>
