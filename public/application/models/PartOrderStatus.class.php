<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartOrderStatuss Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */

class PartOrderStatus extends CustomModel {
    
   
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
   ////part_status functions 
    public function getPartOrderStatusList($filter=false){
        
       if($filter=="addPart"){
           $filter=" and PartStatusID in (2,3,6,11,17,4)";
       }
            $sql="select * from part_status where Status='Active' $filter order by PartStatusDisplayOrder asc";
     
        $res=$this->query( $this->conn, $sql); 
        return $res;
    }
    
    
    public function insertPartOrderStatus($p){
        $sql="insert into part_status (PartStatusName,PartStatusDescription,ServiceProviderID,PartStatusDisplayOrder,DisplayOrderSection,Status)
    values
    (:PartStatusName,:PartStatusDescription,:ServiceProviderID,:PartStatusDisplayOrder,:DisplayOrderSection,:Status)
    ";
        $params=array(
            'PartStatusName'=>$p['PartStatusName'],
            'PartStatusDescription'=>$p['PartStatusDescription'],
            'ServiceProviderID'=>$p['ServiceProviderID'],
            'PartStatusDisplayOrder'=>'',
            'DisplayOrderSection'=>isset($p['DisplayOrderSection'])?'Yes':'No',
            'Status'=>isset($p['Status'])?'In-active':'Active',
        
            
        );
       $this->execute( $this->conn, $sql,$params);  
    }
    
    public function updatePartOrderStatus($p){
          $sql="update part_status set PartStatusName=:PartStatusName,PartStatusDescription=:PartStatusDescription,ServiceProviderID=:ServiceProviderID,PartStatusDisplayOrder=:PartStatusDisplayOrder,DisplayOrderSection=:DisplayOrderSection,Status=:Status
    where PartStatusID=:PartStatusID;
    ";
         $params=array(
            'PartStatusName'=>$p['PartStatusName'],
            'PartStatusDescription'=>$p['PartStatusDescription'],
            'ServiceProviderID'=>$p['ServiceProviderID'],
            'PartStatusDisplayOrder'=>'',
            'PartStatusID'=>$p['PartStatusID'],
            'DisplayOrderSection'=>isset($p['DisplayOrderSection'])?'Yes':'No',
            'Status'=>isset($p['Status'])?'In-active':'Active',
        
            
        );
       $this->execute( $this->conn, $sql,$params); 
    }
    
    public function getPartOrderStatusData($id){
        $sql="select * from part_status where PartStatusID=$id";
        $res=$this->query( $this->conn, $sql); 
        return $res[0];
    }
  
    public function deletePartOrderStatus($id){
        $sql="update part_status set Status='In-Active' where PartStatusID=$id";
        $this->execute( $this->conn, $sql); 
    }
    ////part_status functions 
    
    public function checkIfOrderSection($id){
        $sql="select DisplayOrderSection from part_status where PartStatusID=$id";
         $res=$this->query( $this->conn, $sql); 
         return $res[0]['DisplayOrderSection'];
    }
    
    
    
}
?>