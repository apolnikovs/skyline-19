<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of RepairType Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class RepairTypes extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            
            "LocationName",
            "MainStore",
            "RepairSite",
            "UseStockAllocation",
            "OutWarrantyMarkupPerc",
            "Status"
            
           
            
        ];
    }

    public function insertRepairType($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbInsert('service_provider_part_location', $this->fields, $P, true, true);
        return $id;
    }

    public function updateRepairType($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbUpdate('service_provider_part_location', $this->fields, $P, "ServiceProviderRepairTypeLocationID=" . $P['ServiceProviderRepairTypeLocationID'], true);
    }

    public function getRepairTypeData($id) {
        $sql = "select * from service_provider_part_location where ServiceProviderRepairTypeLocationID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deleteRepairType($id) {
        $q = "update repair_type set isDeleted='Yes' where RepairTypeID=:id";
	$values = array("id" => $id);
        $query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been deleted successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function fetch($args) {
        $spId = isset($args['firstArg'])?$args['firstArg']:'';
        $ManufacturerID = isset($args['secondArg'])?$args['secondArg']:'';
	$dataStatus = isset($args['secondArg'])?$args['secondArg']:'';
        $dbTables = "repair_type AS T1 LEFT JOIN manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID";
        $dbTablesColumns = [
            "T1.RepairTypeID", 
            "T1.RepairType",
            "T2.ManufacturerName", 
            "T1.JobWeighting",
            "T1.RepairIndex", 
            "T1.Chargeable",
            "T1.Warranty", 
            "T1.WarrantyCode",
            "T1.CompulsoryFaultCoding", 
            "T1.ExcludeFromEDI",
            "T1.ForceAdjustmentIfNoPartsUsed", 
            "T1.ExcludeFromInvoicing",
            "T1.PromptForExchange", 
            "T1.NoParts",
            "T1.ScrapExchange", 
            "T1.ExcludeRRCHandlingFee",
            "T1.Unavailable",
            "T1.Type", 
            "T1.Status"
        ];
        $args['where']    = " T1.isDeleted='No' ";
        if($spId != "") {
            $args['where'].= " AND T1.ServiceProviderID='".$spId."' ";
	}
	/* Updated By Praveen Kumar N : Show Inactive Records [START] */
	if($dataStatus != "" && $dataStatus == 'In-active') {
            $args['where'].= " AND T1.Status='".$dataStatus."' ";
	}
	/* END */
        if($ManufacturerID != "") {
            $args['where'].= " AND T1.ManufacturerID='".$ManufacturerID."' ";
	}
        $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
	return $output;
    }
    
    public function processData($args) {
	if(!isset($args['RepairTypeID']) || !$args['RepairTypeID']) {
	    return $this->create($args);
        } else {
            return $this->update($args);
        }
    }
    
    public function create($args) {
	$q="INSERT INTO repair_type (RepairType, ServiceProviderID, ManufacturerID, JobWeighting, RepairIndex, Chargeable, Warranty, WarrantyCode, CompulsoryFaultCoding, ExcludeFromEDI, ForceAdjustmentIfNoPartsUsed, ExcludeFromInvoicing, PromptForExchange, NoParts, ScrapExchange, ExcludeRRCHandlingFee, Unavailable, Type, Status, CreatedDate, CreatedUserID)
	VALUES (:RepairType, :ServiceProviderID, :ManufacturerID, :JobWeighting, :RepairIndex, :Chargeable, :Warranty, :WarrantyCode, :CompulsoryFaultCoding, :ExcludeFromEDI, :ForceAdjustmentIfNoPartsUsed, :ExcludeFromInvoicing, :PromptForExchange, :NoParts, :ScrapExchange, :ExcludeRRCHandlingFee, :Unavailable, :Type, :Status, :CreatedDate, :CreatedUserID)";
        
        $values = array("RepairType"=>$args['RepairType'], "ServiceProviderID"=>$args['ServiceProviderID'], "ManufacturerID"=>$args['ManufacturerID'], "JobWeighting"=>$args['JobWeighting'], "RepairIndex"=>$args['RepairIndex'], 
        "Chargeable"=>(!isset($args['Chargeable'])?'No':'Yes'), "Warranty"=>(!isset($args['Warranty'])?'No':'Yes'), "WarrantyCode"=>$args['WarrantyCode'], "CompulsoryFaultCoding"=>(!isset($args['CompulsoryFaultCoding'])?'No':'Yes'), "ExcludeFromEDI"=>(!isset($args['ExcludeFromEDI'])?'No':'Yes'), "ForceAdjustmentIfNoPartsUsed"=>(!isset($args['ForceAdjustmentIfNoPartsUsed'])?'No':'Yes'), 
        "ExcludeFromInvoicing"=>(!isset($args['ExcludeFromInvoicing'])?'No':'Yes'), "PromptForExchange"=>(!isset($args['PromptForExchange'])?'No':'Yes'), "NoParts"=>(!isset($args['NoParts'])?'No':'Yes'), "ScrapExchange"=>(!isset($args['ScrapExchange'])?'No':'Yes'), "ExcludeRRCHandlingFee"=>(!isset($args['ExcludeRRCHandlingFee'])?'No':'Yes'), 
        "Unavailable"=>(!isset($args['ExcludeRRCHandlingFee'])?'No':'Yes'), "Type"=>$args['Type'], "Status"=>(!isset($args['Status'])?'Active':'In-active'), "CreatedDate"=>date("Y-m-d H:i:s"),"CreatedUserID"=>$this->controller->user->UserID);
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been inserted successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function update($args) {
	$q="UPDATE repair_type SET RepairType=:RepairType, ServiceProviderID=:ServiceProviderID, ManufacturerID=:ManufacturerID, JobWeighting=:JobWeighting, RepairIndex=:RepairIndex, Chargeable=:Chargeable, Warranty=:Warranty, WarrantyCode=:WarrantyCode, CompulsoryFaultCoding=:CompulsoryFaultCoding, ExcludeFromEDI=:ExcludeFromEDI, 
        ForceAdjustmentIfNoPartsUsed=:ForceAdjustmentIfNoPartsUsed, ExcludeFromInvoicing=:ExcludeFromInvoicing, PromptForExchange=:PromptForExchange, NoParts=:NoParts, ScrapExchange=:ScrapExchange, ExcludeRRCHandlingFee=:ExcludeRRCHandlingFee, Unavailable=:Unavailable, Type=:Type, Status=:Status, 
        ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE RepairTypeID=:RepairTypeID";
        
	$values = array("RepairTypeID"=>$args['RepairTypeID'], "RepairType"=>$args['RepairType'], "ServiceProviderID"=>$args['ServiceProviderID'], "ManufacturerID"=>$args['ManufacturerID'], "JobWeighting"=>$args['JobWeighting'], "RepairIndex"=>$args['RepairIndex'], 
        "Chargeable"=>(!isset($args['Chargeable'])?'No':'Yes'), "Warranty"=>(!isset($args['Warranty'])?'No':'Yes'), "WarrantyCode"=>$args['WarrantyCode'], "CompulsoryFaultCoding"=>(!isset($args['CompulsoryFaultCoding'])?'No':'Yes'), "ExcludeFromEDI"=>(!isset($args['ExcludeFromEDI'])?'No':'Yes'), "ForceAdjustmentIfNoPartsUsed"=>(!isset($args['ForceAdjustmentIfNoPartsUsed'])?'No':'Yes'), 
        "ExcludeFromInvoicing"=>(!isset($args['ExcludeFromInvoicing'])?'No':'Yes'), "PromptForExchange"=>(!isset($args['PromptForExchange'])?'No':'Yes'), "NoParts"=>(!isset($args['NoParts'])?'No':'Yes'), "ScrapExchange"=>(!isset($args['ScrapExchange'])?'No':'Yes'), "ExcludeRRCHandlingFee"=>(!isset($args['ExcludeRRCHandlingFee'])?'No':'Yes'), 
        "Unavailable"=>(!isset($args['ExcludeRRCHandlingFee'])?'No':'Yes'), "Type"=>$args['Type'], "Status"=>(!isset($args['Status'])?'Active':'In-active'), "ModifiedDate"=>date("Y-m-d H:i:s"),"ModifiedUserID"=>$this->controller->user->UserID);
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been updated successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function fetchRow($args) {
        $sql = 'SELECT * FROM repair_type WHERE RepairTypeID=:RepairTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':RepairTypeID' => $args['RepairTypeID']));
        $result = $fetchQuery->fetch();
        return $result;
    }
    
    public function getAllActiveRepairTypes($spId = '')
    {
        $condition = "";
        if(!empty($spId))
            $condition = " AND ServiceProviderID = '".$spId."' ";
        $q = "SELECT RepairTypeID,RepairType FROM repair_type where Status='Active' AND isDeleted='No' ".$condition   ;
	$result = $this->query($this->conn, $q);
	return $result;
    }
}
?>