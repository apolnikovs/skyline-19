<?php
require_once('CustomRestClient.class.php');

class RMATrackerRESTClient extends CustomRestClient { 
    
            
    private $server;
    
    public function  __construct(CustomController $Controller) {
    
        parent::__construct($Controller); 
        
        $this->setUsername($this->controller->config['RMATracker']['Username']);
        $this->setPassword($this->controller->config['RMATracker']['Password']);
        
        $this->server = $this->controller->config['RMATracker']['Server'];
        if ($this->server[strlen($this->server) - 1] !== '/') {
            $this->server .= '/';           
        }
        
    }
    
    public function PutNewJob() {
        
        $args = array( 
			'SLNo' => '4242', //j.`JobID` AS SLNo,
			'ClientReferenceNumber' => '', // j.`AgentRefNo` AS `ClientReferenceNumber`,
			'NetworkReference' => null, // IF(mft.`ManufacturerName` = 'Samsung',	-- If manufacturer is samsung
			                          // 	j.`NetworkRefNo`			-- Output manufacturers reference
			                          // ,
			                          // 	NULL 
			                          //  ) `NetworkReference`,
			'ClientAccountNo' => '123456', //  clt.AccountNumber AS ClientAccountNo,
                        'CustTitle' => 'Mr', //  ct.`Title` AS `CustTitle`, 
                        'CustForename' => 'John', //  cu.`ContactFirstName` AS `CustForename`, 
                        'CustSurname' => 'Doe', //  cu.`ContactLastName` AS `CustSurname`, 
                        'CustBuildingName' => 'Hamilton House', //  cu.`BuildingNameNumber` AS `CustBuildingName`,
                        'CustStreet' => 'Palmerston Road', //  cu.`Street` AS `CustStreet`,
                        'CustArea' => '', //  cu.`LocalArea` AS `CustArea`,
                        'CustTown' => 'Northampton', //  cu.`TownCity` AS `CustTown`,
                        'CustCounty' => 'Northamptonshire', //  cuco.`Name` AS `CustCounty`,
                        'CustCountry' => 'UK', //  cuctry.`Name` AS `CustCountry`,                        
                        'CustPostcode' => 'NN1 5EX', //  cu.`PostalCode` AS `CustPostcode`, 
                        'CustPhNo' => '01604777777', //  cu.`ContactHomePhone` AS `CustPhNo`,
   			'CustWrkNo' => '01604888888', //  IF (ISNULL(cu.`ContactWorkPhoneExt`) OR cu.`ContactWorkPhoneExt` = '',          -- Is extension blank or null
			                   //  	cu.`ContactWorkPhone`                                                   -- Yes, so just display phone number
			                   //  ,
			                   //  	CONCAT(cu.`ContactWorkPhone`,' ext ',cu.`ContactWorkPhoneExt`)          -- No, so display number and extension
			                   //  ) AS `CustWrkNo`,
                        'CustMobileNo' => '0708123456', //  cu.`ContactMobile` AS `CustMobileNo`,
                        'CustEmail' => 'j.doe@xxxxx.com', //  cu.`ContactEmail` AS `CustEmail`,
                        'ClientCompanyName' => 'Client Ltd', //  clt.`ClientName` AS `ClientCompanyName`,
			'ClientBuildingName' => 'Client House', //  clt.`BuildingNameNumber` AS `ClientBuildingName`,
			'ClientStreet' => 'Client Street', //  clt.`Street` AS `ClientStreet`,
			'ClientArea' => 'Client Estate', //  clt.`LocalArea` AS `ClientArea`,
			'ClientTown' => 'Clientville', //  clt.`TownCity` AS `ClientTown`,
			'ClientCounty' => 'Berkshire', //  cltco.`Name` AS `ClientCounty`,
			'ClientCountry' => 'UK', //  cltctry.`Name` AS `ClientCountry`,
			'ClientPostcode' => 'RG1 4PK', //  clt.`PostalCode` AS `ClientPostcode`,
			'OriginalRetailer' => 'Original Retailer Inc', //  j.`OriginalRetailer` AS `OriginalRetailer`,
			'RetailerLocation' => 'Shrewsbury', //  j.`RetailerLocation` AS `RetailerLocation`,
			'ServiceType' => 'C', //  st.Code AS `ServiceType`,
			'RepairType' => '', //  j.`RepairType` AS `RepairType`,
			'ModelName' => 'MODEL123456', //  mdl.`ModelName` AS `ModelName`,
			'ManufacturerName' => 'Acorn Electronics', //  mft.`ManufacturerName` AS `ManufacturerName`,
			'UnitType' => 'TV', //  ut.`UnitTypeName` AS `UnitType`,
                        'SerialNumber' => 'SER123456789-10', //  j.`SerialNo` AS `SerialNumber`,
			'DOP' => '01/01/2012', //  j.`DateOfPurchase` AS `DOP`,
			'DOM' => '', //  j.`DateOfManufacture` AS `DOM`,
			'PolicyNo' => 'POL12345678', //  j.`PolicyNo` AS `PolicyNo`,
			'ReportedFault' => "Doesn't Work", //  j.`ReportedFault` AS `ReportedFault`,
			'Notes' => 'This is a test job.', //  j.`Notes` AS `Notes`,
			'ConditionCode' => '1', //  j.`ConditionCode` AS `ConditionCode`,
			'SymptomCode' => '110', //  j.`SymptomCode` AS `SymptomCode`,
			'GuaranteeCode' => '05', //  j.`GuaranteeCode` AS `GuaranteeCode`,
			'ProductLocation' => 'CUSTOMER', //  pl.`ProductLocationName` AS `ProductLocation`,
			'BookedDate' => '01/07/2012', //  MAX(appt.`AppointmentDate`) AS `BookedDate`,
			'BookedTime' => '10:30:00', //  MAX(appt.`AppointmentTime`) AS `BookedTime`,
			'FaultOccurredDate' => '24/06/2012', //  j.`FaultOccurredDate` AS `FaultOccurredDate`,
			'ETDDate' => '', //  j.`ETDDate` AS `ETDDate`,
			'Accessories' => '', //  j.`Accessories` AS `Accessories`,
			'UnitCondition' => 'BAD', //  j.`UnitCondition` AS `UnitCondition`,
			'Insurer' => 'NOT SET', //  j.`Insurer` AS `Insurer`,
			'AuthorisationNo' => '0223344', //  j.`AuthorisationNo` AS `AuthorisationNo`
			'DataConsent' => 'Y', //  j.`DataConsent` AS `DataConsent`
            );
        
        $this->controller->log(var_export($args,true));
        $this->controller->log('Username: '.$this->getUsername());
        $this->controller->log('Password: '.$this->getPassword());
        $this->controller->log('URL: '.$this->server.'PutNewJob');
        
        $this->setResponseFormat('json');
        $result = $this->execute($this->server.'PutNewJob',$args,'POST');
        
        $this->controller->log(var_export($result,true));
        
    }
    
    

}
?>
