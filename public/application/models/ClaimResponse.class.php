<?php

/**
 * ClaimResponse.class.php
 * 
 * Model with routines to access samsung_claim_resposne table in the Skyline
 * database
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.01
 * 
 * Changes
 * Date        Version Author                Reason
 * 22/08/2012  1.00    Andrew J. Williams    Initial Version
 * 24/08/2012  1.01    Andrew J. Williams    Rename of table from samsung_claim_response to claim_response
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class ClaimResponse extends CustomModel  {
    public $debug = false;
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* For Database Connection */
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
       
        $this->table = TableFactory::ClaimResponse();
    }
    
    /**
     * create
     *  
     * Create a Samsung Claim Responses item
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new reposne claim item
     * 
     * @return (status - Status Code, message - Status message, id - ID of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'SUCCESS',
                             'samsungClaimResponseId' => $this->conn->lastInsertId());
        } else {
            $result = array('status' => 'FAIL',
                            'samsungClaimResponseId' => 0,
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
}

?>
