<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{$printJob.PageTitle|escape:'html'}</title>
<style>
    
    .wrap {
	position:relative;
	float:left;
	zoom:1;
	margin:0px;
	padding-top:0px;
    }
    
    .receipt {
	/*position: absolute;*/
	/*top: {$brandLogoHeight}px;*/
	/*right: 3px;*/
	position: relative;
	display: block;
	float: right;
	color: #039CDF;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 18px;
	font-size: 16px;
	font-weight: bold;
	margin-top: 0;
	margin-bottom: 3px;
	text-align: left;
	text-transform: uppercase;
    }
    
    .logo {
	float:left;
	position:absolute;
	top:0px;
	left:0px;
    }
    
    {*
    html body div.wrap table tbody tr td.odd {
	border-top:5px solid #336699;
	padding-top:10px;
    }
    *}
    
    td.odd {
	border-top:5px solid #336699;
	padding-top:10px;
    }    
    
</style>
	
</head>

<body style="width: 100% !important; background-color: #ffffff; margin: 0; padding: 0px 0px 0px 20px;">
    
    <div class="wrap">
	{*
	<img  style="outline: none; text-decoration: none; display: block;margin:3px 0 5px;" alt="{$printJob.Brand|escape:'html'}" title="{$printJob.Brand|escape:'html'}" src="{$brandLogo|escape:'html'}" width="{$brandLogoWidth}" height="{$brandLogoHeight}" />
	<div class="receipt">{$printJob.PageTitle|escape:'html'}</div>
	*}

        {* Begin Issue 228 - A. Williams 21/02/2013 - Add second logo to paperwork for Samsung Experience *}
        {* Temporary fix to allow two logos - https://bitbucket.org/pccs/skyline/issue/228/add-second-logo-to-paperwork-for-samsung *}
        {* This will be replaced later with functionallity to allow this for any brand *}
        {if $brandId == '2030'} {*if '2030' == '2030'*}
            {$twoLogo = true}
            <table style="width:90%">{* This section for Samsung One Touch Header  *}
                <tr>
                    <td style="width:350px;">
                        <img  style="outline: none; text-decoration: none; display: block;margin:3px 0 5px;" alt="{$printJob.Brand|escape:'html'}" title="{$printJob.Brand|escape:'html'}" src="{$brandLogo|escape:'html'}" width="{$brandLogoWidth}" height="{$brandLogoHeight}" />

                    </td>
                    <td style="text-align: right;">
                        <img  style="outline: none; text-decoration: none; display: block;margin:3px 0 5px; align: right;" alt="Samsung" title="Samsung" src="{$img_path}/images/samsung_secondary.png" />
 
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: bottom ;text-align: left;">

                        <div class="receipt" style="margin-bottom:10px;"><h2>{$printJob.PageTitle|escape:'html'}</h2></div>
                    </td>
                    <td style="text-align: right;">
                        <p>&nbsp;<p>
                        {if $printJob.JobID}
                                <barcode code="{$printJob.JobID|escape:'html'}" type="C39"  style="padding:0px; margin-right:-18px;">
                        {/if}
                        <p>&nbsp;<p>
                         {if $printJob.JobID}
			<div style="font-weight:bold; font-size:18px; padding:0px; text-align:right; color:#666;">
			 Job No:  SL {$printJob.JobID|escape:'html'}
                            </div>
			                     {/if}  
                    </td>
                </tr>
            </table>
        {else}    
            {$twoLogo = false}
	<table style="width:90%;">
	    <tr>
		<td style="width:350px;">
		    <img  style="outline: none; text-decoration: none; display: block;margin:3px 0 5px;" alt="{$printJob.Brand|escape:'html'}" title="{$printJob.Brand|escape:'html'}" src="{$brandLogo|escape:'html'}" width="{$brandLogoWidth}" height="{$brandLogoHeight}" />
		</td>
		<td style="vertical-align: bottom; text-align: right;">
		    <div class="receipt" style="margin-bottom:10px;"><h2>{$printJob.PageTitle|escape:'html'}</h2></div>
                    <p>&nbsp;<p>
                    {if $printJob.JobID}
                            <barcode code="{$printJob.JobID|escape:'html'}" type="C39" style="padding:0px; margin-right:-18px;">
                    {/if}
                    <p>&nbsp;<p>
                    {if $printJob.JobID}
			<div style="font-weight:bold; font-size:18px; padding:0px; text-align:right; color:#666;">
			 Job No:  SL {$printJob.JobID|escape:'html'}
                            </div>
		   {/if}  
		</td>
	    </tr>
	</table>
        {/if} 
        {* End Issue 228 - A. Williams 21/02/2013 - Add second logo to paperwork for Samsung Experience *}
	
	<table width="610" height="700" border="0" cellspacing="0" cellpadding="0">
	    {*
	    <tr>
		<td width="293">&nbsp;</td>
		<td width="22">&nbsp;</td>
		<td width="32">&nbsp;</td>
		<td width="272" align="right">

	    </tr>*}
	    <tr style="border-top: 3px solid black;">
    		<td class="odd">
		    <h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif;line-height:18px; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">
			Retailer Details
		    </h1>
		    <table width="272" cellspacing="0" cellpadding="0" border="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
			<tbody>
			    <tr height="20">
				<td width="209">
				    <h4 style="margin:0px; font-weight:bold; text-transform:uppercase; color:#666666;">
					{$printJob.Branch|escape:'html'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails1|default:'&nbsp;'}</h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails2|default:'&nbsp;'}</h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails3|default:'&nbsp;'}</h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails4|default:'&nbsp;'}</h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails5|default:'&nbsp;'}</h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails6|default:'&nbsp;'}</h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails7|default:'&nbsp;'}</h4>
				</td>
			    </tr>
                        </tbody>
		    </table>
    		</td>
    		<td class="odd">&nbsp;</td>
    		<td class="odd">&nbsp;</td>
    		<td class="odd" style="vertical-align: top">
		    <h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif;line-height:18px; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">
			Job Details
		    </h1>
		    <table width="272" cellspacing="0" cellpadding="0" border="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
			<tbody>
			    <tr>
				<td width="129">
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Date Booked: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.DateBooked|escape:'html'}
                                </td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Booked By: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.BookedByName|Lower|Capitalize|escape:'html'}
                                </td>
			    </tr>
                            <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Service Type: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.ServiceTypeName|escape:'html'}
                                </td>
			    </tr>
                            <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Job Type: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.JobTypeName|escape:'html'}
                                </td>
			    </tr>    
                            <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Original Location: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.ProductLocation|escape|Capitalize:'html'}
                                </td>
			    </tr>  
                            <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    {$ReferralNo}: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.AgentRefNo|escape:'html'}
                                </td>
			    </tr> 
                            <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Service Agent: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.ServiceCentreName|escape:'html'}&nbsp;&nbsp;
                                    {if $printJob.ServiceCentreJobNo}
                                        SP: {$printJob.ServiceCentreJobNo|escape:'html'}
                                    {/if}    
                                </td>
			    </tr> 
                        </tbody>
		    </table>
		</td>
	    </tr>
	    <tr>
		<td class="odd" style="vertical-align:top">
		    <h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif;ine-height:18px; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 2px; text-align: left; text-transform:uppercase;">
			Customer Details
		    </h1>
		</td>
		<td class="odd">&nbsp;</td>
		<td class="odd">&nbsp;</td>
		<td class="odd">
		    <h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">Unit Details</h1>
		</td>
	    </tr>
	    <tr>
		<td style="vertical-align: top">
                    <table width="272" border="0" cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
			<tbody>
			    <tr>
				<td width="209">
				    <h4 style="margin:0px; font-weight:bold; color:#666666;">
					{$consumerFullName|default:'--'|escape:escape:'html'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails1|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails2|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails3|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails4|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails5|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails6|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails7|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			    <tr>
				<td>
				    <h4 style="margin:0px; font-weight:normal;">
					{$printJob.CDetails8|default:'&nbsp;'}
				    </h4>
				</td>
			    </tr>
			</tbody>
		    </table>

		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="vertical-align:top">
                    <table width="272" cellspacing="0" cellpadding="0" border="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
			<tbody>
			    <tr>
				<td width="129">
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Manufacturer: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.ManufacturerName|Lower|Capitalize:'html'}
                                </td>
			    </tr>
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Unit Type: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.pUnitType|Lower|Capitalize|escape:'html'}
                                </td>
			    </tr>
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Model No: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.pModel|escape:'html'}
                                </td>
			    </tr>
				{* 
				
				Modification : IF ImeiRequired is set to yes then Replace Product Code with IMEI No
				Done by Praveen Kumar N [04/07/2013] 
				
				*}
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    {if $printJob.ImeiRequired == '' || $printJob.ImeiRequired == 'No'} Product Code:{else} IMEI No: {/if}
					</span> 
				    </h4>
				</td>
                                <td>
                                    {if $printJob.ImeiRequired == '' || $printJob.ImeiRequired == 'No'} {$printJob.ProductNo|escape:'html'} {else} {$printJob.ImeiNo|escape:'html'} {/if}
                                </td>
			    </tr>
				{* Updated by Praveen [end] *}
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Serial No: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.SerialNo|escape:'html'}
                                </td>
			    </tr>
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Purchase Date: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.DateOfPurchase|escape:'html'}
                                </td>
			    </tr>
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Receipt No: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.ReceiptNo|escape:'html'}
                                </td>
			    </tr>
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Extended Warrantor: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.ExtendedWarrantyNo|escape:'html'}
                                </td>
			    </tr>
                            <tr>
                                <td>
				    <h4 style="margin:0px; font-weight:normal;">
					<span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">
					    Authorisation No: 
					</span> 
				    </h4>
				</td>
                                <td>
                                    {$printJob.AuthorisationNo|escape:'html'}
                                </td>
			    </tr>
			</tbody>
		    </table>
		</td>
	    </tr>
	    <tr>
		<td height="10px" colspan="4">
		    &nbsp;													
		</td>
	    </tr>              
	</table>
                            
        <table width=92% style="border: 1px solid #336699; line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;border-collapse:collapse;">
            <tr>
                <td width="90" style="font-weight:bold; color:#336699; text-transform:uppercase; padding-left:5px;">
                    Accessories:
                </td>
                <td>
                    {$printJob.Accessories|escape:'html'}
                </td>
                <td width="109" rowspan="2" style="border-left: 1px solid #336699;">
                    <!-- QR Code -->
                    <p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid #336699; color:#336699; font-weight:bold; text-transform:uppercase; padding-left:5px;">
                    Condition: 
                </td>
                <td style="border-top: 1px solid #336699;">
                    {$printJob.UnitCondition|escape:'html'}
                </td>
            </tr>
        </table>
                
        
        <h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase; padding-top: 10px; padding-bottom: 5px;">Service Request</h1>
        
        <table width=92% style="border: 1px solid #336699; line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;border-collapse:collapse;">
            <tr>
                <td {if $twoLogo == true}height="100"{else}height="140"{/if} style="vertical-align: top; padding-left:5px;padding-top:5px;">
                    {$printJob.ReportedFault|escape:'html'}
                </td>
            </tr>
        </table>
        
        <h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase; padding-top: 10px; padding-bottom: 5px;">Terms and Conditions</h1>
        
        <table width=92% style="border: 1px solid #336699; line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
            <tr>
                <td>
                    {$printJob.CustomerTermsConditions|escape:'html'}
                </td>
                <td rowspan="2" style="text-align: center; border-left: 1px solid #336699; width: 204px;">
                {if $printJob.ServiceTypeName|lower == "warranty"}
                    This Job has been booked as a Manufacturer's Warranty claim, please check all information is correct before proceeding.
                {else}
                        <table cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px; padding:5px; {*border:1px solid #DDDDDD;*}">
                            <tbody>
                                <tr>
                                    <td colspan="2" width="150" style="border-right: 1px solid #DDDDDD;"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h5 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">
                                            Billing Details
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">
                                            Labour:
                                        </span>
                                    </td>
                                    <td colspan="2">
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">
                                            {$InvoiceCosts.Labour}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase;">
                                            <span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">
                                                Parts:
                                            </span>
                                        </h4>
                                    </td>
                                    <td colspan="2">
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">
                                            {$InvoiceCosts.Parts}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase;">
                                            <span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">
                                                Carriage
                                            </span>
                                        </h4>
                                    </td>
                                    <td colspan="2">
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">
                                            {$InvoiceCosts.Carriage}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase;">
                                            <span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">
                                                VAT @ {$InvoiceCosts.VATRate}%:
                                            </span>
                                        </h4>
                                    </td>
                                    <td colspan="2">
                                        <h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">
                                            {$InvoiceCosts.VAT}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h2 style="margin:0px; font-weight:normal;text-transform:uppercase;">
                                            <span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">
                                                Total:
                                            </span>
                                        </h2>
                                    </td>
                                    <td>
                                        <h2 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right; color:#666;">
                                            £{$InvoiceCosts.Gross}
                                        </h2>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    {/if}
                  </td>
              </tr>                          
              <tr>
                  <td valign="bottom">
                      Future Contact Authorised By :  Email <span style="display:inline;-moz-border-radius: 9px/9px; -webkit-border-radius: 9px 9px; border-radius: 9px/9px; border:solid 1px #000000; width:9px; height:9px;">&nbsp;{if $printJob.DataProtectionEmail}x{else}&nbsp;{/if}&nbsp;</span>
                      Phone <span style="display:inline;-moz-border-radius: 9px/9px; -webkit-border-radius: 9px 9px; border-radius: 9px/9px; border:solid 1px #000000; width:9px; height:9px;">&nbsp;{if $printJob.DataProtectionHome}x{else}&nbsp;{/if}&nbsp;</span>
                      SMS <span style="display:inline;-moz-border-radius: 9px/9px; -webkit-border-radius: 9px 9px; border-radius: 9px/9px; border:solid 1px #000000; width:9px; height:9px;">&nbsp;{if $printJob.DataProtectionSMS}x{else}&nbsp;{/if}&nbsp;</span>
                      Letter <span style="display:inline;-moz-border-radius: 9px/9px; -webkit-border-radius: 9px 9px; border-radius: 9px/9px; border:solid 1px #000000; width:9px; height:9px;">&nbsp;{if $printJob.DataProtectionLetter}x{else}&nbsp;{/if}&nbsp;</span>
                      <p>&nbsp;<p>
                      Customer Signature .......................................................... Date {$smarty.now|date_format:"%d/%m/%Y"}
                  </td>
            </tr>
        </table>
				    
    </div>
				    
</body>

</html>