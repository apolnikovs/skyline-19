{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $BoughtOutGuarantee}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}

{block name=scripts}

<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>

<script type="text/javascript">

    var table;


    function showEdit(the) {
    
	var bogID = null;
    
	if(the) {
	    bogID = table.fnGetData(the)[0];
	} else {
	    var tbl = TableTools.fnGetInstance("table");
	    var data = tbl.fnGetSelectedData();
	    bogID = data[0][0];
	}
	$.post("{$_subdomain}/JobAllocation/getEditBOG", { bogID: bogID, action: "edit" }, function(response) {
	    $.colorbox({
		html :	    response, 
		width:	    "600px",
		scrolling:  false,
                onComplete:function(){
                    $("#manufacturerID").combobox();
                    $("#repairSkillID").combobox();
                    $("#unitTypeID").combobox();
                    $("#modelID").combobox();
                }
	    });
	});
    }


    function showInsert() {
	var bogID = null;
	$.post("{$_subdomain}/JobAllocation/getEditBOG", { bogID: bogID, action: "edit" }, function(response) {
	    response = response.replace("Edit Bought Out Guarantee", "Insert Bought Out Guarantee");
	    $.colorbox({
		html :	    response, 
		width:	    "600px",
		scrolling:  false,
                onComplete:function(){
                    $("#manufacturerID").combobox();
                    $("#repairSkillID").combobox();
                    $("#unitTypeID").combobox();
                    $("#modelID").combobox();
                }
	    });
	});
    }
    $(document).ready(function() {
        $("#networkSelect").combobox({
            change: function() {
                $(".dataTables_wrapper").remove();
                var the = this;
                if($(this).val() == "") {
                    $("#clientSelect").hide();
                } else {
                    var html = "<option value=''>{$page['Text']['select_client']|escape:'html'}</option>";
                    $.post("{$_subdomain}/Data/getNetworkClients", { networkID: $(the).val() }, function(response) {
                        var clients = $.parseJSON(response);
                        if(clients.length > 0) {
                            for(var i = 0; i < clients.length; i++) {
                                html += "<option value='" + clients[i].ClientID + "'>" + clients[i].ClientName + "</option>";
                            }
                            $("#clientSelect").html(html);
                            $("#clientSelectLabel").show();
                            //$("#clientSelect").show();
                            $("#clientSelect").combobox({
                                change: function() {
                                    $(".dataTables_wrapper").remove();
                                    table = null;

                                    var html = "\
                                        <table id='table' class='browse dataTable' style='display:none;'>\
                                            <thead>\
                                                <tr>\
                                                    <th></th>\
                                                    <th>Manufacturer</th>\
                                                    <th>Repair Skill</th>\
                                                    <th>Unit Type</th>\
                                                    <th>Model</th>\
                                                    <th>Status</th>\
                                                </tr>\
                                            </thead>\
                                            <tbody>\
                                            </tbody>\
                                        </table>\
                                    ";

                                    $(html).appendTo("#boughtOutGuaranteeContent");

                                    var clientID = $(this).val();

                                    var tableObj = {
                                        bAutoWidth:	    false,
                                        aoColumns: [
                                            { bVisible: false },
                                            { sWidth: '22%', sDefaultContent: "" },
                                            { sWidth: '22%', sDefaultContent: "" }, 
                                            { sWidth: '22%', sDefaultContent: "" }, 
                                            { sWidth: '22%', sDefaultContent: "" }, 
                                            { sWidth: '12%', sDefaultContent: "" } 
                                        ],
                                        bDestroy:       true,
                                        bServerSide:    false,
                                        bProcessing:    false,
                                        htmlTableId:    "table",
                                        sAjaxSource:    "{$_subdomain}/Data/getBoughtOutGuaranteeData?clientID=" + clientID,
                                        sDom:           "ft<'#dataTables_child'>Trpli",
                                        bPaginate:      true,
                                        sPaginationType:"full_numbers",
                                        bSearch:        false,
                                        iDisplayLength: 10,
                                        oLanguage: {
                                            sSearch: "Search within results:&nbsp;"
                                        },
                                        oTableTools: {
                                            sRowSelect: "single",
                                            aButtons: [
                                                {
                                                    sExtends:	"text",
                                                    sButtonText:    "Edit",
                                                    sButtonClass:	"marginRight10",
                                                    fnInit:	function(nButton, oConfig) {
                                                        //$(nButton).attr("id", "editBtn");
                                                    },
                                                    fnClick:	function() { showEdit(null); }
                                                },
                                                {
                                                    sExtends:	"text",
                                                    sButtonText:    "Insert",
                                                    fnClick:	function() { showInsert(null); }
                                                }
                                            ]
                                        },
                                        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                            if(aData[5] == "In-active") {
                                                $(nRow).css("color", "red");
                                            }
                                        }	    
                                    };

                                    $("#table").show();
                                    table = $("#table").dataTable(tableObj);

                                    var html = '<a href="#" id="delSearch" style="top:14px; right:4px; position:absolute;">\
                                                    <img style="margin:0px 0px -8px 0px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
                                                </a>';
                                    $(html).insertAfter(".dataTables_filter input");
                                }
                            });
                        } else {
                            $("#clientSelect").hide();
                        }
                    });
                }
            }
        });
    });
    {*$(document).on("change", "#networkSelect", function() {
	$(".dataTables_wrapper").remove();
	var the = this;
	if($(this).val() == "") {
	    $("#clientSelect").hide();
	} else {
	    $.post("{$_subdomain}/Data/getNetworkClients", { networkID: $(the).val() }, function(response) {
		var clients = $.parseJSON(response);
		if(clients.length > 0) {
		    var html = "<option value=''>{$page['Text']['select_client']|escape:'html'}</option>";
		    for(var i = 0; i < clients.length; i++) {
			html += "<option value='" + clients[i].ClientID + "'>" + clients[i].ClientName + "</option>";
		    }
		    $("#clientSelect").html(html);
		    $("#clientSelect").show();
		} else {
		    $("#clientSelect").hide();
		}
	    });
	}
    });*}
    
    
    $(document).on("click", "#delSearch", function() {
	$(".dataTables_filter input").val("");
	table.fnFilter("");
	return false;
    });


    $(document).on("dblclick", "tr", function() {
	showEdit(this);
	return false;
    });


    $(document).on("click", "#cancel", function() {
	$.colorbox.close();
	return false;
    });

    /*
    $(document).on("click", "#cancel", function() {
	$("#bogForm").submit();
	return false;
    });    


    $(document).ready(function() {
	
	$("#bogForm").validate({
	    rules: {
		manufacturerSelect: { required: true},
		repairSkillSelect:  { required: true},
		test:		    { required: true}
	    },
	    messages: {
		manufacturerSelect: { required: "{$page['Errors']['manufacturer']|escape:'html'}" },
		repairSkillSelect:  { required: "{$page['Errors']['repair_skill']|escape:'html'}" },
		test:		    { required: "required" }
	    },
	    errorPlacement: function(error, element) {
		error.insertAfter(element);
	    },
	    debug:	    true,
	    ignore:	    "",
	    errorClass:	    'fieldError',
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   'label',
	    submitHandler: function() {
		alert("alert");
		//console.log($("#bogForm").serialize());
		
		//$.post("url", $("#bogForm").serialize(), function() {
		    
		//});
	    }
	});
	
    });
    */

    {*$(document).on("change", "#clientSelect", function() {
    //function clientSelectOnChange()
    //{
        
	$(".dataTables_wrapper").remove();
	table = null;

	var html = "\
	    <table id='table' class='browse dataTable' style='display:none;'>\
		<thead>\
		    <tr>\
			<th></th>\
			<th>Manufacturer</th>\
			<th>Repair Skill</th>\
			<th>Unit Type</th>\
			<th>Model</th>\
			<th>Status</th>\
		    </tr>\
		</thead>\
		<tbody>\
		</tbody>\
	    </table>\
	";

	$(html).appendTo("#boughtOutGuaranteeContent");

	var clientID = $(this).val();
	
	var tableObj = {
            bAutoWidth:	    false,
            aoColumns: [
                { bVisible: false },
                { sWidth: '22%', sDefaultContent: "" },
                { sWidth: '22%', sDefaultContent: "" }, 
                { sWidth: '22%', sDefaultContent: "" }, 
                { sWidth: '22%', sDefaultContent: "" }, 
                { sWidth: '12%', sDefaultContent: "" } 
            ],
            bDestroy:       true,
            bServerSide:    false,
            bProcessing:    false,
            htmlTableId:    "table",
            sAjaxSource:    "{$_subdomain}/Data/getBoughtOutGuaranteeData?clientID=" + clientID,
            sDom:           "ft<'#dataTables_child'>Trpli",
            bPaginate:      true,
            sPaginationType:"full_numbers",
            bSearch:        false,
            iDisplayLength: 10,
	    oLanguage: {
		sSearch: "Search within results:&nbsp;"
	    },
            oTableTools: {
                sRowSelect: "single",
                aButtons: [
                    {
                        sExtends:	"text",
                        sButtonText:    "Edit",
			sButtonClass:	"marginRight10",
			fnInit:	function(nButton, oConfig) {
			    //$(nButton).attr("id", "editBtn");
			},
                        fnClick:	function() { showEdit(null); }
                    },
                    {
                        sExtends:	"text",
                        sButtonText:    "Insert",
                        fnClick:	function() { showInsert(null); }
                    }
                ]
            },
	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		if(aData[5] == "In-active") {
		    $(nRow).css("color", "red");
		}
	    }	    
	};
	
	$("#table").show();
	table = $("#table").dataTable(tableObj);

	var html = '<a href="#" id="delSearch" style="top:14px; right:4px; position:absolute;">\
			<img style="margin:0px 0px -8px 0px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
		    </a>';
        $(html).insertAfter(".dataTables_filter input");
      //}  
    });*}

</script>

{/block}


{block name=body}

    <div class="breadcrumb">
        <div>
            <a href="{$_subdomain}/SystemAdmin">{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/jobAllocation">{$page['Text']['job_allocation']|escape:'html'}</a> / &nbsp;{$page['Text']['page_title']|escape:'html'}
        </div>
    </div>


    <div class="main" id="home">

	<div class="boughtOutGuaranteeTopPanel" >
	    <form id="boughtOutGuaranteeTopForm" name="boughtOutGuaranteeTopForm" method="post" action="" class="inline">
		<fieldset>
		    <legend title="">{$page['Text']['legend']|escape:'html'}</legend>
		    <p>
			<label>{$page['Text']['description']|escape:'html'}</label>
		    </p> 
		</fieldset> 
	     </form>
	 </div>  

	<div id="boughtOutGuaranteeContent" class="ServiceAdminResultsPanel" style="margin-top:20px; margin-bottom:20px;">
	    
	    <div style="margin-bottom:-40px;">
                {$page['Labels']['network_label']|escape:'html'}
		<select id="networkSelect">
		    <option value="">{$page['Text']['select_network']|escape:'html'}</option>
		    {foreach $networks as $network}
			<option value="{$network.NetworkID}">{$network.CompanyName}</option>
		    {/foreach}
		</select><br />
                <span id="clientSelectLabel" style="display:none; padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
		<select id="clientSelect" style="display:none;">
		    <option value="">{$page['Text']['select_client']|escape:'html'}</option>
		</select>
	    </div>
	    
	    {*	
	    <table id="table" class="browse dataTable" style="display:none;">
		<thead>
		    <tr>
			<th></th>
			<th>Manufacturer</th>
			<th>Repair Skill</th>
			<th>Unit Type</th>
			<th>Model</th>
		    </tr>
		</thead>
		<tbody>
		</tbody>
	    </table>
	    *}
	    
	</div>
		    
    </div>

{/block}



