{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Colour}
    {$fullscreen=true}
{/block}


{block name=scripts}


   
<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript">
    function showTablePreferences(){
$.colorbox({ 
 
                       href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=Colour/table=Colour",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      
 $(document).ready(function() {

 var oTable = $('#ColourResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/LookupTables/loadColourTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#ColourResults').show();
                               
			} );
                        },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,
 "aoColumns": [ 
			
			
                               
                            
                            { "bVisible":0 },
                                true,
                                true,
                         
                               
                               true
                            
		] 
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#ColourResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        console.log(anSelected);
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                           href:"{$_subdomain}/LookupTables/processColour/id="+anSelected[0].id,
                        title: "Edit Colour",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
             
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/LookupTables/deleteColour/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#ColourResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processColour/id="+anSelected[0].id,
                        title: "Edit Colour",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  


	$('input[id^=inactivetick]').click( function() {
         $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#ColourResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadColourTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#ColourResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadColourTable/");
                 }
	} );  
        var v=0;
	$('a[id^=showpendingtick]').click( function() {
        v++;
        if(v==1){
                 $('#tt-Loader').show();
                                $('#ColourResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadColourTable/pending=1/");
                 }else{
                 v=0;
                  $('#tt-Loader').show();
                                $('#ColourResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadColourTable/");
                 }
	} );                            
    
	$('input[id^=unaprovedtick]').click( function() {
        $('#inactivetick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#ColourResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadColourTable/unaproved=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#ColourResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadColourTable/");
                 }
	} );     


    });//doc ready end
function ColourInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processColour/",
                        title: "Insert Colour",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function ColourEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
               
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processColour/id="+aData,
                        title: "Edit Colour",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}


                    
                    
                
                
             
function filterTable(name){
$('input[type="text"]','#ColourResults_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#ColourResults_filter').trigger(e);


}

    </script>

    
{/block}


{block name=body}
<!--<div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>-->
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >LookupTables</a> {if isset($backtomodel)}/<a href="{$_subdomain}SystemAdmin/index/lookupTables/models" > Models</a> {/if}/ Colour

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="ColourTopForm" name="ColourTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >Colours</legend>
                        <p>
                            <label>Colour setup</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
{if isset($pendingNo)&&$pendingNo>0}
                        <div style="text-align: center;float:left;background:#FFE8F1;border:1px solid red;padding: 4px;margin-top: 10px;">
                            <a id="showpendingtick" href="#">Show Pending</a>
                            <span style="color:red">{$pendingNo}</span> {$page['Labels']['tableName']} Need Approval
                        </div>
                        {/if}

                  <div class="ServiceAdminResultsPanel" id="ColourResultsPanel" >
                    
                  
                 
                
                     <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                      
                      
                    <form method="POST" action="{$_subdomain}/LookupTables/saveAssigned" id="ColourResultsForm" class="dataTableCorrections">
                        <table style="display:none" id="ColourResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            
                                        <th>id</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                  
                                    
                                     
                               
                                  
                               
                                <th style="width:10px"></th>
                            
                                    </tr>
                            </thead>
                            <tbody>
                           
                            
                            </tbody>
                        </table>  
                          
                           
                     </form>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="ColourInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               
<div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                     {if $activeUser=="SuperAdmin"} <input id="unaprovedtick"  type="checkbox" > Show Unapproved {/if}
                    </div>

    </div>
               


{/block}



