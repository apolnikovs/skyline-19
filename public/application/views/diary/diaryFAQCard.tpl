<head>
    <script>
 function newCategoryForm()
 {
     $('#mainFaqCardDiv').toggle();
     $('#newCategoryDiv').toggle();
     $.colorbox.resize();
 }
 
 function addNewCategory(){
 $.post("{$_subdomain}/Diary/addFaqCategory",{ 'category':$('#newCategoryText').val() },
function(data) {
id=$.trim(data);
 $('#hEng').append(new Option($('#newCategoryText').val(), id));
 newCategoryForm();
});
 }
 
 function validateFaqForm(f){
 if($('#hEng').val()=='000'||$('#hEng').val()==""||$('#faqAnswer').val()==""){
 alert("PLOP!!! All Fields Should be Filled!!!");
 }else{
 f.submit();
 }
 }
    </script>
    <body>
        <div id="mainFaqCardDiv">
        <fieldset>
            
            <legend>FAQ</legend>
            
            <form name="faqCardForm" method="post" action="{$_subdomain}/Diary/insertFaq">
            {if isset($data)}<input type="hidden" name="id" value="{$data['DiaryFaqID']}">{/if}
           
            <table>
                <tr>
                    <td style="background:none">
                        Category:
                    </td>
                    <td style="background:none">
                        <select name="hEng" id="hEng">
                            <option value="000">Please select category</option>
                        {foreach $categoryList as $ff}
                              <option {if isset($data)&&$data['DiaryFaqCategoryID']==$ff.DiaryFaqCategoryID}selected=selected{/if}   value="{$ff.DiaryFaqCategoryID}">{$ff.CategoryName}</option>
                           {/foreach}
                        </select> <button type="button" class="btnStandard" onclick="newCategoryForm()">New</button>
                    </td>
                
                </tr>
                <tr >
                    <td style="background:none">
                        Question:
                    </td>
                    <td style="background:none">
                        <textarea id="faqQuestion" name="faqQuestion">{if isset($data['FaqQuestion'])}{$data['FaqQuestion']}{/if}</textarea>
                    </td>
                </tr>
                <tr>
                    <td style="background:none">
                        Answer:
                    </td>
                    <td style="background:none">
                        <textarea id="faqAnswer" name="faqAnswer">{if isset($data['FaqAnswer'])}{$data['FaqAnswer']}{/if}</textarea>
                    </td>
                </tr>
                
                
            </table>
            <div style="float:right">
                <button type="button" onclick="validateFaqForm(this.form)" class="btnStandard">{if isset($data)}Update{else}Insert{/if}</button>
                <button type="button" onclick="$.colorbox.close()" class="btnStandard">Cancel</button>
            </div>
                </form>
        </fieldset>
                </div>
 <div id="waitDiv" style="display:none">
     <div style="min-height:100px;min-width: 100px;">
        <img src="{$_subdomain}/images/processing.gif">
        <div style="text-align: center">Please wait!</div></div> </div>   
        
        <div id="newCategoryDiv" style="display:none">
            <fieldset style="width:500px">
                <legend>New Category</legend>
                <input style="width:480px" type="text" id="newCategoryText"><br>
                <button type="button" onclick="addNewCategory()" class="btnStandard" style="float:left">Add</button>
                <button type="button" onclick="newCategoryForm()"  class="btnStandard" style="float:right">Cancel</button>
            </fieldset>
        </div>
    </body>

</head>