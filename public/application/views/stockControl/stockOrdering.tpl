{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Stock"}
    {$PageId = $StockOrderingPage}
    {$fullscreen=true}
    {$showTopLogoBlankBox=false}
    {$controller="StockControl"}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}


{block name=scripts}


   
 <script>
   
   
        
        
        
        var oldValue;
    $(document).ready(function() {
    
    $('th input:checkbox').click(function(e) {

var table = $(e.target).closest('table');
$('td input:checkbox', table).attr('checked', e.target.checked);

});
    var oTable = $('#StockResults').dataTable( {
"sDom": 'R<"left"><"top"f><"viewTypeP">t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/{$controller}/loadStockOrderingTable/spid="+$('#serviceProviderSelect').val()+"/suplierID="+$('#supplierSelect').val()+"/statusesSelect="+$('#statusesSelect').val()+"/viewType="+$('input[name=viewType]:checked').val()+"/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#StockResults').show();
                               
                               
			} );
                        },
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": "Search within results"
                            },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[  25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,

"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            {if $er==8}{$vis=0}{/if}
                            {if $er==9}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               { "bVisible":1,"bSortable":false,"sWidth":'10px' }
                            
		] 
               
   
 
        
          
});//datatable end

    //view type
  $("div.viewTypeP").html('<input onclick="setViewType(1)" checked="checked" type="radio" name="viewType" value="1">Summary <input onclick="setViewType(2)" type="radio" name="viewType" value="2">Detailed');
  
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#StockResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
                       
		});
		$(event.target.parentNode).addClass('row_selected');
               
                
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
      /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



   
    
    
    
    
	
        var v=0;
	
	                           
          
          
          
          
                          
             




	
		
  $('#serviceProviderSelect, #statusesSelect, #supplierSelect').change( function() {
        
    
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockOrderingTable/spid="+$('#serviceProviderSelect').val()+"/suplierID="+$('#supplierSelect').val()+"/statusesSelect="+$('#statusesSelect').val()+"/viewType="+$('input[name=viewType]:checked').val());
       
	} );               
              

} );//doc ready

//displaying table pref colorbox
function showTablePreferences(){
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/tableDisplayPreferenceSetup/page=stockOrdering/table=part",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}



                
               
function toggleApplyBut(){
if($('#statusApply').val()!=''){ 
$('#applyStatusBut').attr('disabled',false).removeClass('gplus-gray-disabled').addClass('gplus-blue');

}else{
$('#applyStatusBut').attr('disabled',true).removeClass('gplus-blue').addClass('gplus-gray-disabled');
}
}
 
function applyStatusConfirm(){
$.colorbox({ 
 
                        html:$('#ViaDiv2').html(),
                        title: "Confirmation",
                        opacity: 0.75,
                        width:400,
                        overlayClose: false,
                        escKey: false

                });
}
 
function requisitionConfirm(){
parts=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
});
$.post("{$_subdomain}/StockControl/checkRequisition", { parts: parts}, function(res) {
   en=jQuery.parseJSON(res);
  
   if(en.length>0){
   {$legend="legend=Create Requisition"}
{$text="text=Warning: Stock Items are tagged that are already assigned to existing requisitions. If you continue these items will be automatically deselected from the Requisition "}
{$confButAction="confButAction=untickRequsitioned(en);$.colorbox.close()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
   }else{
   {$legend="legend=Create Requisition"}
{$text="text=Are you sure you wish to create a Requisition for the tagged stock items?"}
{$confButAction="confButAction=setRequsitioned();$.colorbox.close()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
   }
    
});

} 
 
function untickRequsitioned(p){
$.each(p, function(index, value) {
[id*="cat"]
$("[id*='"+value.PartID+"'] input:checked").attr("checked",false);
});
setRequsitioned();
}
 
function untickWrongStatus(p){
$.each(p, function(index, value) {
$("[id*='"+value.PartID+"'] input:checked").attr("checked",false);
});
applyStatus();
}

function untickNotReqApprovedStatus(p){
$.each(p, function(index, value) {
$("[id*='"+value.PartID+"'] input:checked").attr("checked",false);
});
MakeTaggedOrders();
}

 
 function applyStatus(){
 parts=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
});

 $.post("{$_subdomain}/StockControl/checkIfCanApplyNewStatus", { parts: parts,status:$('#statusApply').val() }, function(res) {

 
 //selected parts dont have status 2 or 18
  en=jQuery.parseJSON(res);
   if(en.length>0){
   {$legend="legend=Update Requisition Status"}
{$text="text=Warning: Stock Items are tagged that cannot be updated due to their current status. If you continue these items will be automatically deselected from this request."}
{$confButAction="confButAction=untickWrongStatus(en);$.colorbox.close()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
 }else{
  $.post("{$_subdomain}/StockControl/changePartsStatus", { parts: parts,status:$('#statusApply').val() }, function(res) {
 var oTable = $('#StockResults').dataTable();
 oTable.fnReloadAjax();
 });
 }
 });
 
 }
 
function setRequsitioned(){
 parts=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
});
if(parts.length>0){
 $.post("{$_subdomain}/StockControl/setRequisition", { parts: parts,status:18 }, function(res) {
  en=jQuery.parseJSON(res);
 
 window.open("{$_subdomain}/StockControl/printRequisition/reqIds="+en,"_blank");
 var oTable = $('#StockResults').dataTable();
 oTable.fnReloadAjax();
 });
 }else{
 $.colorbox.close();
 }
}


function MakeTaggedOrders()
{
    parts=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
});
 $.post("{$_subdomain}/StockControl/MakeTaggedOrders", { parts: parts }, function(res) {
en=jQuery.parseJSON(res);

 var oTable = $('#StockResults').dataTable();
 oTable.fnReloadAjax();
 window.open("{$_subdomain}/StockControl/printOrders/ordersIds="+en,"_blank");
  $.colorbox.close();
  
 });
}


function MakeTaggedOrdersPopup(){
parts=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
});
if(parts.length>0){
$.post("{$_subdomain}/StockControl/validateMakeTaggedOrders", { parts: parts }, function(res) {
 en=jQuery.parseJSON(res);
 
   if(en.length>0){
{$legend="legend=Make Tagged Orders"}
{$text="text=Warning: Stock Items are tagged that have not been Requisitioned or Requisition Approved. If you continue these items will be automatically deselected from the Order. "}
{$confButAction="confButAction=untickNotReqApprovedStatus(en)"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}/",
                        title: "Warning",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        onComplete:function(){
                        $('#RequisitionNoInput').focus().select();
                        }

                });
   }else{
   {$legend="legend=Make Tagged Orders"}
{$text="text=Are you sure you wish to create an Order for the tagged stock items"}

{$confButAction="confButAction=MakeTaggedOrders()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        onComplete:function(){
                        $('#RequisitionNoInput').focus().select();
                        }

                });
   
   }
                });
                }
}


function approveRequisitionPopUp(){
{$legend="legend=Approve Requisition"}
{$text="text=Enter the Requisition Number you wish to approve."}
{$inputLabel="inputLabel=Requisition No"}
{$inputID="inputID=RequisitionNoInput"}
{$confButAction="confButAction=validateReq()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}/{$inputID|escape:'url'}/{$inputLabel|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        onComplete:function(){
                        $('#RequisitionNoInput').focus().select();
                        }

                });
}

function reprintReqPopup(){
{$legend="legend=Reprint Requisition"}
{$text="text=Enter the Requisition Number you wish to reprint."}
{$inputLabel="inputLabel=Requisition No"}
{$inputID="inputID=RequisitionNoInput"}
{$confButAction="confButAction=checkIfExistReq()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}/{$inputID|escape:'url'}/{$inputLabel|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        onComplete:function(){
                        $('#RequisitionNoInput').focus().select();
                        }

                });
}

function checkIfExistReq(){
req=$('#RequisitionNoInput').val();
    $.post("{$_subdomain}/StockControl/validateReq", { req: req,existOnly:1 }, function(res) {
    switch(res){
    case "1": { 
        $('#validationMsgP').html("Requisition Number does not exist")
        $('#RequisitionNoInput').focus().select();
        $.colorbox.resize();
        break;}
    
    
    case "0": { 
        $req=new Array();
        $req.push(req);
      window.open("{$_subdomain}/StockControl/printRequisition/reqIds="+$req,"_blank");
        break;
    }
    }
    //$.colorbox.close();
});
}
function validateReq(){
    req=$('#RequisitionNoInput').val();
    $.post("{$_subdomain}/StockControl/validateReq", { req: req }, function(res) {
    switch(res){
    case "1": { 
        $('#validationMsgP').html("Requisition Number does not exist")
        $('#RequisitionNoInput').focus().select();
        $.colorbox.resize();
        break;}
    
    case "2": { 
        $('#validationMsgP').html("The status of the stock items on this Requisition number are not available for approval")
        $('#RequisitionNoInput').focus().select();
        $.colorbox.resize();
        break;
    }
    case "0": { 
       $.post("{$_subdomain}/StockControl/approveReq", { req: req }, function(res) {
       var oTable = $('#StockResults').dataTable();
 oTable.fnReloadAjax();
  $.colorbox.close();
   });
        break;
    }
    }
    //$.colorbox.close();
});
}

function setViewType(n){
var oTable = $('#StockResults').dataTable();
if(n==1){
oTable.fnSetColumnVis( 8,false );
oTable.fnSetColumnVis( 9,false );

}
if(n==2){
oTable.fnSetColumnVis( 8,true );
oTable.fnSetColumnVis( 9,true );

}

    oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockOrderingTable/spid="+$('#serviceProviderSelect').val()+"/suplierID="+$('#supplierSelect').val()+"/statusesSelect="+$('#statusesSelect').val()+"/viewType="+$('input[name=viewType]:checked').val());
                 
}


    </script>

    
{/block}


{block name=body}
<!-- <div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>-->
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/index/siteMap/" >{$page['Text']['SiteMap']}</a> / {$page['Text']['StockOrdering']}

        </div>
    </div>
           
    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="StockTopForm" name="StockTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['StockOrderingTable']}</legend>
                        <p>
                            <label>{$page['Text']['legend_text']}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
                {if !isset($NotPermited)}        
 <div class="ServiceAdminResultsPanel" id="StockResultsPanel" >
                    

                 
                    
     
                    
                    <form id="StockResultsForm" class="dataTableCorrections">
                        {if isset($splist)}
                            <p>
                        <select id="serviceProviderSelect" style="float:left;padding: 4px;position:relative;top:-2px">
                            <option value="0">Select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}">{$s.Acronym}</option>
                        {/foreach}
                        </select>
                        </p>
                        {/if}
                        
                        {if isset($suppliers)}
                            <p>
                        <select name="supplierSelect" id="supplierSelect" style="float:left;padding: 4px;position:relative;top:-2px">
                            <option value="0">Select Supplier</option>
                        {foreach $suppliers as $s}
                            <option value="{$s.ServiceProviderSupplierID}">{$s.CompanyName}</option>
                        {/foreach}
                        </select>
                        </p>
                        {/if}
                        
                        
                        {if isset($AllStatuses)}
                            <p>
                        <select name="statusesSelect" id="statusesSelect" style="float:left;padding: 4px;position:relative;top:-2px">
                            <option value="0">Select Status</option>
                        {foreach $AllStatuses as $s}
                            <option value="{$s.PartStatusID}">{$s.PartStatusName}</option>
                        {/foreach}
                        </select>
                        </p>
                        {/if}
                        
                        
                        <table id="StockResults" style="display:none" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px;text-align: center"><input type="checkbox"></th>
			    </tr>
                        </thead>
                        <tbody>
                           <div style="text-align:left" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                        </tbody>
                    </table>  
                    <input type="hidden" name="sltSPs" id="sltSPs" >            
                    </form>
                    
                    
                    
                </div>        

                <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:900px">
                        <div >
<!--                            <button type="button" id="edit"  class="gplus-blue">{$page['Buttons']['edit']|escape:'html'}</button>
                            <button type="button" onclick="stockInsert()" class="gplus-blue">{$page['Buttons']['insert']|escape:'html'}</button>
                            <button type="button" id="delete" class="gplus-red">{$page['Buttons']['delete']|escape:'html'}</button>-->
                            <p style="float:left">
                            
                            
                            <button style="width:150px" type="button" onclick="requisitionConfirm()" class="gplus-blue">{$page['Buttons']['createRequistions']|escape:'html'}</button>
                            <button style="width:150px" type="button" onclick="approveRequisitionPopUp()" class="gplus-blue">{$page['Buttons']['aproveRequistions']|escape:'html'}</button>
                            <button style="width:150px" type="button" onclick="reprintReqPopup()" class="gplus-blue">{$page['Buttons']['ReprintRequistions']|escape:'html'}</button>
                          </p>
                            <p style="float:left;margin-left:20px;">
                            <select id="statusApply" onchange="toggleApplyBut();">
                                <option value="">Update Status</option>
                                    {foreach from=$statuses item=b}
                                        <option value="{$b.PartStatusID}">{$b.PartStatusName}</option>
                                    {/foreach}
                                
                                </select>
                                    <button id="applyStatusBut" disabled="disabled" onclick="applyStatusConfirm()" type="button" onclick="" class="gplus-gray-disabled">{$page['Buttons']['Apply']|escape:'html'}</button>
                       </p>
                            <p>
                       <button style="width:150px" type="button" onclick="MakeTaggedOrdersPopup()" class="gplus-blue">{$page['Buttons']['MakeTaggedOrders']|escape:'html'}</button>
                        </p>
                        </div>
<!--                            <div id="buttonSpacer" style="width:100px;float:left">&nbsp;</div><button type="button" id="stockHistoryBut" class="gplus-blue">Stock History</button>-->
                    </div>
                  
<!--                    <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                  
                    </div>-->
             
                  
                </div>        
                <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div> 

                

                {if $SuperAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               
             {/if}   <!-- end of notpermited-->

    </div>
                <div style="float: left;width: 100%;top: 12px;position: relative;">
                <hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/index/siteMap'">Finish</button>
                </div>
    
 <div id="ViaDiv2" style="display:none;font-size: 14px;width:300px;margin-bottom: 0px;height:150px">
     
         <fieldset>
             <legend>Update Stock Status</legend>
             <p style="color:6b6b6b">Are you sure?</p>
             <br>
       
           <button id=""  type=button class="btnStandard"  onclick="applyStatus();$.colorbox.close();" style="width:100px;float:left"><span class="label">Yes</span></button>   
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:100px;float:right"><span class="label">No</span></button>   
          </fieldset>
 </div>
                
 
                
               

{/block}



