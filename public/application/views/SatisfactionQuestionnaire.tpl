{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $SatisfactionQuestionnaire}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />       
        <style type="text/css" >
        .ui-combobox-input {
            width:300px;
        }
        /* added by srinivas for enabling full page */
        .container, #header, .breadcrumb, #container-main .main  {
        width: 98%;
        min-width: 950px;                       
        }
        /* end */
        </style>
    {/block}
    {block name=scripts}

        {* get brandids in string *}
        {assign var=sl value=""} 
        {foreach $brands as $brand}         
         {assign var=sl value=$sl|cat:$brand.BrandID}
         {assign var=sl value=$sl|cat:","}
         {/foreach}
        {* end of brand ids *}
<script type="text/javascript">

    $(document).ready(function() {
      
    $("#bId").combobox({
            change: function() {
                if($("#bId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/LookupTables/satisfactionQuestionnaire/'+urlencode($("#bId").val()));
                }
            }
        });  
    
                //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');

                                });


                     {if $SupderAdmin eq true} 

                        var  displayButtons = "AV";
                     
                     {else}

                         var displayButtons =  "AV";

                     {/if} 
                     


                    
                    $('#CResults').PCCSDataTable( {                  
                    sDom: 'Rft<"#dataTables_command">rpli',                     
                    "bServerSide": false,
                    "aoColumns": [                 
			null,
                        null,
                        null,                       
                        null,
                         { "bVisible": false },
                        null
			],               
                         
                            "fnServerParams": function ( aoData )   
                            {  
                                aoData.push( { "name": "BrandArr", "value": "{$sl}" } );                             
                            }  ,
                                         
                            "aaSorting"     : [[0,"asc"]],    
                            displayButtons  : displayButtons,
                            
                            createAppUrl    : '{$_subdomain}/LookupTables/satisfactionQuestionnaire/insert/'+urlencode("{$bId}"),
                            createDataUrl   : '{$_subdomain}/LookupTables/ProcessData/Questionnaire/',
                            formInsertButton: 'insert_save_btn',
                            copyInsertRowId : 'copyRowId',
                            addButtonText   : 'Insert',
                            addFormTitle    : 'Add Questionary',  
                            
                            viewFormTitle   : 'View Questionary',                            
                            viewAppUrl      : '{$_subdomain}/LookupTables/satisfactionQuestionnaire/view/',                            
                            viewButtonId    : 'viewButtonId',
                            viewButtonText  : 'View',
                            
                            frmErrorRules:   {
                                            
                                                    SQbId:
                                                        {
                                                            required: true
                                                        }
                                                    
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    SQbId:
                                                        {
                                                            required: "Brand Name Required"
                                                        }
                                             },                   
                            popUpFormWidth:  715,
                            popUpFormHeight: 430,
                            
                           // pickButtonId:"copy_btn",
                           // pickButtonText:"{$page['Buttons']['copy']|escape:'html'}",
                           // pickCallbackMethod: "gotoInsertPage",
                           // colorboxForceClose: false,
          
                            
                            colorboxFormId  : "SQForm",
                            frmErrorMsgClass: "fieldError",
                            frmErrorElement : "label",
                            htmlTablePageId : 'CResultsPanel',
                            htmlTableId     : 'CResults',
                            fetchDataUrl    : '{$_subdomain}/LookupTables/ProcessData/Questionnaire/fetch/'+urlencode("{$bId}"),
                            formCancelButton: 'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                           // dblclickCallbackMethod: 'gotoEditPage',
                            //fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            
                            iDisplayLength:  50,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
  
    });

    </script>

    {/block}
{block name=body}

 
    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="bIdForm" style="margin-bottom: -53px;" >

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 
                        <br /><br />
                        Brand :
                        <select name="bId" id="bId" >
                            <option value="" {if $bId eq ''}selected="selected"{/if}> Select Brand </option>
                             

                            {foreach $brands as $brand}

                                <option value="{$brand.BrandID}" {if $bId eq $brand.BrandID}selected="selected"{/if}>{$brand.BrandName|escape:'html'}</option>

                            {/foreach}
                

                                                    </select>


                    </form>
                </div>  


                <div class="LTResultsPanel" id="CResultsPanel" >

                    <table id="CResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        
                                        <th title="{$page['Text']['questionnaire_brandID']|escape:'html'}" >{$page['Text']['questionnaire_brandID']|escape:'html'}</th> 
                                        <th title="{$page['Labels']['brand_ID']|escape:'html'}" >{$page['Labels']['brand_ID']|escape:'html'}</th>
                                        <th title="{$page['Labels']['brand_name']|escape:'html'}" >{$page['Labels']['brand_name']|escape:'html'}</th>
                                        <th title="{$page['Text']['implementation_date']|escape:'html'}"  >{$page['Text']['implementation_date']|escape:'html'}</th>
                                        <th title="{$page['Labels']['termination_date']|escape:'html'}"  >{$page['Labels']['termination_date']|escape:'html'}</th>
                                        <th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                        
                                        
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}


