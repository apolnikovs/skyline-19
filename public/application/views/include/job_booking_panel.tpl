
<script type="text/javascript" src="{$_subdomain}/js/plupload/plupload.full.js"></script>


<script type="text/javascript">


    var state = 0;
    var batchHash = randomString();
    var description = "test";
    var csvSearchTable;
    var mainHtml;


    $(document).ready(function() {
    
   
    $('#AddBookingTypes').click(function(e) {
                
                   $(location).attr('href', '{$_subdomain}/Job/settings');
                    
                });
                
     $('.EditBookingTypes').click(function(e) {
                
                   if($(this).attr('id')=='ServiceActionTrackingSettings')
                   {
                      alert('Sorry, there is no settings page for Service Action Tracking at the moment.');     
                   }
                   else
                   {
                        $(location).attr('href', '{$_subdomain}/Job/settings/'+$(this).attr('id'));
                   }
                    
                });            
                

    });
   
    $(document).on("click", "#csvSearchA", function() {

	var html = '\
		    <div id="csvSearchDiv">\
			<fieldset>\
			    <legend>CSV Batch Imports</legend>\
			    <table style="margin-top:20px;" id="csvSearchTable" border="0" cellpadding="0" cellspacing="0" class="browse">\
				<thead>\
				    <tr>\
					<th>Import Date</th>\
					<th>Time</th>\
					<th>Batch No</th>\
					<th>Description</th>\
					<th>Service Provider</th>\
					<th>Jobs</th>\
				    </tr>\
				</thead>\
				<tbody></tbody>\
			    </table>\
			    <button id="csvSelect" class="gplus-blue-disabled">Select</button>\
			</fieldset>\
		    </div>\
		   ';

	$.colorbox({    html :	    html, 
			title :	    "CSV Batch Imports", 
			scrolling : false,
			onComplete: function() { csvSearchCboxLoaded(); }
		    });

	return false;
    });


    function csvSearchCboxLoaded() {

        csvSearchTable = $('#csvSearchTable').dataTable({
	    aoColumns:		[
				    { sWidth: "14%" },
				    { sWidth: "8%" },
				    { sWidth: "11%" },
				    null,
				    { sWidth: "20%" },
				    { sWidth: "7%" },
				    { bVisible: false }
				],
	    bProcessing:	true,
	    bServerSide:	false,
	    sAjaxSource:	"{$_subdomain}/Job/getBatchList/",
	    sDom:		"t<'#dataTables_child'>rpl",
	    sPaginationType:	"full_numbers",
	    bPaginate:		true,
	    bSearch:		false,
	    iDisplayLength:	10,
	    aaSorting:		[[ 2, "desc" ]],
	    fnDrawCallback:	function() { csvSearchTableInit(); }
	});


	$(document).on("dblclick", "td", function() {
	    var pos = csvSearchTable.fnGetPosition(this);
	    var data = csvSearchTable.fnGetData(pos[0]);
	    location.assign("{$_subdomain}/index/jobSearch?jobSearch=*&batchID=" + data[6]);
	});


	$(document).on("click", "#csvSelect", function() {
	    var pos = csvSearchTable.fnGetPosition($(".row_selected").find("td")[0]);
	    var data = csvSearchTable.fnGetData(pos[0]);
	    location.assign("{$_subdomain}/index/jobSearch?jobSearch=*&batchID=" + data[6]);
	});


	function csvSearchTableInit() {
	
	    $.colorbox.resize();
	    $("#csvSelect").attr("class","gplus-blue-disabled");
	    $("tr").each(function() {
		if($(this).attr("class")) {
		    var newClass = $(this).attr("class").replace(" row_selected","");
		    $(this).attr("class",newClass);
		}
	    });

	    $(document).on("click", "tr", function() {
		$("tr").each(function() {
		    if($(this).attr("class")) {
			var newClass = $(this).attr("class").replace(" row_selected","");
			$(this).attr("class",newClass);
		    }
		});
		$(this).attr("class", $(this).attr("class") + " row_selected");
		$("#csvSelect").attr("class","gplus-blue");
	    });
    
	}
	

    }


    //click handler for job types dropdown box starts here.
    $(document).on('click', ".spaceBlock.PopUp", function() {

	$job_type = $(this).attr("id").replace("_", "=");

	//It opens color box popup page.              
	$.colorbox( {	href: '{$_subdomain}/Job/networkClientPopUp/' + $job_type + '/' + Math.random(),
			title: 'New Job Booking',
			data: $('#newJobBookingForm').serializeArray(), 
			opacity: 0.75,
			width:650,
			overlayClose: false,
			escKey: false, 
			onComplete: function() {
			    $(this).colorbox.resize({ height: ($('#JobNetworkClientForm').height() + 150) + "px" });
			}
	}); 

	return false;
    });


    $(document).on("click","#csvImportA",function() {

	var html='  <fieldset>\
			<legend>CSV Job Import</legend>\
			<div id="csvSelectFileDiv" style="width:450px;">\
			    <div id="container" style="margin-top:20px; position:relative; display:inline-block; width:100%;">\
				<p id="message" style="display:none;" class="textCenter"></p>\
				<div id="statusWrapper" style="display:none;" class="rowWrapper">\
				    <div class="rowHeading">Status</div>\
				    <div id="status" class="blackBold">Waiting for file to be selected</div>\
				</div>\
				<div id="fileNameWrapper" style="display:none;" class="rowWrapper">\
				    <div class="rowHeading">File name</div>\
				    <div id="filelist" class="blackBold"></div>\
				</div>\
				<div id="descriptionWrapper" style="display:none;" class="rowWrapper">\
				    <div id="descriptionHeading">\
					Please enter a description that will be assigned to this specific import\
				    </div>\
				    <div id="description" class="blackBold"><input type="text" id="descriptionInput" /></div>\
				</div>\
				<div id="totalJobsWrapper" style="display:none;" class="rowWrapper">\
				    <div class="rowHeading">Total Jobs</div>\
				    <div id="totalJobs" class="blackBold"></div>\
				</div>\
				<div id="importedJobsWrapper" style="display:none;" class="rowWrapper">\
				    <div class="rowHeading">Imported Jobs</div>\
				    <div id="importedJobs" class="blackBold"></div>\
				</div>\
				<div id="progressBarWrapper" style="display:none;" class="rowWrapper">\
				    <div id="percentage">0%</div>\
				    <div id="progressbar"></div>\
				</div>\
				<p id="message2" style="display:none;"></p>\
				<div id="buttonWrapper" class="textCenter">\
				    <a id="pickfiles" class="btnStandard" href="#">Select file</a>\
				    <a id="uploadfiles" class="btnStandard" style="display:none" href="#">Upload file</a>\
				    <a id="cancel" class="btnCancel" href="#">Cancel</a>\
				    <a id="refresh" class="btnStandard" style="display:none" href="#">Reupload</a>\
				</div>\
			    </div>\
			</div>\
		    </fieldset>\
		 ';

	mainHtml = html;

	$.colorbox({    html : html, 
			title : "Import CSV", 
			scrolling : false,
			width: 600,
			onComplete : function() { importCboxLoaded(); }
		    });

	$("#uploadfiles").css("display","none")
	$("#cancel").css("display","inline-block");

	return false;
    });


    $(document).on("click","#cancel",function() {
	$.colorbox.close();
	return false;
    });


    $(document).on("click","#pickfiles",function() {
	$("#message").text("");
	$("#filelist").text("");
	return false;
    });


    $(document).on("click","#refresh",function() {
	$.colorbox({    html : mainHtml, 
			title : "Import CSV", 
			scrolling : false,
			width: 550,
			onComplete : function() { importCboxLoaded(); }
		    });
	return false;
    });


    function importCboxLoaded() {

	batchHash = randomString();

	if($("#csvSelectFileDiv").length != 0) {

	    //AJAX file upload

	    $(function() {
	    
		var uploader = new plupload.Uploader({
		    runtimes : 'html4',
		    browse_button : 'pickfiles',
		    container : 'container',
		    max_file_size : '10mb',
		    url : '{$_subdomain}/index/csvUpload?batchHash=' + batchHash,
		    filters : [
			{ title : "CSV files", extensions : "csv" }
		    ]
		});
		
		
		
		$('#uploadfiles').click(function(e) {

		    if($("#descriptionInput").val()=="") {
			$("#message").css("display","block");
			$("#message").text("Please input batch description.");
			$("#message").css("color","red");
			$.colorbox.resize();
			return false;
		    } else {
			$("#statusWrapper").css("display","block");
			description=$("#descriptionInput").val();
			uploader.settings.url += "&description=" + description;
			$("#message").css("display","none");
			$("#description").html($("#descriptionInput").val());
			$.colorbox.resize();
		    }

		    $("#descriptionHeading").text("Description");
		    $("#descriptionHeading").attr("class","rowHeading");

		    $('#uploadfiles').css("display","none");

		    //start spinner
		    $("#status").html("Uploading file... <img src='{$_subdomain}/css/Skins/skyline/images/loader.gif' />");

		    state = 0;    //set upload state to 0 so that progress update function could kick in
		    loop();

		    uploader.start();
		    e.preventDefault();

		    $.colorbox.resize();
		    return false;

		});

		
		uploader.bind('init', function() {
		    setTimeout(function() {
			$('input[type=file]').each(function() {
			    //$(this).css("font-size","19px");
			    $(this).css("margin-top","-1px");
			    $(this).css("margin-left","-4px");
			    //$(this).css("visibility","hidden");
			    //var the = this;
			    //$(document).on("click", "#pickfiles", function() {
			    //	$(the).click();
			    //})  
			});
		    }, 200);
		});
		
		
		//IE6 nuisance
		//setTimeout(function() { uploader.init(); }, 100);
		uploader.init();

		
		//this function makes recursive AJAX calls to get current import progress response
		function loop() {
		    if(state == 0) {
			$.post("{$_subdomain}/index/progressResponse", { batchHash: batchHash }, function(data) {
			    updateProgress(data);
			    setTimeout(function() { loop(); }, 500);
			});
		    } else {
			$.post("{$_subdomain}/index/progressResponse", { batchHash: batchHash }, function(data) {
			    updateProgress(data);
			});
		    }
		}


		function updateProgress(jsonString) {

		    //$("#message2").html(jsonString);	//debug json
		    //$("#message2").html($("#message2").html() + jsonString + "<br/>");	//debug json
		    var data = $.parseJSON(jsonString);

		    var progress = (data.importedJobs/data.totalJobs) * 100;
		    if(isNaN(progress)) {
			progress = 0;
		    }
		    progress = parseInt(progress);

		    $("#percentage").text(progress + "%");
		    $("#progressbar").progressbar({ value: progress });

		    if(data.batchState == "initiating") {
			$("#status").text("checking");
		    }
		    
		    if(data.batchState == "importing") {
			$("#status").css("color", "blue");
			$("#status").text("Importing jobs");
			$("#totalJobsWrapper, #importedJobsWrapper, #progressBarWrapper").css("display", "block");
		    }

		    if(data.batchState == "finished") {
			$("#status").css("color", "green");
			$("#status").text("Finished");
			$("#totalJobsWrapper, #importedJobsWrapper, #progressBarWrapper").css("display", "block");
		    }

		    $("#totalJobs").text(data.totalJobs);
		    $("#importedJobs").text(data.importedJobs);

		    $.colorbox.resize();

		}


		uploader.bind('FilesAdded', function(up, files) {
		    
		    $.each(files, function(i, file) {
			$('#filelist').append(
			    '<div id="' + file.id + '">' +
			    //file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
			    file.name +
			'</div>');
		    });

		    //IE6 fix
		    setTimeout(function() {
			$('input[type=file]').each(function() {
			    $(this).css("visibility","hidden");
			});
		    }, 200);
		    

		    $("#status").text("File selected, waiting for upload");
		    $("#message").css("display","none");
		    
		    $("#descriptionWrapper").css("display","block");
		    $("#fileNameWrapper").css("display","block");
		    $("#uploadfiles").css("display","inline-block")
		    $("#pickfiles").css("display","none");
		    $("#cancel").css("display","inline-block");
		    
		    $.colorbox.resize();
		    
		});


		uploader.bind('UploadProgress', function(up, file) {
		    $('#' + file.id + " b").html(file.percent + "%");
		    $.colorbox.resize();
		});


		uploader.bind('Error', function(up, err) {
		    if(err.code=="-601") {
			$('#filelist').text(err.file.name);
			$("#message").text("Wrong file format, only CSV is allowed.");
			$("#message").css("display","block");
			$("#message").css("color","red");
		    } else {
			$('#filelist').append("<div>Error: " + err.code +
			    ", Message: " + err.message +
			    (err.file ? ", File: " + err.file.name : "") +
			    "</div>"
			);
			//$("#message").text("An error has occured. Please try again later.");
			//$("#message").css("color","red");
		    }
		    $("#fileNameWrapper").css("display","block");
		    $.colorbox.resize();
		});

		
		uploader.bind('FileUploaded', function(up, file, response) {
		    state=1;    //change the state to 1 so that progress update recursive AJAX function would stop
		    //$("#message2").html($("#message2").html() + response.response + "<br/>");	//debug json

		    //IE6 fix
		    setTimeout(function() {
			$('input[type=file]').each(function() {
			    $(this).css("visibility","hidden");
			});
		    }, 200);

		    //process response result
		    var result=$.parseJSON(response.response);

		    //header match problem
		    if(result.status == 2) {
			headerTable(result.header);
			$("#message").css("display","block");
			$("#message").text("There are errors in the File Header – please correct and try again");
			$("#status").text("Errors found in Header Record");
			$("#message").css("color","red");
		    }
		
		    //content error
		    if(result.status == 3) {
			contentTable(result);
			$("#message").css("display","block");
			$("#message").text("There are errors in the File Content – please correct and try again.");
			$("#status").text("Errors found in File Content");
			$("#message").css("color","red");
		    }
		
		    //general error
		    if(result.status == 0) {
			contentTable(result);
			$("#message").css("display","block");
			$("#message").text(result.error);
			$("#message").css("color","red");
		    }

		    //import finished successfuly
		    if(result.status == 1) {
			$('#' + file.id + " b").html("100%");
			$("#cancel").text("Close");
			$.colorbox.resize();
		    }

		});
		
		
		function headerTable(header) {
		    
		    var html = '<table id="headerTable" style="clear:both" border="0" cellpadding="0" cellspacing="0" class="browse">\
				    <thead>\
					<tr>\
					    <th>Column</th>\
					    <th>Required Format</th>\
					    <th>Actual Content</th>\
					</tr>\
				    </thead>\
				</table>\
				';
    
		    $(html).insertBefore("#buttonWrapper");
		    
		    var hTable = $("#headerTable").dataTable({
			aaData:		header,
			aoColumns:	[
					    null,
					    { sDefaultContent: "" },
					    null
					],
			bProcessing:	true,
			bServerSide:	false,
			sDom:		"t<'#dataTables_child'>rpl",
			bPaginate:	false,
			bSearch:	false,
			iDisplayLength:	-1
		    });
    
		    $("#headerTable tbody tr").each(function() {
			var cell = $(this).find("td")[2];
			if($(cell).text() != "OK") {
			    $(this).css("color","red");
			}
		    });
    
		    $("#statusWrapper").css("display","none");
		    $("#refresh").css("display","inline-block");

		    $.colorbox.resize();

		}
		
				    
		function contentTable(data) {
		    
		    var html = '<p style="margin:10px 0px; display:block; float:left; position:relative; clear:both;">Errors found in cells:</p>\
				<table id="errorTable" style="clear:both" border="0" cellpadding="0" cellspacing="0" class="browse">\
				    <thead>\
					<tr>\
					    <th>Row</th>\
					    <th>Column</th>\
					    <th>Actual value</th>\
					    <th>Format rule</th>\
					</tr>\
				    </thead>\
				</table>\
				';

		    $(html).insertBefore("#buttonWrapper");
		    
		    var html = '<p style="margin-top:15px; width:100%; text-align:center; display:block; float:left; position:relative; clear:both;">\
				    <a href="#" id="showHeaderTable">Display Content Rules</a>\
				</p>\
				<table id="headerTable" style="display:none; clear:both;" border="0" cellpadding="0" cellspacing="0" class="browse">\
				    <thead>\
					<tr>\
					    <th>Column</th>\
					    <th>Column Title</th>\
					    <th></th>\
					    <th>Format Rule</th>\
					</tr>\
				    </thead>\
				</table>\
			       ';
    
		    var showHeaderTable = false;
    
		    $(document).on("click", "#showHeaderTable", function() {
			if(!showHeaderTable) {
			    $("#headerTable").css("display","block");
			    $(this).text("Display Content Rules");
			    showHeaderTable = true;
			} else {
			    $("#headerTable").css("display","none");
			    $(this).text("Hide Content Rules");
			    showHeaderTable = false;
			}
			$.colorbox.resize();
		    });
    
		    $(html).insertAfter("#buttonWrapper");
		    
		    var hTable = $("#headerTable").dataTable({
			aaData:		data.header,
			aoColumns:	[
					    { sWidth: "20%" },
					    { sDefaultContent: "", sWidth: "30%" },
					    { bVisible: false },
					    { sWidth: "50%" },
					],
			bProcessing:	false,
			bServerSide:	false,
			sDom:		"t<'#dataTables_child'>rpl",
			bPaginate:	false,
			bSearch:	false,
			iDisplayLength:	-1
		    });

		    var eTable = $("#errorTable").dataTable({
			aaData:		data.errorRows,
			aoColumns:	[
					    null,
					    null,
					    null,
					    null
					],
			bProcessing:	false,
			bServerSide:	false,
			sDom:		"t<'#dataTables_child'>rpl",
			bPaginate:	true,
			bSearch:	false,
			iDisplayLength:	10,
			aLengthMenu:	[[5,10,20,30,50,-1],[5,10,20,30,50,"All"]],
			sPaginationType:"full_numbers"
		    });
			
		    $("#statusWrapper").css("display","none");
		    $("#refresh").css("display","inline-block");

		    $.colorbox.resize();

		}
				    


	    });

	}

    }


</script>


<form id="newJobBookingForm" name="newJobBookingForm" method="get">
        
    <fieldset style="padding-right: 40px;padding-top: 18px; padding-bottom:5px">

	<legend>New Job Booking</legend>

	<p style="font-size:13px">
	   Select one of these options to commence booking a new service job 
	</p>

	<p id="jobSearchP" style="margin-bottom: 0;">

	    <span style="float:left;position:relative;zoom:1;" >

		{*if $JobTypesList|@count gt 0*}

		    {if $loggedin_user->ClientID}

			{foreach $JobTypesList as $jtype}  
                            <label>
			    
                            <a href="{$_subdomain}/Job/index/JobTypeID={$jtype.JobTypeID}" class="spaceBlock" style="margin-right:0px;padding-right:0px;" >
				{$jtype.Name|escape:'html'}
			    </a>
                            
                             {if $loggedin_user->Username=="sa" || isset($loggedin_user->Permissions["AP11009"])}
                                 <img style="padding:0px;margin:0px;" id="JobTypeID={$jtype.JobTypeID|escape:'html'}" class="EditBookingTypes" src="{$_subdomain}/css/Skins/{$_theme}/images/defaultsSetup_icon.png" height="28" border="0" >
                             {/if} 
                            
                            </label>
			{/foreach} 

		    {else}

			 {foreach $JobTypesList as $jtype}     
                            <label>
                             <a href="#" id="JobTypeID_{$jtype.JobTypeID|escape:'html'}" class="spaceBlock PopUp" style="margin-right:0px;padding-right:0px;" >
				{$jtype.Name|escape:'html'}
			     </a> 
                             {if $loggedin_user->Username=="sa" || isset($loggedin_user->Permissions["AP11009"])}
                                 <img style="padding:0px;margin:0px;" id="JobTypeID={$jtype.JobTypeID|escape:'html'}" class="EditBookingTypes" src="{$_subdomain}/css/Skins/{$_theme}/images/defaultsSetup_icon.png" height="28" border="0" >
                             {/if}    
                            </label>
			 {/foreach} 

		    {/if}
                    
                    {if $loggedin_user->Username=="sa" || isset($loggedin_user->Permissions["AP12005"])}
			<label>
                            
                            <a href="{$_subdomain}/Job/serviceActionTracking" id="serviceActionTracking" class="spaceBlock" style="margin-right:0px;padding-right:0px;" >
                                Service Action Tracking
                            </a> 
                            {if $loggedin_user->Username=="sa" || isset($loggedin_user->Permissions["AP11009"])}
                                 <img style="padding:0px;margin:0px;" id="ServiceActionTrackingSettings" class="EditBookingTypes" src="{$_subdomain}/css/Skins/{$_theme}/images/defaultsSetup_icon.png" height="28" border="0" >
                             {/if}
                                                            
                        </label>
		    
                            
                    {/if}
                    
                    {*
                    {if $loggedin_user->Username=="sa" || isset($loggedin_user->Permissions["AP11009"])}
                       <label> <img src="{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png" style="cursor:pointer;" title="Click here to create new booking type." alt="Click here to create new booking type." width="24" height="24" id="AddBookingTypes" >  </label>
		    {/if}
                    *}
                    

		    {if $loggedin_user->Username=="sa" || 
			$loggedin_user->Username=="JBCAM" || 
			$loggedin_user->Username=="jbcam" || 
			$loggedin_user->Username=="sd001" ||
			$loggedin_user->Username=="SD001"}
                        <br>
			<label><a href="#" id="csvImportA" style="margin-right:5px;" class="btnStandard">CSV Import</a> </label>
			<label><a href="#" id="csvSearchA" style="margin-right:7px;" class="btnStandard">Search CSV Batch</a> </label>
		    {/if}

		   
		    
                    
                    
		{*else}

		    <label class="fieldError">Service Types are not attached to Job types.</label>

		{/if*}

	    </span>

	</p>

    </fieldset>
        
</form>
