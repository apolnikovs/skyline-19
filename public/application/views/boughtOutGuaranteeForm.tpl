{*{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}*}
<script>

$(document).ready(function() {
    //$("#manufacturerID").combobox();

    $(document).on("click", "#save", function() {

    
	$("#bogForm").validate({
	    /*rules: {
		manufacturerID: { required: function() { 
						return ($("#manufacturerID").val() == "");
					    } 
		},
		repairSkillID:  { required: function() { 
						return ($("#repairSkillID").val() == ""); 
					    } 
		}
	    },
	    messages: {
		manufacturerID: { required: "{$page['Errors']['manufacturer']|escape:'html'}" },
		repairSkillID:  { required: "{$page['Errors']['repair_skill']|escape:'html'}" }
	    },*/
	    errorPlacement: function(error, element) {
		error.insertAfter(element);
	    },
	    ignore:	    "",
	    errorClass:	    'fieldError',
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   'p',
	    submitHandler: function(form) {
		$.post("{$_subdomain}/JobAllocation/saveEditBOG", $("#bogForm").serialize(), function(response) {
		    $.colorbox.close();
		    table.fnReloadAjax(null,null,null,true);
		});
		return false;
	    }
	});

	$("#bogForm").submit();

	return false;

    });


});


</script>


<div id="bogModalContainer">
    
    <fieldset>
	<legend align="center">{$legend}</legend>
	<br/>
	{*<p style="text-align:center; margin-bottom:15px;">Fields marked with <sup style="color:red; font-size:1em;">*</sup> are compulsory</p>
	*}
	<form id="bogForm" action="" method="post">
	
	    <p>
		<label class="fieldLabel required" for="manufacturerID">{$page['Labels']['manufacturer']|escape:'html'}:</label>
		<select id="manufacturerID" name="manufacturerID">
		    <option value="">All Manufacturers</option>
		    {foreach $manufacturers as $man}
			<option value="{$man.ManufacturerID}" {if $data && $data.ManufacturerID == $man.ManufacturerID}selected="selected"{/if}>{$man.ManufacturerName}</option>
		    {/foreach}
		</select>
	    </p>

	    <p>
		<label class="fieldLabel" for="repairSkillID">{$page['Labels']['repair_skill']|escape:'html'}:</label>
		<select id="repairSkillID" name="repairSkillID">
		    <option value="">All Repair Skills</option>
		    {foreach $repairSkills as $rep}
			<option value="{$rep.RepairSkillID}" {if $data && $data.RepairSkillID == $rep.RepairSkillID}selected="selected"{/if}>{$rep.RepairSkillName}</option>
		    {/foreach}
		</select>
	    </p>

	    <p>
		<label class="fieldLabel" for="unitTypeID">{$page['Labels']['unit_type']|escape:'html'}:</label>
		<select id="unitTypeID" name="unitTypeID">
		    <option value="">&nbsp;</option>
		    {foreach $unitTypes as $unit}
			<option value="{$unit.UnitTypeID}" {if $data && $data.UnitTypeID == $unit.UnitTypeID}selected="selected"{/if}>{$unit.UnitTypeName}</option>
		    {/foreach}
		</select>
	    </p>

	    <p>
		<label class="fieldLabel" for="modelID">{$page['Labels']['model']|escape:'html'}:</label>
		<select id="modelID" name="modelID">
		    <option value="">&nbsp;</option>
		    {foreach $models as $model}
			<option value="{$model.ModelID}" {if $data && $data.ModelID == $model.ModelID}selected="selected"{/if}>{$model.ModelNumber}</option>
		    {/foreach}
		</select>
	    </p>

	    <p style="margin-left:110px;">
		{if $data && $data.Status}
		    <input type="radio" name="status" value="Active" {if $data && $data.Status == "Active"}checked="checked"{/if}> {$page['Labels']['active']|escape:'html'}
		    <input type="radio" name="status" value="In-active" {if $data && $data.Status == "In-active"}checked="checked"{/if}> {$page['Labels']['inactive']|escape:'html'}
		{else}
		    <input type="radio" name="status" value="Active" checked="checked"> {$page['Labels']['active']|escape:'html'}
		    <input type="radio" name="status" value="In-active"> {$page['Labels']['inactive']|escape:'html'}
		{/if}
	    </p>

	    <input type="hidden" value="{$data.NetworkID|default:""}" name="networkID" />
	    <input type="hidden" value="{$data.ClientID|default:""}" name="clientID" />
	    <input type="hidden" value="{$data.BoughtOutGuaranteeID|default:""}" name="bogID" />
	    
	</form>
	
	<p style="text-align:center;">
	    <a href="#" id="save" class="DTTT_button" style="margin-right:10px;">save</a>
	    <a href="#" id="cancel" class="DTTT_button">cancel</a>
	</p>
	
    </fieldset>
    
</div>