<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script>

//adding custom method to validate a date field
$.validator.addMethod('regex', function(value, element, param) {
    return this.optional(element) || value.match(typeof param == 'string' ? new RegExp(param) : param);
    },
    'Please enter a value in the correct format.'
);
{literal}
    var pattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
{/literal}


$(document).ready(function() {

    //$("input[name=dateFrom], input[name=dateTo]").datepicker({ dateFormat: "yy-mm-dd" });

    $(document).on("click", "#generate", function() {
	
	var date = new Date();
	fileName = "Service Action Tracking Report " + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDay() + " " + 
		   date.getHours() + "." + date.getMinutes() + "." + date.getSeconds() + "." + date.getMilliseconds();
	
	$("#transactionReportForm").validate = null;

	$("#transactionReportForm").validate({
	    /*rules: {
		dateFrom:   { regex: pattern },
		dateTo:	    { regex: pattern },
	    },
	    messages: {
		dateFrom:   { regex: "Must contain a valid date." },
		dateTo:	    { regex: "Must contain a valid date." },
	    },*/
	    errorPlacement: function(error, element) {
		error.insertAfter(element);
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "p",
	    submitHandler: function() {
		
		document.body.style.cursor = "wait";
		
		/*
		$("#client").next(".ui-combobox").remove();
		$("#branch").next(".ui-combobox").remove();
		$("#network").next(".ui-combobox").remove();
		$("label[for=branch], label[for=client], label[for=network]").remove();
		*/
	       
		$.colorbox.close();
	       
		hash = randomString();
		
		$.post("{$_subdomain}/Report/serviceActionTrackingReport/", 
			$("#transactionReportForm").serialize() + "&hash=" + hash + "&filename=" + fileName + "&branches=" + JSON.stringify($("#branch").multiselect("getChecked").map(function() { return this.value; }).get()),
			function(response) {
			    document.body.style.cursor = "default";
			    location.href = "{$_subdomain}/Report/downloadReportFile/?filename=" + response;
			}
		);
		
		//$("label[for=dateFrom], input[name=dateFrom], label[for=dateTo], input[name=dateTo], #btnWrapper").hide();
		//$.colorbox.resize();
    
		return false;
	    }
	});

	$("#transactionReportForm").submit();
	
	$("#generate").die();
	$("#generate").off();
	
	return false;
	
    });
    
    
    $(document).on("click", "#cancel", function() {
	$.colorbox.close();
	return false;
    });


});


</script>


<div id="samsungTransactionReportContainer">
    
    <fieldset>
	
	<legend align="center">Service Action Tracking Report</legend>
	
	<form id="transactionReportForm" action="" method="post">
	    
	    <p style="margin:10px 0px;">This report will list Service Action Tracking records associated to the current User</p>
	    
	    {if $userLevel == "Admin"}
		<label for="network" style="width: 90px; margin-right:0;">Network: </label>
		<select id="network" name="network">
		    <option value="">Select Network</option>
		    {foreach $networks as $network}
			<option value="{$network.NetworkID}">{$network.CompanyName}</option>
		    {/foreach}
		</select>
	    {/if}
	    
	    {if $userLevel == "Network"}
		<label for="client" style="width: 90px; margin-right:0;">Client: </label>
		<select id="client" name="client">
		    <option value="">Select Client</option>
		    {foreach $clients as $client}
			<option value="{$client.ClientID}">{$client.ClientName}</option>
		    {/foreach}
		</select>
	    {/if}

	    {if $userLevel == "Client"}
		<label for="branch" style="width: 90px; margin-right:0;">Branch: </label>
		<select id="branch" name="branch">
		    {foreach $branches as $branch}
			<option value="{$branch.BranchID}">{$branch.BranchName}</option>
		    {/foreach}
		</select>
	    {/if}

	    <br/>
	    
	    <label for="dateFrom" style="margin-right:0; width: 90px;">Booked From:</label>
	    <input type="text" style="width:80px;" name="dateFrom" />

	    <label for="dateTo" style="margin-right:5px; margin-left:5px; width: 67px;">Booked To:</label>
	    <input type="text" style="width:80px;" name="dateTo" />
	    
	</form>
	
	<div id="btnWrapper" style="text-align:center; position:relative; display:block; float:left; width:100%; clear:both; margin:10px 0px 10px 0px;">
	    <a href="#" id="generate" class="btnConfirm" style="margin-right:10px; display:inline-block; width:60px;">Generate</a>
	    <a href="#" id="cancel" class="btnCancel" style="display:inline-block; float:none; width:60px;">Cancel</a>
	</div>
	
    </fieldset>
    
</div>
