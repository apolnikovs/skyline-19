<?php

require_once('CustomController.class.php');


class SimpleAPIController extends CustomController {
    
    
    
    public $config;  
    public $messages;
    
    
    
    public function __construct() { 
        
        parent::__construct(); 
        
        $this->config = $this->readConfig("application.ini");
        
        $this->messages = $this->loadModel("Messages"); 
	
    }
    
    
    
    public function goToJobUpdateAction() {
	
	$simpleAPIModel = $this->loadModel("SimpleAPI");
	$valid = $simpleAPIModel->validateUser($_REQUEST);
	
	if ($valid) {
	    
	    $jobID = $simpleAPIModel->getJobID($_REQUEST);
	    $_SESSION["EngineerName"] = $_REQUEST["EngineerName"];
	    header("Location: " . SUB_DOMAIN . "/index/jobupdate/" . $jobID . "/?ref=authRequired");
	    
	} else {
	    
	    throw new Exception("User validation failed.");
	    
	}
	
    }
    
    
    
}


?>