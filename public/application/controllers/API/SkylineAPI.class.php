<?php

/**
 * SkylineAPI.class.php
 * 
 * Master class for SkyLine API.
 * 
 * Provides functionality for getting skyline user information
 * By deafult all methods return a 501 (not implemented response. Sepcific APIs
 * should extend this class and override to provide specific functionality.
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.02
 *  
 * 25/06/2012  1.00    Andrew J. Williams    Initial Version
 * 16/07/2012  1.01    Andrew J. Williams    Issue 41 - Skyline API Class returning wrong user type
 * 25/09/2012  1.02    Andrew J. Williams    ServiceBase - Clarion has no PUT capability, so send POST requests to PUT
 ********************************************************************************/

require_once('CustomRESTController.class.php');

class SkylineAPI extends CustomRESTController {
    
    public $user = null;
    
    public function __construct($user_info) { 
        
        parent::__construct(); 
        
        
        /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
        /* ==========================================
         * initialise Session Model
         * ==========================================
         */
        
        $this->session = $this->loadModel('Session');

        /* ==========================================
         * Set skyline user details
         * ==========================================
         */
        
        $this->user = $user_info; 
        
        //$this->log(var_export($this->user, true));

    }
    
    public function get( $args ) {
        
        $this->sendResponse(501);
                
    }    
    
    public function put( $args ) {
        
        $this->sendResponse(501);
                
    }
    
    public function post( $args ) {
        
        $this->put( $args );                                                    /* ServiceBase - Clarion can not put, only post so send all post to put */
                
    }
    
    public function delete( $args ) {
        
        $this->sendResponse(501);
        
        
    }
    
    /**
     * Description
     * 
     * Get the type of user who has authenticated to the API
     * 
     * @param none
     * 
     * @return string {"Network", ServiceProvider", "Client", "Branch"} getUserType - type of user or null if no user type found
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/ 
    
    protected function getUserType() {
        /* Only the id for the type of user is set in the users table and hence ib getUser in the user's model in the user_info type of the class */
        if (isset($this->user->ServiceProviderID)) {
            return("ServiceProvider");
        } elseif (isset($this->user->BranchID)) {
            return("Branch");    
        } elseif (isset($this->user->ClientID)) {
            return("Client");
        } elseif (isset($this->user->NetworkID)) {
            return("Network");
        } else {
            return (null);
        }
    }
}

?>