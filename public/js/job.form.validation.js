function validateJobForm() {
    if ($('#jobSearch').val()=='') {        
        $('#jobError').html('Please enter a search term').show('slow');
        $('#jobSearch').focus();
        return false;
    }
    return true;
}

function validateFreeTextJobBookingForm() {
    if ( $('#jobDescription').val()=='' ||
        $('#Manufacturer').val()=='' ||
        $('#jobType').val()=='' ) {        
            $('#jobError').html('Please fill in all the fields marked compulsory').show();
            $('.formError').show('slow');

            $('#jobDescription').focus();
            return false;
    }
    return true;
}

function setLabel( id, text ) {
    var label = $('label[for=' + id + ']');
    $(label).html(text).css('margin-right','-'+label.width()+'px');  
}

$(document).ready(function(){

    $('input[type=text]').jLabel().attr('autocomplete','off').blur(function() {$('.formError').hide('slow');} );
    $('form:first *:input[type!=hidden]:first').focus();            


    $('#jobSearchForm input[name=jobGroup]').click(function(){
   
       //log.info( 'clicked -> ' + this.value ); 
       switch(this.value){
           case 'JobID':
             setLabel('jobSearch','Please enter a SL number or select a different option');
             break;
           case 'ServiceCentreJobNo':
               setLabel('jobSearch','Please enter a job number or select a different option');
               break;
           case 'PostalCode':
               setLabel('jobSearch','Please enter a postcode or select a different option');
               break;    
           case 'ContactLastName':
               setLabel('jobSearch','Please enter a surname or select a different option');
               break;                   
           default:
               setLabel('jobSearch','jLabel Error');
       }
    }); 
  

//    $('#jobSearchForm input[type=radio]').keypress(function(e) {
//        if(e.which == 13) {
//            log.info('clicked job button');
//            $(this).blur();
//            if (validateJobForm()) $('#jobAdvancedSearchForm').submit();
//            return false;
//        }
//    } );    
    
    $('#Description').keydown(function(){
        $('.formError').hide('slow');
    });
    $('#Manufacturer, #Type').change(function(){
        $('.formError').hide('slow');
    });
    
    $('#PickDataButton').click(function(){
        log.info($('#JobID').val());
    });
});// end  document ready